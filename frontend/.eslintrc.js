/* eslint-disable prettier/prettier */
const path = require('path');

module.exports = {
    extends: [path.resolve(__dirname, '../.eslintrc.js'), 'plugin:react/recommended'],
    plugins: ['react', 'react-hooks', 'formatjs'],
    parserOptions: {
        sourceType: 'module',
        useJSXTextNode: true,
        project: [
            path.resolve(__dirname, 'tsconfig.json'),
            path.resolve(__dirname, 'tsconfig.eslint.json'),
        ],
        ecmaVersion: 2018,
        ecmaFeatures: {
            jsx: true,
        },
    },
    settings: {
        react: {
            version: 'detect',
        },
    },
    rules: {
        'react/display-name': 0,
        'react/react-in-jsx-scope': 0,
        'react/no-children-prop': 0,
        'react/prop-types': 0,
        'react-hooks/rules-of-hooks': 'error',
        'react-hooks/exhaustive-deps': 0,
        'react-hooks/rules-of-hooks': 0,
        '@typescript-eslint/explicit-module-boundary-types': 0,
        'formatjs/enforce-plural-rules': [
            'error',
            {
                one: true,
                other: true,
                zero: false,
            },
        ],
    },
}

