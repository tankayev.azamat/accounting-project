import React from 'react';
import { useAppSelector } from '../../hooks';
import { AdminMenu } from './admin/adminMenu';
import { EmployeesMenu } from './employees/employeesMenu';

export const NavBar: React.FC = () => {
  const user = useAppSelector((state) => state.users.user?.user);
  if (user?.roleId === process.env.REACT_APP_MAIN_ROLE) return <AdminMenu />;
  return <EmployeesMenu />;
};
