import React from 'react';
import { Link, Outlet, useLocation } from 'react-router-dom';
import { Button, Layout, Menu } from 'antd';
import {
  BarChartOutlined,
  CalendarOutlined,
  DollarCircleOutlined,
  DotChartOutlined,
  FileDoneOutlined,
  HomeOutlined,
  SubnodeOutlined,
  TeamOutlined,
  UnorderedListOutlined,
  UserAddOutlined,
} from '@ant-design/icons';
import type { MenuProps } from 'antd';
import { ReactComponent as Logo } from '../../../assets/images/flogo.svg';
import { useAppDispatch } from '../../../hooks';
import { logoutUser } from '../../../store/services/UsersSlice';

import './adminMenu.css';

type MenuItem = Required<MenuProps>['items'][number];

function getItem(
  label: React.ReactNode,
  key?: React.Key | null,
  icon?: React.ReactNode,
  children?: MenuItem[],
  type?: 'group'
): MenuItem {
  return {
    key,
    icon,
    children,
    label,
    type,
  } as MenuItem;
}

const items: MenuItem[] = [
  getItem(<Link to="/job_directory">Должности</Link>, '/job_directory', <TeamOutlined />),
  getItem(<Link to="/kpi_catalog">KPI</Link>, '/kpi_catalog', <BarChartOutlined />),
  getItem(
    <Link to="/service_elements">Элементы услуг</Link>,
    '/service_elements',
    <DotChartOutlined />
  ),
  getItem(<Link to="/KpiKeyIndicator">Виды KPI</Link>, '/KpiKeyIndicator', <SubnodeOutlined />),
  getItem(<Link to="/rates">Тарифы</Link>, '/rates', <DollarCircleOutlined />),
];

const { Header, Sider, Content } = Layout;

export const AdminMenu: React.FC = () => {
  const dispatch = useAppDispatch();
  const location = useLocation();

  const logoutUserHandler = () => {
    dispatch(logoutUser());
  };

  return (
    <Layout style={{ minHeight: '100vh' }}>
      <Header className="header">
        <div className="logo logoStyle">
          <Logo />
        </div>
        <Button onClick={logoutUserHandler} style={{ borderRadius: 15 }} type="primary">
          Logout
        </Button>
      </Header>
      <Layout>
        <Sider
          style={{
            overflow: 'auto',
            height: '100vh',
            position: 'sticky',
            top: 0,
            left: 0,
          }}
          width={200}
          className="site-layout-background"
        >
          <Menu
            defaultSelectedKeys={[location.pathname]}
            selectedKeys={[location.pathname]}
            style={{ height: '100%', borderRight: 0 }}
            mode="inline"
            items={[
              {
                key: '/',
                icon: <HomeOutlined />,
                label: <Link to="/">Главная</Link>,
              },
              {
                key: '/employees_list',
                icon: <TeamOutlined />,
                label: <Link to="/employees_list">Сотрудники</Link>,
              },
              {
                key: '/clients_list',
                icon: <UserAddOutlined />,
                label: <Link to="/clients_list">Клиенты</Link>,
              },
              {
                key: '8',
                icon: <UnorderedListOutlined />,
                label: 'Справочники',
                children: items,
              },
              {
                key: '/kpi_agreements',
                icon: <CalendarOutlined />,
                label: <Link to="/kpi_agreements">Согласование</Link>,
              },
              {
                key: '/fact_of_service',
                icon: <FileDoneOutlined />,
                label: <Link to="/fact_of_service">Оказанные услуги</Link>,
              },
            ]}
          />
        </Sider>
        <Layout style={{ padding: '0 24px 24px', marginTop: '10px' }}>
          <Content
            className="site-layout-background Content"
            style={{
              padding: 24,
              margin: 0,
              minHeight: 280,
            }}
          >
            <Outlet />
          </Content>
        </Layout>
      </Layout>
    </Layout>
  );
};
