import React from 'react';
import { Link, Outlet, useLocation } from 'react-router-dom';
import { Button, Layout, Menu } from 'antd';
import { BarChartOutlined, HomeOutlined, TeamOutlined, UserAddOutlined } from '@ant-design/icons';
import { ReactComponent as Logo } from '../../../assets/images/flogo.svg';
import { useAppDispatch, useAppSelector } from '../../../hooks';
import { logoutUser } from '../../../store/services/UsersSlice';

import './employeesMenu.css';

const { Header, Sider, Content } = Layout;

export const EmployeesMenu: React.FC = () => {
  const dispatch = useAppDispatch();
  const location = useLocation();
  const user = useAppSelector((state) => state.users.user?.user);

  const logoutUserHandler = () => {
    dispatch(logoutUser());
  };

  const menuItems = [
    {
      key: '/',
      icon: <HomeOutlined />,
      label: <Link to="/">Главная</Link>,
    },
    {
      key: '/clients_list',
      icon: <UserAddOutlined />,
      label: <Link to="/clients_list">Клиенты</Link>,
    },
    {
      key: '/kpi_employee/',
      icon: <BarChartOutlined />,
      label: <Link to="/kpi_employee/">KPI</Link>,
    },
  ];

  const employeeMenuLink = {
    key: '/employees_list',
    icon: <TeamOutlined />,
    label: <Link to="/employees_list">Сотрудники</Link>,
  };

  if (user?.positionId?.title === 'Ведущий бухгалтер') menuItems.push(employeeMenuLink);

  return (
    <Layout style={{ minHeight: '100vh' }}>
      <Header className="header">
        <div className="logo logoStyle">
          <Logo />
        </div>
        <Button onClick={logoutUserHandler} style={{ borderRadius: 15 }} type="primary">
          Logout
        </Button>
      </Header>
      <Layout>
        <Sider
          style={{
            overflow: 'auto',
            height: '100vh',
            position: 'sticky',
            top: 0,
            left: 0,
          }}
          width={200}
          className="site-layout-background"
        >
          <Menu
            defaultSelectedKeys={[location.pathname]}
            selectedKeys={[location.pathname]}
            style={{ height: '100%', borderRight: 0 }}
            mode="inline"
            items={menuItems}
          />
        </Sider>
        <Layout style={{ padding: '0 24px 24px', marginTop: '10px' }}>
          <Content
            className="site-layout-background Content"
            style={{
              padding: 24,
              margin: 0,
              minHeight: 280,
            }}
          >
            <Outlet />
          </Content>
        </Layout>
      </Layout>
    </Layout>
  );
};
