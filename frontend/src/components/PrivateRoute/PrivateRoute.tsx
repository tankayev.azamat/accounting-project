import { FC, useEffect } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import { userInfo } from 'app/store/services/UsersSlice';
import { AxiosRequestConfig } from 'axios';
import axios from '../../axiosApi';
import { useAppSelector } from '../../hooks';
import { initStore } from '../../store';

interface PropType {
  component: React.FC;
  isMatch?: boolean;
}

export const PrivateRoute: FC<PropType> = ({ component: Component, isMatch }) => {
  const user = useAppSelector((state) => state.users.user?.user);

  const navigate = useNavigate();

  useEffect(() => {
    axios.interceptors.request.use((config: AxiosRequestConfig) => {
      let parseData = null;
      const tokenInfo = localStorage.getItem('token');
      if (!tokenInfo) return config;

      parseData = JSON.parse(tokenInfo);
      if (parseData?.tokenData?.exp * 1000 < Date.now()) {
        localStorage.removeItem('token');
        navigate('/');
        return config;
      }

      const token: userInfo | null | undefined = initStore().getState().users.user;
      if (!token) return config;

      if (config.headers) config.headers['Authorization'] = token.tokenData.token;
      return config;
    });
  }, []);

  if (isMatch) {
    if (
      user?.roleId === process.env.REACT_APP_MAIN_ROLE ||
      user?.positionId?._id === process.env.REACT_APP_LEAD_POSITION
    ) {
      return <Component />;
    }
    return <Navigate to="/" />;
  }
  if (user && localStorage.getItem('token')) {
    return <Component />;
  }
  return <Navigate to="/loginUser" />;
};
