import { useEffect, useState } from 'react';
import { shallowEqual } from 'react-redux';
import { ConfigProvider, DatePicker, DatePickerProps, Form, Modal } from 'antd';
import rusLocale from 'antd/es/locale/ru_RU';
import { checkMonth, openNotification } from '../../../components/helpers/helpers';
import { useAppDispatch, useAppSelector } from '../../../hooks';
import { getHocService } from '../../../store/services/ClientAddHocServiceSlice';
import { addKpiFact, getKPIFactsOfEmployee } from '../../../store/services/KPIFactSlice';
import { getEmployee, getKpiFromEmployee } from '../../../store/services/KpiSlice';
import { IElements, KPIFactAddDrawer } from '../KPIFactAddDrawer.tsx/KPIFactAddDrawer';

interface Props {
  showModal: boolean;
  closeModal: () => void;
}

export const AddKPIFact = ({ showModal, closeModal }: Props) => {
  const { hocServiceList } = useAppSelector((state) => state.hocService, shallowEqual);
  const { employee } = useAppSelector((state) => state.kpi, shallowEqual);
  const { employeesKpiFacts } = useAppSelector((state) => state.kpiFact, shallowEqual);

  const [dateOfFact, setDateOfFact] = useState({
    year: 0,
    month: '',
  });

  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getKpiFromEmployee());
    dispatch(getEmployee());
    dispatch(getHocService());
    getKPIFactsOfEmployee();
  }, [dispatch]);

  const [showDrawer, setShowDrawer] = useState(false);
  const [form] = Form.useForm();

  const chosenDateHandler: DatePickerProps['onChange'] = (date) => {
    if (date) {
      setDateOfFact({
        year: date?.toDate().getFullYear(),
        month: checkMonth(date?.toDate().getMonth() + 1),
      });
    } else {
      return;
    }
  };

  const confirmToClosePeriod = () => {
    let num = 0;
    employeesKpiFacts.map((fact) => {
      if (fact.year === dateOfFact.year && fact.month === dateOfFact.month) {
        num += 1;
      } else {
        return;
      }
    });
    if (num > 0) {
      openNotification('error', 'bottomLeft', 'на выбранную дату уже имеется факт KPI');
      closeModal();
    } else {
      openDrawer();
      closeModal();
    }
  };

  const closeDrawer = () => {
    form?.resetFields();
    closeModal();
    setShowDrawer(false);
  };

  const saveKpiFactHandler = async (values: [IElements]) => {
    let hocServices = 0;
    hocServiceList.map((item) => {
      item.employeeId.map((employeeId) => {
        if (employeeId._id === employee[0]._id) {
          hocServices += item.sum;
        }
      });
    });

    const newKpiFact = {
      employeeId: employee[0]._id,
      year: dateOfFact.year,
      month: dateOfFact.month,
      kpiFactElements: values,
      hocPayments: hocServices * 0.25,
      totalSalary: employee[0].fixedSalary,
    };

    if (!null) {
      dispatch(addKpiFact(newKpiFact));
    }
    closeModal();
    form?.resetFields();
    closeDrawer();
    openNotification('success', 'bottomLeft', 'Факт KPI создан успешно!');
  };

  const onFinishFailed = () => {
    openNotification('error', 'bottomLeft', 'Не ввели обязательные поля');
  };

  const openDrawer = () => {
    form?.resetFields();
    setShowDrawer(true);
  };

  return (
    <>
      <Modal
        open={showModal}
        onCancel={closeModal}
        okText="Создать"
        onOk={() => confirmToClosePeriod()}
        cancelText="Отмена"
        width={600}
      >
        <div
          style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            flexDirection: 'column',
          }}
        >
          <h3 style={{ margin: '15px' }}> На какую дату вы хотите создать факт KPI? </h3>
          <ConfigProvider locale={rusLocale}>
            <DatePicker
              format={'DD.MM.YYYY'}
              onChange={chosenDateHandler}
              placeholder="Выберите дату"
            />
          </ConfigProvider>
        </div>
      </Modal>
      <KPIFactAddDrawer
        closeDrawer={closeDrawer}
        onFinishFailed={onFinishFailed}
        saveKpiFact={saveKpiFactHandler}
        form={form}
        isDrawer={showDrawer}
      />
    </>
  );
};
