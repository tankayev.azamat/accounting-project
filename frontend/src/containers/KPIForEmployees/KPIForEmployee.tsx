import { useEffect } from 'react';
import { shallowEqual } from 'react-redux';
import { Link } from 'react-router-dom';
import { Breadcrumb, Modal, Table, Tabs, Typography } from 'antd';
import { useAppDispatch, useAppSelector } from '../../hooks';
import { clearKpiState, getKpiFromEmployee } from '../../store/services/KpiSlice';
import { KPIFact } from './KPIFact';

import './KPIForEmployee.css';

export const KPIForEmployee = () => {
  const { Title } = Typography;
  const { kpiElements, percentForKpi, kpiError } = useAppSelector(
    (state) => state.kpi,
    shallowEqual
  );
  const user = useAppSelector((state) => state.users.user?.user, shallowEqual);
  const dispatch = useAppDispatch();

  const error = () => {
    Modal.error({
      title: 'Ошибка',
      content: kpiError?.message,
    });
  };

  useEffect(() => {
    if (kpiError) error();
  }, [error]);

  useEffect(() => {
    dispatch(getKpiFromEmployee());
    return () => {
      dispatch(clearKpiState());
    };
  }, [dispatch]);

  const columns = [
    {
      title: 'Вид',
      dataIndex: 'view',
      key: 'view',
    },
    {
      title: 'Цель',
      dataIndex: 'goal',
      key: 'goal',
    },
    {
      title: 'Описание',
      dataIndex: 'description',
      key: 'description',
    },
    {
      title: 'Примечание',
      dataIndex: 'comment',
      key: 'comment',
    },
    {
      title: 'Вес',
      dataIndex: 'weight',
      key: 'weight',
    },
  ];

  const items = [
    {
      label: 'Форма KPI',
      key: 'item-1',
      children: (
        <>
          <Title>КРІ для должности: {user?.positionId?.title}</Title>
          {kpiElements.length > 0 ? (
            <>
              <h3>Бонус за выполнение 100% КРI: {percentForKpi} %</h3>
              <Table
                rowKey={(record) => record.goal + Math.random() + Math.random()}
                columns={columns}
                dataSource={kpiElements}
              />
            </>
          ) : (
            <p className="textKpi">Нет созданных KPI</p>
          )}
        </>
      ),
    },
    { label: 'Факт KPI', key: 'item-2', children: <KPIFact /> },
  ];

  return (
    <div style={{ position: 'relative' }}>
      <Breadcrumb>
        <Breadcrumb.Item>
          <Link to="/">Главная</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>КРІ</Breadcrumb.Item>
      </Breadcrumb>
      <Tabs defaultActiveKey="1" items={items} />
    </div>
  );
};
