import { useEffect, useState } from 'react';
import { shallowEqual } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { Button, Select, Table, Typography } from 'antd';
import { ColumnsType } from 'antd/lib/table';
import { useAppDispatch, useAppSelector } from '../../hooks';
import {
  clearFactKpiState,
  getAllYears,
  getFactsOfYear,
  IKPIFact,
  setCurrentKpi,
  setSelectedYearHandler,
} from '../../store/services/KPIFactSlice';
import { getEmployee } from '../../store/services/KpiSlice';
import { AddKPIFact } from './AddKPIFact/AddKPIFact';

export const KPIFact = () => {
  const { years, kpiFactsOfYear, selectedYear } = useAppSelector(
    (state) => state.kpiFact,
    shallowEqual
  );

  const [showModal, setShowModal] = useState(false);
  const [optionForSelect, setOptionsForSelect] = useState([
    {
      value: 0,
      label: 0,
    },
  ]);
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getEmployee());
    dispatch(getAllYears());
    return () => {
      dispatch(clearFactKpiState());
    };
  }, [dispatch]);

  useEffect(() => {
    const options = [...new Set(years)].map((year) => {
      return { label: year, value: year };
    });
    setOptionsForSelect(options);
  }, [years]);

  const closeModal = () => {
    setShowModal(false);
  };

  const openModel = () => {
    setShowModal(true);
  };

  const selectedYearHandler = (value: number) => {
    dispatch(getFactsOfYear(value));
    dispatch(setSelectedYearHandler(value));
  };

  const navigate = useNavigate();

  const Information = (record: IKPIFact) => {
    dispatch(setCurrentKpi(record));
    navigate(`kpi/${record?._id}`);
  };

  const columns: ColumnsType<IKPIFact> = [
    {
      title: 'Год',
      dataIndex: 'year',
      key: 'year',
    },
    {
      title: 'Месяц',
      dataIndex: 'month',
      key: 'month',
    },
    {
      title: 'Дата и время создания',
      dataIndex: 'createdDate',
      key: 'createdDate',
    },
    {
      title: 'Сумма зарплаты',
      dataIndex: 'totalSalary',
      key: 'totalSalary',
      render: (_, record) => record.totalSalary.toLocaleString('ru'),
    },
    {
      title: 'Статус',
      dataIndex: 'status',
      key: 'status',
    },
    {
      key: 'more',
      title: 'Действие',
      render: (_, record) => {
        return (
          <Button onClick={() => Information(record)} type="primary">
            {' '}
            Подробнее
          </Button>
        );
      },
    },
  ];

  return (
    <>
      <Typography style={{ fontWeight: '500', marginBottom: '10px' }}>Выберите год</Typography>
      <Select
        style={{ width: '100px', marginBottom: '20px' }}
        value={selectedYear}
        options={optionForSelect}
        size="large"
        onChange={selectedYearHandler}
        placeholder="Выберите год"
      />
      <Button
        style={{
          margin: '37px',
          position: 'absolute',
          top: '0px',
          left: '120px',
          marginBottom: '20px',
        }}
        type="primary"
        onClick={() => openModel()}
      >
        Создать
      </Button>
      {kpiFactsOfYear.length ? (
        <Table
          dataSource={kpiFactsOfYear}
          rowKey={() => Math.random() + Math.random()}
          columns={columns}
        />
      ) : (
        <p
          style={{
            fontWeight: 'bold',
            fontSize: '16px',
            textAlign: 'center',
            marginTop: '100px',
            fontStyle: 'italic',
          }}
        >
          Выберите данные из списка либо создайте новый факт KPI
        </p>
      )}
      <AddKPIFact showModal={showModal} closeModal={closeModal} />
    </>
  );
};
