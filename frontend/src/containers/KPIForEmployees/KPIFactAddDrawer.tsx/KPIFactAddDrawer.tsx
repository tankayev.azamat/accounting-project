import { useEffect, useState } from 'react';
import { shallowEqual } from 'react-redux';
import { Button, Drawer, Form, Input, InputNumber, Modal, Typography } from 'antd';
import { FormInstance } from 'antd/es/form/Form';
import { useAppDispatch, useAppSelector } from '../../../hooks';
import { clearKpiState, getKpiFromEmployee } from '../../../store/services/KpiSlice';

export interface ModifiedIKpiFacts {
  factElements: [
    {
      view: string;
      goal: string;
      description: string;
      comment: string;
      weight: number;
      factWeight: number | null;
      employeeComment: string | null;
    }
  ];
}
interface Props {
  closeDrawer?: () => void;
  onFinishFailed?: () => void;
  saveKpiFact?: (value: [IElements]) => void;
  form?: FormInstance;
  isDrawer?: boolean;
  totalPercent?: number;
}

export interface IElements {
  view: string;
  goal: string;
  description: string;
  comment: string;
  weight: number;
  factWeight: number | null;
  employeeComment: string | null;
}

export const KPIFactAddDrawer = ({
  closeDrawer,
  onFinishFailed,
  saveKpiFact,
  form,
  isDrawer,
}: Props) => {
  const { kpiElements, kpiError } = useAppSelector((state) => state.kpi, shallowEqual);
  const dispatch = useAppDispatch();

  const [data, setData] = useState(Array<IElements>);

  const error = () => {
    Modal.error({
      title: 'Ошибка',
      content: kpiError?.message,
    });
  };

  useEffect(() => {
    if (kpiError) error();
  }, [error]);

  useEffect(() => {
    dispatch(getKpiFromEmployee());
    if (kpiElements) {
      const newElements: IElements[] = [];
      kpiElements.map((elementsObject) => {
        const elements = {
          view: elementsObject.view,
          goal: elementsObject.goal,
          description: elementsObject.description,
          comment: elementsObject.comment,
          weight: elementsObject.weight,
          factWeight: 0,
          employeeComment: '',
        };
        newElements.push(elements);
      });
      setData(newElements);
    }
    return () => {
      dispatch(clearKpiState());
    };
  }, [dispatch]);

  return (
    <Drawer
      title="Добавление факта KPI за месяц"
      placement="top"
      onClose={closeDrawer}
      open={isDrawer}
      height={670}
    >
      <Form
        form={form}
        name="basic"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        initialValues={{ remember: true }}
        onFinish={saveKpiFact}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item wrapperCol={{ offset: 1, span: 16 }}>
          <Button type="primary" htmlType="submit">
            Сохранить
          </Button>
          <Button
            style={{ margin: '15px' }}
            onClick={closeDrawer}
            value="large"
            type="primary"
            danger
          >
            Отмена
          </Button>
        </Form.Item>
        <Form.List name="kpiFactElements" initialValue={data}>
          {(fields) => (
            <>
              {fields.map(({ key, name, ...restField }) => {
                return (
                  <span
                    key={key}
                    style={{
                      display: 'flex',
                      justifyContent: 'space-around',
                      marginBottom: '30px',
                    }}
                  >
                    <Form.Item {...restField} name={[name, 'goal']}>
                      <Typography
                        style={{
                          width: '150px',
                          height: '50px',
                          marginRight: '30px',
                          fontWeight: '500',
                        }}
                      >
                        {data[name].goal}
                      </Typography>
                    </Form.Item>
                    <Form.Item {...restField} name={[name, 'comment']}>
                      <Typography
                        style={{
                          width: '300px',
                          height: '50px',
                        }}
                      >
                        {data[name].comment}
                      </Typography>
                    </Form.Item>
                    <Form.Item {...restField} name={[name, 'weight']}>
                      <Typography style={{ width: '100px', fontWeight: '500' }}>
                        {data[name].weight}%
                      </Typography>
                    </Form.Item>
                    <Form.Item {...restField} name={[name, 'factWeight']}>
                      <InputNumber
                        min={0}
                        max={100}
                        style={{
                          width: '100px',
                          borderColor: 'rgb(115, 161, 240)',
                        }}
                        name="factWeight"
                      />
                    </Form.Item>
                    <Form.Item {...restField} name={[name, 'employeeComment']}>
                      <Input
                        style={{
                          width: '250px',
                          height: '50px',
                        }}
                        placeholder="введите комментарий"
                        name="employeeComment"
                      />
                    </Form.Item>
                  </span>
                );
              })}
            </>
          )}
        </Form.List>
      </Form>
    </Drawer>
  );
};
