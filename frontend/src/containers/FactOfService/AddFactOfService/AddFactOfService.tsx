import { useEffect, useState } from 'react';
import { shallowEqual } from 'react-redux';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { Breadcrumb, Button, Modal, Table, Typography } from 'antd';
import { ColumnsType } from 'antd/es/table';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import {
  checkMonth,
  numberWithCommas,
  openNotification,
} from '../../../components/helpers/helpers';
import { useAppDispatch, useAppSelector } from '../../../hooks';
import { IClient } from '../../../store/services/ClientsListSlice';
import {
  ActiveClientsFactOfService,
  closePeriodOfCurrentMonth,
  getCurrentMonth,
  resetNotification,
  setCurrentClient,
} from '../../../store/services/FactForMonthSlice';
import { AddFactOfServicePaymentDrawer } from '../AddFactOfServicePaymentDrawer/AddFactOfServicePaymentDrawer';

export const AddFactOfService = () => {
  const { Title } = Typography;
  const [showModalFet, setShowModal] = useState(false);
  const { currentMonth, loading } = useAppSelector((state) => state.factForMonth);
  const [totalFactFee, setTotalFactFee] = useState(0);
  const [disableButton, setDisableButton] = useState(true);
  const params = useParams();
  const { successfully, message, hasErrors, edited } = useAppSelector(
    (state) => state.factForMonth,
    shallowEqual
  );
  const navigate = useNavigate();
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getCurrentMonth(params.id));
  }, [dispatch]);
  useEffect(() => {
    dispatch(getCurrentMonth(params.id));
    dispatch(resetNotification());
  }, [edited]);
  useEffect(() => {
    if (successfully) {
      openNotification('success', 'bottomLeft', message);
      setTimeout(() => {
        dispatch(resetNotification());
      }, 500);
    }
    if (hasErrors) {
      openNotification('error', 'bottomLeft', message);
      setTimeout(() => {
        dispatch(resetNotification());
      }, 500);
    }
  }, [successfully, hasErrors]);

  const showDrawerHandler = (record: ActiveClientsFactOfService) => {
    dispatch(setCurrentClient(record));
    setShowModal(!showModalFet);
  };

  const closeModal = () => {
    setShowModal(!showModalFet);
  };

  useEffect(() => {
    const sum = currentMonth?.activeClientsFactOfService
      .map((client) => client.factTotalSum)
      .reduce((acc, number) => acc + number);
    if (sum !== undefined) setTotalFactFee(sum);
    const isAllAdded = currentMonth?.activeClientsFactOfService.filter(
      (client) => client.status === 'Ожидает'
    );
    if (isAllAdded?.length === 0) {
      setDisableButton(false);
    }
    if (isAllAdded?.length !== 0) {
      setDisableButton(true);
    }
  }, [currentMonth]);

  const columns: ColumnsType<ActiveClientsFactOfService> = [
    {
      title: 'Наименование',
      dataIndex: 'clientId',
      key: 'clientId',
      render: (recocrd: IClient) => {
        return recocrd.name;
      },
    },
    {
      title: 'Абонплата',
      dataIndex: 'totalSum',
      key: 'totalSum',
      render: (_, record) => record.totalSum.toLocaleString('ru'),
    },
    {
      title: 'Сумма Факт. сопровождения',
      dataIndex: 'factTotalSum',
      key: 'factTotalSum',
      render: (_, record) => record.factTotalSum.toLocaleString('ru'),
    },
    {
      title: 'Разница',
      dataIndex: 'difference',
      key: 'difference',
      render: (_, record) => record.difference.toLocaleString('ru'),
    },
    {
      title: 'Статус',
      dataIndex: 'status',
      key: 'status',
    },
    {
      key: 'more',
      title: 'Действие',
      render: (record: ActiveClientsFactOfService) => {
        return (
          <>
            {currentMonth?.status === 'Обработан' ? (
              <>
                <Button onClick={() => showDrawerHandler(record)} type="primary">
                  Подробнее
                </Button>
              </>
            ) : (
              <>
                <Button onClick={() => showDrawerHandler(record)} type="primary">
                  Добавить
                </Button>
              </>
            )}
          </>
        );
      },
    },
  ];

  const closePeriodHandler = () => {
    dispatch(closePeriodOfCurrentMonth(currentMonth?._id));
    setDisableButton(true);
    navigate('/fact_of_service');
  };

  const confirmToClosePeriod = () => {
    Modal.confirm({
      title: 'Закрытие Периода',
      icon: <ExclamationCircleOutlined />,
      content: 'Хотите закрыть период за месяц?',
      onOk: () => closePeriodHandler(),
      okText: 'Да',
      cancelText: 'Нет',
    });
  };

  return (
    <>
      <Breadcrumb>
        <Breadcrumb.Item>
          <Link to="/">Главная</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>
          <Link to="/fact_of_service">Оказанные услуги</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>Факт за месяц</Breadcrumb.Item>
      </Breadcrumb>
      <Title>Создание факта оказания услуг</Title>
      <div
        style={{
          display: 'flex',
          alignItems: 'flex-start',
          justifyContent: 'space-between',
        }}
      >
        <div
          style={{
            position: 'relative',
            display: 'flex',
            alignItems: 'flex-start',
            justifyContent: 'flex-start',
          }}
        >
          <div>
            <p>
              <b>Год: {currentMonth?.year.toString()}</b>
            </p>
            <p>
              <b>Месяц: {checkMonth(currentMonth?.month)}</b>
            </p>
          </div>
          {currentMonth?.status === 'Обработан' ? (
            <>
              <h3 style={{ marginLeft: '15px', color: 'green' }}> Выбранный период закрыт!</h3>
            </>
          ) : (
            <Button
              style={{ margin: '15px', position: 'absolute', top: '-15px', left: '240px' }}
              type="primary"
              disabled={disableButton}
              danger
              onClick={() => confirmToClosePeriod()}
            >
              Закрыть период
            </Button>
          )}
        </div>
        <div>
          <Typography>
            Итог фактического сопровождения сотрудников по всем клиентам:{' '}
            {numberWithCommas(totalFactFee)}
          </Typography>
        </div>
      </div>
      <Table
        loading={loading}
        columns={columns}
        style={{ marginTop: '15px' }}
        dataSource={currentMonth?.activeClientsFactOfService}
        rowKey={(record) => record.clientId._id}
      />
      <AddFactOfServicePaymentDrawer closeModal={closeModal} showModal={showModalFet} />
    </>
  );
};
