import { useState } from 'react';
import { ConfigProvider, DatePicker, DatePickerProps, Modal } from 'antd';
import rusLocale from 'antd/es/locale/ru_RU';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import { useAppDispatch } from '../../../hooks';
import { setNewFactForMonth, setSelectedYear } from '../../../store/services/FactForMonthSlice';
interface Props {
  showModal: boolean;
  closeModal: () => void;
}
export const AddNewFact = ({ showModal, closeModal }: Props) => {
  const [dateOfFact, setDateOfFact] = useState({
    year: 0,
    month: 0,
  });
  const dispatch = useAppDispatch();
  const chosenDateHandler: DatePickerProps['onChange'] = (date) => {
    if (date) {
      setDateOfFact({
        year: date.toDate().getFullYear(),
        month: date.toDate().getMonth() + 1,
      });
    } else {
      return;
    }
  };
  const addNewFactForMonth = () => {
    dispatch(setSelectedYear(dateOfFact?.year));
    dispatch(setNewFactForMonth({ year: dateOfFact.year, month: dateOfFact.month }));
    closeModal();
  };
  const confirmToClosePeriod = () => {
    Modal.confirm({
      title: 'Создание Факта оказания услуг',
      icon: <ExclamationCircleOutlined />,
      content: 'Хотите создать Факт за месяц?',
      onOk: () => addNewFactForMonth(),
      okText: 'Да',
      cancelText: 'Нет',
    });
  };
  return (
    <>
      <Modal
        open={showModal}
        onCancel={closeModal}
        okText="Создать"
        onOk={confirmToClosePeriod}
        cancelText="Отмена"
        width={600}
      >
        <div
          style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            flexDirection: 'column',
          }}
        >
          <h3 style={{ margin: '15px' }}> На какую дату вы хотите создать Факт оказания услуг? </h3>
          <ConfigProvider locale={rusLocale}>
            <DatePicker
              format={'DD.MM.YYYY'}
              onChange={chosenDateHandler}
              placeholder="Выберите дату"
            />
          </ConfigProvider>
        </div>
      </Modal>
    </>
  );
};
