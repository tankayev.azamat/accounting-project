import React, { ChangeEvent, useEffect, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import {
  Breadcrumb,
  Button,
  Col,
  Form,
  Modal,
  Row,
  Select,
  SelectProps,
  Tag,
  Typography,
} from 'antd';
import { ExclamationCircleOutlined, SearchOutlined } from '@ant-design/icons';
import type { DatePickerProps } from 'antd';
import type { CustomTagProps } from 'rc-select/lib/BaseSelect';
import { openNotification } from '../../../components/helpers/helpers';
import { useAppDispatch, useAppSelector } from '../../../hooks';
import {
  addEmployee,
  clearEmplyeesArrayState,
  deleteEmployee,
  getEmployeeFilteredStatus,
  getEmployees,
  getEmployeeSubordinates,
  IEmployee,
  resetNotificationStatus,
  setCurrentEmployee,
} from '../../../store/services/EmployeesListSlice';
import { EmployeeAddDrawer } from '../EmployeesList/EmployeeAddDrawer/EmployeeAddDrawer';
import { EmployeesTableElements } from './EmployeesTableElements/EmployeesTableElements';

export const EmployeesList: React.FC = () => {
  const { employees, open, addedSuccessfully, hasErrors, message, loading } = useAppSelector(
    (state) => state.employees
  );
  const [statesFilterEmployee, setStatesFilterEmployee] = useState<string[]>([]);
  const user = useAppSelector((state) => state.users.user?.user);
  const [newEmployee, setNewEmployee] = useState({
    firstName: '',
    lastName: '',
    position: '',
    status: '',
    fixedSalary: 0,
    headId: null,
    dateOfDismissal: null,
    dateOfEmployment: new Date(),
  });

  const dispatch = useAppDispatch();
  const { Title } = Typography;
  const [isDrawer, setIsDrawer] = useState(false);
  const [form] = Form.useForm();
  const navigate = useNavigate();

  useEffect(() => {
    if (addedSuccessfully) {
      openNotification('success', 'bottomLeft', message);
      setTimeout(() => {
        dispatch(resetNotificationStatus());
      }, 1000);
    }
    if (hasErrors) {
      openNotification('error', 'bottomLeft', message);
      setTimeout(() => {
        dispatch(resetNotificationStatus());
      }, 1000);
    }
  }, [addedSuccessfully, hasErrors]);

  useEffect(() => {
    if (user?.roleId === process.env.REACT_APP_MAIN_ROLE) {
      dispatch(getEmployeeFilteredStatus(statesFilterEmployee));
    } else {
      dispatch(getEmployeeSubordinates());
    }
    return () => {
      dispatch(clearEmplyeesArrayState());
    };
  }, [dispatch]);

  const showDrawerMenu = () => {
    setIsDrawer(true);
  };

  const inputChangeHandler = (e: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setNewEmployee((prevState) => ({ ...prevState, [name]: value.trim() }));
  };

  const selectStatusHandler = (value: string) => {
    const copyNewEmployee = { ...newEmployee };
    copyNewEmployee.status = value;
    setNewEmployee(copyNewEmployee);
  };

  const selectPositionHandler = (value: string) => {
    const copyNewEmployee = { ...newEmployee };
    copyNewEmployee.position = value;
    setNewEmployee(copyNewEmployee);
  };

  const selectHeadIdHandler = (value: any) => {
    const copyNewEmployee = { ...newEmployee };
    copyNewEmployee.headId = value;
    setNewEmployee(copyNewEmployee);
  };

  const closeDrawerHandler = () => {
    setIsDrawer(false);
    form.resetFields([
      'firstName',
      'lastName',
      'position',
      'status',
      'fixedSalary',
      'headId',
      'dateOfDismissal',
      'headId',
      'dateOfEmployment',
    ]);
    setNewEmployee({
      firstName: '',
      lastName: '',
      position: '',
      status: '',
      fixedSalary: 0,
      headId: null,
      dateOfDismissal: null,
      dateOfEmployment: new Date(),
    });
  };

  const onFinishFailed = () => {
    openNotification('error', 'bottomLeft', 'Не ввели обязательные поля');
  };

  const sendNewEmployeeHandler = async () => {
    const obj = { ...newEmployee };
    await dispatch(addEmployee(obj));
    await dispatch(getEmployees());
    closeDrawerHandler();
  };

  const onDateChange: DatePickerProps['onChange'] = (date) => {
    const copyNewEmployee = { ...newEmployee };
    copyNewEmployee.dateOfEmployment = date ? date.toDate() : new Date();
    setNewEmployee(copyNewEmployee);
  };

  const InformationEmployee = (record: IEmployee) => {
    dispatch(setCurrentEmployee(record));
    navigate(`/employees_list/${record?._id}`);
  };

  const deleteHandler = (record: IEmployee) => {
    const onOk = () => {
      const payload = {
        id: record._id,
        status: 'Удалён',
      };
      dispatch(deleteEmployee(payload));
      open === false;
    };

    const hideModal = () => {
      open === false;
    };

    return Modal.confirm({
      title: 'Предупреждение',
      icon: <ExclamationCircleOutlined />,
      content: 'Вы действительно хотите удалить сотрудника?',
      okText: 'OK',
      cancelText: 'Отменить',
      open: open,
      onOk: onOk,
      onCancel: hideModal,
    });
  };

  const filterChange = (value: string[]) => {
    setStatesFilterEmployee(value);
  };

  const filterClickHandler = () => {
    return dispatch(getEmployeeFilteredStatus(statesFilterEmployee));
  };

  const options: SelectProps['options'] = [];
  options.push(
    {
      value: 'Работает',
      label: 'Работает',
    },
    {
      value: 'Не работает',
      label: 'Не работает',
    },
    {
      value: 'Удалён',
      label: 'Удалён',
    }
  );

  const tagRender = (props: CustomTagProps) => {
    const { label, closable, onClose } = props;

    const onPreventMouseDown = (event: React.MouseEvent<HTMLSpanElement>) => {
      event.preventDefault();
      event.stopPropagation();
    };
    return (
      <Tag
        color="red"
        onMouseDown={onPreventMouseDown}
        closable={closable}
        onClose={onClose}
        style={{ marginRight: 3 }}
      >
        {label}
      </Tag>
    );
  };
  return (
    <>
      <Breadcrumb>
        <Breadcrumb.Item>
          <Link to="/">Главная</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>Сотрудники</Breadcrumb.Item>
      </Breadcrumb>
      <Title>Сотрудники</Title>
      {user?.roleId === process.env.REACT_APP_MAIN_ROLE ? (
        <>
          <Select
            mode="tags"
            tagRender={tagRender}
            style={{ width: '25%' }}
            placeholder="Поиск по статусу"
            options={options}
            onChange={filterChange}
          />
          <Button
            style={{ marginLeft: '15px' }}
            onClick={() => filterClickHandler()}
            htmlType="submit"
            type="primary"
            shape="circle"
            icon={<SearchOutlined />}
          />
        </>
      ) : null}
      <Row>
        <Col xs={24} md={{ span: 16, offset: 2 }}>
          <Col style={{ display: 'flex', justifyContent: 'end' }}>
            {user?.roleId === process.env.REACT_APP_MAIN_ROLE ? (
              <Button
                style={{ marginBottom: 20 }}
                onClick={showDrawerMenu}
                value="large"
                type="primary"
              >
                Добавить Сотрудника
              </Button>
            ) : null}
          </Col>
          <EmployeesTableElements
            deleteHandler={deleteHandler}
            showDrawer={InformationEmployee}
            allEmployees={employees}
            spinner={loading}
          />
        </Col>
        <EmployeeAddDrawer
          closeDrawerHandler={closeDrawerHandler}
          selectPositionHandler={selectPositionHandler}
          selectHeadIdHandler={selectHeadIdHandler}
          selectStatusHandler={selectStatusHandler}
          sendNewEmployeeHandler={sendNewEmployeeHandler}
          onDateChange={onDateChange}
          inputChangeHandler={inputChangeHandler}
          newEmployee={newEmployee}
          onFinishFailed={onFinishFailed}
          form={form}
          isDrawer={isDrawer}
        />
      </Row>
    </>
  );
};
