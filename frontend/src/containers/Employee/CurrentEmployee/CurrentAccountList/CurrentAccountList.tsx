import { ChangeEvent, useEffect, useState } from 'react';
import { shallowEqual } from 'react-redux';
import { useParams } from 'react-router-dom';
import { Form } from 'antd';
import { openNotification } from '../../../../components/helpers/helpers';
import { useAppDispatch, useAppSelector } from '../../../../hooks';
import { correctPassword, getUserEmployeeId } from '../../../../store/services/RegisterUserSlice';
import { CurrentEditAccount } from '../CurrentEditAccount/CurrentEditAccount';

interface Props {
  isEditDrawer: boolean;
  setIsDrawer: (value: React.SetStateAction<boolean>) => void;
}

export function CurrentAccountList({ isEditDrawer, setIsDrawer }: Props) {
  const { userData } = useAppSelector((state) => state.userRegister, shallowEqual);
  const [form] = Form.useForm();
  const params = useParams();
  const dispatch = useAppDispatch();

  const [EditAccountState, setEditAccountState] = useState({
    login: userData?.login,
    password: '',
  });
  useEffect(() => {
    setEditAccountState({ login: userData?.login, password: '' });
  }, [userData]);
  const sendEditAccountHandler = () => {
    const payload = {
      _id: userData ? userData?._id : '',
      password: EditAccountState.password,
    };
    dispatch(correctPassword(payload));
    const objectWithId = {
      id: params.id,
    };
    dispatch(getUserEmployeeId(objectWithId));
    closeEditAccountDrawerHandler();
  };

  useEffect(() => {
    const payload = {
      id: params.id,
    };
    dispatch(getUserEmployeeId(payload));
  }, [dispatch]);

  const inputEditAccountChangeHandler = (e: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setEditAccountState((prevState) => ({ ...prevState, [name]: value }));
  };
  const closeEditAccountDrawerHandler = () => {
    setIsDrawer(false);
    form.resetFields(['login', 'password']);
    setEditAccountState({
      login: '',
      password: '',
    });
  };

  const onFinishFailed = () => {
    openNotification('warning', 'bottomLeft', 'Пожалуйста, введите обязательные поля');
  };

  return (
    <CurrentEditAccount
      closeEditAccountDrawerHandler={closeEditAccountDrawerHandler}
      isEditAccountDrawer={isEditDrawer}
      formInstance={form}
      sendEditAccountHandler={sendEditAccountHandler}
      onEditFinishFailed={onFinishFailed}
      editAccount={EditAccountState}
      inputEditAccountChangeHandler={inputEditAccountChangeHandler}
    />
  );
}
