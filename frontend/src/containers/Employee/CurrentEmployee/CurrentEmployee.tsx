import { ChangeEvent, useEffect, useState } from 'react';
import { shallowEqual } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import { Breadcrumb, Button, DatePickerProps, Form, Modal, Tabs } from 'antd';
import { EditOutlined, ExclamationCircleOutlined } from '@ant-design/icons';
import { openNotification } from '../../../components/helpers/helpers';
import { useAppDispatch, useAppSelector } from '../../../hooks';
import {
  editCurrentEmployee,
  getEmployeesId,
  resetNotificationStatus,
} from '../../../store/services/EmployeesListSlice';
import { IPosition } from '../../../store/services/PositionSlice';
import { CurrentEmployeeEdit } from './CurrentEmployeeEdit/CurrentEmployeeEdit';
import { CurrentEmployeeInfo } from './CurrentEmployeeInfo/CurrentEmployeeInfo';

export const CurrentEmployee = () => {
  const dispatch = useAppDispatch();
  const params = useParams();
  const user = useAppSelector((state) => state.users.user?.user);
  const { currentEmployee, open, editedSuccessfully, hasErrors, message } = useAppSelector(
    (state) => state.employees,
    shallowEqual
  );
  const [isDrawer, setIsDrawer] = useState(false);
  const [form] = Form.useForm();
  const [editEmployee, setEditEmployee] = useState({
    firstName: currentEmployee?.firstName,
    lastName: currentEmployee?.lastName,
    position: currentEmployee?.position,
    status: currentEmployee?.status,
    fixedSalary: currentEmployee?.fixedSalary,
    headId: currentEmployee?.headId,
    dateOfDismissal: currentEmployee?.dateOfDismissal,
    dateOfEmployment: currentEmployee?.dateOfEmployment,
  });
  useEffect(() => {
    if (hasErrors) {
      openNotification('error', 'bottomLeft', message);
      setTimeout(() => {
        dispatch(resetNotificationStatus());
      }, 1000);
    }
    if (editedSuccessfully) {
      openNotification('success', 'bottomLeft', message);
      setTimeout(() => {
        dispatch(resetNotificationStatus());
      }, 1000);
    }
  }, [editedSuccessfully, hasErrors]);

  const sendEditEmployeeHandler = () => {
    const onOk = () => {
      const payloadEdit = {
        id: params.id,
        data: {
          firstName: editEmployee?.firstName,
          lastName: editEmployee?.lastName,
          position: editEmployee.position,
          status: editEmployee?.status,
          fixedSalary: editEmployee?.fixedSalary,
          headId: editEmployee?.headId,
          dateOfDismissal: editEmployee?.dateOfDismissal,
          dateOfEmployment: editEmployee?.dateOfEmployment,
        },
      };
      dispatch(editCurrentEmployee(payloadEdit));

      open === false;
      closeDrawerHandler();
    };

    const hideModal = () => {
      open === false;
    };

    Modal.confirm({
      title: 'Предупреждение',
      icon: <ExclamationCircleOutlined />,
      content: 'Вы действительно хотите изменить данные сотрудника?',
      okText: 'OK',
      cancelText: 'Отменить',
      open: open,
      onOk: onOk,
      onCancel: hideModal,
    });
  };

  useEffect(() => {
    dispatch(getEmployeesId(params.id));
  }, [dispatch]);

  const showDrawerMenu = () => {
    setIsDrawer(true);
  };

  const closeDrawerHandler = () => {
    setIsDrawer(false);
  };

  const items = [
    { label: 'Информация', key: 'item-1', children: <CurrentEmployeeInfo /> },
    { label: 'История', key: 'item-2', children: 'История отсутствует' },
  ];

  const onFinishFailed = () => {
    openNotification('warning', 'bottomLeft', 'Пожалуйста, заполните обязательные поля!');
  };

  const inputChangeHandler = (event: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event?.target;
    setEditEmployee({ ...editEmployee, [name]: value.trim() });
  };

  const onDateChange: DatePickerProps['onChange'] = (date) => {
    const copy = { ...editEmployee };
    copy.dateOfEmployment = date ? date.toDate() : new Date();
    setEditEmployee(copy);
  };

  const onRegDateChange: DatePickerProps['onChange'] = (date) => {
    const copy = { ...editEmployee };
    copy.dateOfDismissal = date ? date.toDate() : new Date();
    setEditEmployee(copy);
  };

  const selectPositionHandler = (value: IPosition) => {
    const copy = { ...editEmployee };
    copy.position = value;
    setEditEmployee(copy);
  };

  const selectHeadIdHandler = (value: string) => {
    const copy = { ...editEmployee };
    copy.headId = value;
    setEditEmployee(copy);
  };

  const selectStatusHandler = (value: string) => {
    const copy = { ...editEmployee };
    copy.status = value;
    setEditEmployee(copy);
  };

  return (
    <>
      <div style={{ display: 'flex', justifyContent: 'space-between' }}>
        <Breadcrumb>
          <Breadcrumb.Item>
            <Link to="/">Главная</Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item>
            <Link to="/employees_list">Сотрудники</Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item>Информация о сотруднике</Breadcrumb.Item>
        </Breadcrumb>
        {currentEmployee?.status !== 'Удалён' &&
        user?.roleId === process.env.REACT_APP_MAIN_ROLE ? (
          <Button
            onClick={showDrawerMenu}
            type="primary"
            icon={<EditOutlined style={{ fontSize: '20px' }} />}
            size="large"
          />
        ) : null}
      </div>
      <Tabs defaultActiveKey="1" items={items} />
      <CurrentEmployeeEdit
        closeDrawerHandler={closeDrawerHandler}
        isDrawer={isDrawer}
        form={form}
        selectHeadIdHandler={selectHeadIdHandler}
        selectPositionHandler={selectPositionHandler}
        selectStatusHandler={selectStatusHandler}
        sendEditEmployeeHandler={sendEditEmployeeHandler}
        onRegDateChange={onRegDateChange}
        onDateChange={onDateChange}
        onFinishFailed={onFinishFailed}
        inputChangeHandler={inputChangeHandler}
        currentEmployee={currentEmployee}
      />
    </>
  );
};
