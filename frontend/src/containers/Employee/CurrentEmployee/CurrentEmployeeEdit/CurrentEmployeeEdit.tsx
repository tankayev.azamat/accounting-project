import { ChangeEvent, useEffect } from 'react';
import { Button, DatePicker, DatePickerProps, Drawer, Form, Input, Select, Space } from 'antd';
import { FormInstance } from 'antd/es/form/Form';
import moment from 'moment';
import { useAppDispatch, useAppSelector } from '../../../../hooks';
import { getEmployees, IEmployee } from '../../../../store/services/EmployeesListSlice';
import { getPositionById, getPositions, IPosition } from '../../../../store/services/PositionSlice';

interface Props {
  closeDrawerHandler?: () => void;
  onDateChange?: DatePickerProps['onChange'];
  inputChangeHandler?: (e: ChangeEvent<HTMLInputElement>) => void;
  onFinishFailed?: () => void;
  sendEditEmployeeHandler?: () => void;
  form?: FormInstance;
  isDrawer?: boolean;
  selectStatusHandler: (value: string) => void;
  selectPositionHandler: (value: IPosition) => void;
  selectHeadIdHandler: (value: string) => void;
  currentEmployee: IEmployee | null;
  onRegDateChange: DatePickerProps['onChange'];
}

export const CurrentEmployeeEdit = ({
  closeDrawerHandler,
  isDrawer,
  form,
  sendEditEmployeeHandler,
  onFinishFailed,
  inputChangeHandler,
  currentEmployee,
  onDateChange,
  onRegDateChange,
  selectStatusHandler,
  selectPositionHandler,
  selectHeadIdHandler,
}: Props) => {
  const { Option } = Select;
  const { positions, position } = useAppSelector((state) => state.positions);
  const { employees } = useAppSelector((state) => state.employees);

  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getPositions());
    currentEmployee?.position._id ? dispatch(getPositionById(currentEmployee?.position._id)) : null;
    dispatch(getEmployees());
  }, [dispatch, currentEmployee?.position]);
  return (
    <Drawer
      title="Изменить данные сотрудника"
      placement="right"
      onClose={closeDrawerHandler}
      open={isDrawer}
      width={400}
    >
      <Space direction="vertical">
        <Form
          form={form}
          name="basic"
          labelCol={{ span: 11 }}
          wrapperCol={{ span: 25 }}
          initialValues={{ remember: false }}
          onFinish={sendEditEmployeeHandler}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            label="Фамилия"
            name="lastName"
            rules={[
              { required: true, message: 'Пожалуйста, введите фамилию' },
              {
                pattern: new RegExp(/^[a-zа-яё\s]+$/iu),
                message: 'Фамилия должна включать только буквы',
              },
              {
                pattern: new RegExp(/^\S/),
                message: 'Поле не должно содержать лишних пробелов',
              },
            ]}
            initialValue={currentEmployee?.lastName}
          >
            <Input name="lastName" onChange={inputChangeHandler} />
          </Form.Item>
          <Form.Item
            label="Имя"
            name="firstName"
            rules={[
              { required: true, message: 'Пожалуйста, введите имя' },
              {
                pattern: new RegExp(/^[a-zа-яё\s]+$/iu),
                message: 'Имя должно включать только буквы',
              },
              {
                pattern: new RegExp(/^\S/),
                message: 'Поле не должно содержать лишних пробелов',
              },
            ]}
            initialValue={currentEmployee?.firstName}
          >
            <Input name="firstName" onChange={inputChangeHandler} />
          </Form.Item>
          <Form.Item
            label="Должность"
            name="position"
            rules={[{ required: true, message: 'Пожалуйста, выберите должность' }]}
            initialValue={
              currentEmployee?.position.title ? currentEmployee?.position.title : undefined
            }
          >
            <Select
              style={{ display: 'block', borderRadius: '9px' }}
              onChange={selectPositionHandler}
            >
              {positions.map((position) => (
                <Option key={position._id} value={position._id}>
                  {position.title}
                </Option>
              ))}
            </Select>
          </Form.Item>
          {position?.title === 'Бухгалтер' || position?.title === 'Помощник бухгалтера' ? (
            <Form.Item
              label="Начальник"
              name="headId"
              rules={[{ required: true, message: 'Пожалуйста, выберите начальника' }]}
              initialValue={currentEmployee?.headId}
            >
              <Select
                style={{ display: 'block', borderRadius: '9px' }}
                onChange={selectHeadIdHandler}
              >
                {employees
                  .filter((item) => item._id !== currentEmployee?._id)
                  .map((employee) => (
                    <Option key={employee._id} value={employee._id}>
                      {employee.lastName} {employee.firstName}
                    </Option>
                  ))}
              </Select>
            </Form.Item>
          ) : (
            ''
          )}
          <Form.Item
            label="Оклад"
            name="fixedSalary"
            rules={[
              { required: true, message: 'Пожалуйста, введите сумму оклада' },
              {
                pattern: new RegExp(/^[0-9]{1,}$/),
                message: 'Оклад должен состоять только из цифр',
              },
              {
                pattern: new RegExp(/^\S/),
                message: 'Поле не должно содержать лишних пробелов',
              },
            ]}
            initialValue={currentEmployee?.fixedSalary}
          >
            <Input name="fixedSalary" onChange={inputChangeHandler} />
          </Form.Item>
          <Form.Item
            label="Дата принятия"
            name="dateOfEmployment"
            rules={[{ required: true, message: 'Пожалуйста, выберите дату' }]}
            initialValue={
              currentEmployee?.dateOfEmployment
                ? moment(currentEmployee?.dateOfEmployment)
                : undefined
            }
          >
            <DatePicker format={'DD.MM.YYYY'} onChange={onDateChange} />
          </Form.Item>
          <Form.Item
            label="Дата увольнения"
            name="dateOfDismissal"
            initialValue={
              currentEmployee?.dateOfDismissal
                ? moment(currentEmployee?.dateOfDismissal)
                : undefined
            }
          >
            <DatePicker format={'DD.MM.YYYY'} onChange={onRegDateChange} />
          </Form.Item>
          <Form.Item
            label="Статус"
            name="status"
            rules={[{ required: true, message: 'Пожалуйста, выберите статус' }]}
            initialValue={currentEmployee?.status}
          >
            <Select
              style={{ display: 'block', borderRadius: '9px' }}
              onChange={selectStatusHandler}
            >
              <Option value="Работает">Работает</Option>
              <Option value="Не работает">Не работает</Option>
            </Select>
          </Form.Item>
          <Form.Item wrapperCol={{ offset: 0 }}>
            <Button type="primary" htmlType="submit" style={{ margin: '15px' }} value="large">
              Сохранить
            </Button>
            <Button
              style={{ margin: '15px' }}
              onClick={closeDrawerHandler}
              value="large"
              type="primary"
              danger
            >
              Отмена
            </Button>
          </Form.Item>
        </Form>
      </Space>
    </Drawer>
  );
};
