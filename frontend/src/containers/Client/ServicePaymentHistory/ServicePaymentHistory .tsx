import { useEffect } from 'react';
import { Col, Collapse, Row } from 'antd';
import Title from 'antd/lib/typography/Title';
import { useAppDispatch, useAppSelector } from '../../../hooks';
import {
  getServicePaymentHistoryById,
  resetState,
} from '../../../store/services/ServicePaymentHistorySlice';
import { resetServicePaymentId } from '../../../store/services/ServicePaymentSlice';
import { CurrentClientTable } from '../CurrentClient/CurrentClientInfo/CurrentClientTable/CurrentClientTable';

export const ServicePaymentHistory = () => {
  const { Panel } = Collapse;
  const dispatch = useAppDispatch();
  const { servicePaymentHistory } = useAppSelector((state) => state.servicePaymentHistory);
  const { servicePaymentId, addedSuccessfully } = useAppSelector((state) => state.servicePayment);
  useEffect(() => {
    dispatch(resetState());
    if (servicePaymentId) {
      dispatch(getServicePaymentHistoryById(servicePaymentId));
    }
    return () => {
      dispatch(resetServicePaymentId());
    };
  }, [dispatch, addedSuccessfully]);
  return (
    <>
      <Title>История абонентской платы</Title>
      {servicePaymentHistory.length > 0 ? (
        <Row>
          <Col xs={24} md={{ span: 22, offset: 1 }}>
            {servicePaymentHistory?.map((service, index: number) => {
              return (
                <div key={index + 1}>
                  <Collapse>
                    <Panel
                      header={`Дата изменения ${new Date(service.dateOfEvent).toLocaleString()}`}
                      key={index + 1}
                    >
                      <CurrentClientTable currentServicePayment={service} key={service._id} />
                    </Panel>
                  </Collapse>
                </div>
              );
            })}
          </Col>
        </Row>
      ) : (
        <p>Нет истории по абонентской плате </p>
      )}
    </>
  );
};
