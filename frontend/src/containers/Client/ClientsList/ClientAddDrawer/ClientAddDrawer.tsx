import { ChangeEvent, useState } from 'react';
import { Button, Checkbox, DatePickerProps, Drawer, Form, Input, Select, Space } from 'antd';
import { FormInstance } from 'antd/es/form/Form';

interface Props {
  closeDrawerHandler?: () => void;
  selectStatusHandler?: (value: string) => void;
  onDateChange?: DatePickerProps['onChange'];
  inputChangeHandler?: (e: ChangeEvent<HTMLInputElement>) => void;
  newClient?: {
    bin: string;
    iin: string;
    name: string;
    typeOfEconomicActivity: string;
    status: string;
  };
  selectPositionHandler?: (value: string) => void;
  onFinishFailed?: () => void;
  sendNewClientHandler?: () => void;
  form?: FormInstance;
  isDrawer?: boolean;
  isDisabled?: boolean;
  isIndividualEntrepreneur: () => void;
  individualEntrepreneur: boolean;
  selecthangeHandler: (value: string) => void;
}

export const ClientAddDrawer = ({
  isIndividualEntrepreneur,
  individualEntrepreneur,
  closeDrawerHandler,
  isDrawer,
  form,
  sendNewClientHandler,
  onFinishFailed,
  newClient,
  inputChangeHandler,
  selecthangeHandler,
}: Props) => {
  const { Option } = Select;
  const [isDisabled, setIsDisabled] = useState(true);

  const changeFormHandler = () => {
    const hasErrors = form?.getFieldsError().some(({ errors }) => errors.length);
    setIsDisabled(hasErrors ? hasErrors : false);
  };
  return (
    <Drawer title="Добавить клиента" placement="right" onClose={closeDrawerHandler} open={isDrawer}>
      <Space direction="vertical">
        <Checkbox onChange={isIndividualEntrepreneur} checked={individualEntrepreneur}>
          Клиент ИП
        </Checkbox>
        <Form
          form={form}
          name="basic"
          labelCol={{ span: 10 }}
          wrapperCol={{ span: 16 }}
          initialValues={{ remember: false }}
          onFinish={sendNewClientHandler}
          onFinishFailed={onFinishFailed}
          onFieldsChange={changeFormHandler}
          autoComplete="off"
        >
          {individualEntrepreneur ? (
            <Form.Item
              label="ИИН"
              name="iin"
              rules={[
                {
                  required: true,
                  message: 'Пожалуйста, введите ИИН',
                },
                {
                  pattern: new RegExp(/^[0-9]{1,}$/),
                  message: 'ИИН должен состоять только из цифр',
                },
                {
                  max: 12,
                  message: 'Макимальное количество символов - 12',
                },
                {
                  min: 12,
                  message: 'Минимальное количество символов - 12',
                },
              ]}
            >
              <Input value={newClient?.iin} name="iin" onChange={inputChangeHandler} />
            </Form.Item>
          ) : (
            <Form.Item
              label="БИН"
              name="bin"
              rules={[
                {
                  required: true,
                  message: 'Пожалуйста, введите БИН',
                },
                {
                  pattern: new RegExp(/^[0-9]{1,}$/),
                  message: 'БИН должен состоять только из цифр',
                },
                {
                  max: 12,
                  message: 'Макимальное количество символов - 12',
                },
                {
                  min: 12,
                  message: 'Минимальное количество символов - 12',
                },
              ]}
            >
              <Input value={newClient?.bin} name="bin" onChange={inputChangeHandler} />
            </Form.Item>
          )}
          <Form.Item
            label="Наименование"
            name="name"
            rules={[
              { required: true, message: 'Пожалуйста, введите наименование' },
              {
                pattern: new RegExp(/^\S/),
                message: 'поле не должно содержать лишних пробелов',
              },
            ]}
          >
            <Input value={newClient?.name} name="name" onChange={inputChangeHandler} />
          </Form.Item>
          <Form.Item
            label="Вид деятельности"
            name="typeOfEconomicActivity"
            rules={[
              { required: true, message: 'Пожалуйста, введите "Вид деятельности" компании' },
              {
                pattern: new RegExp(/^\S/),
                message: 'поле не должно содержать лишних пробелов',
              },
            ]}
          >
            <Input
              value={newClient?.typeOfEconomicActivity}
              name="typeOfEconomicActivity"
              onChange={inputChangeHandler}
            />
          </Form.Item>
          <Form.Item
            label="Статус"
            name="status"
            rules={[{ required: true, message: 'Пожалуйста, выберите статус' }]}
          >
            <Select style={{ display: 'block', borderRadius: '9px' }} onChange={selecthangeHandler}>
              <Option value="Активный">Активный</Option>
              <Option value="Неактивный">Неактивный</Option>
            </Select>
          </Form.Item>

          <Form.Item wrapperCol={{ offset: 0 }}>
            <Button
              disabled={isDisabled}
              type="primary"
              htmlType="submit"
              style={{ margin: '15px' }}
              value="large"
            >
              Сохранить
            </Button>
            <Button
              style={{ margin: '15px' }}
              onClick={closeDrawerHandler}
              value="large"
              type="primary"
              danger
            >
              Отмена
            </Button>
          </Form.Item>
        </Form>
      </Space>
    </Drawer>
  );
};
