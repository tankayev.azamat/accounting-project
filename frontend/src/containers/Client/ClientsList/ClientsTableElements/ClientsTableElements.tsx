/* eslint-disable @typescript-eslint/no-unused-vars */
import { Button, Input, Table, Typography } from 'antd';
import { ColumnsType } from 'antd/lib/table';
import { SearchOutlined } from '@ant-design/icons';
import DeleteOutlined from '@ant-design/icons/lib/icons/DeleteOutlined';
import { useAppSelector } from '../../../../hooks';
import { IClient } from '../../../../store/services/ClientsListSlice';

import 'antd/dist/antd.min.css';
import './ClientTableElements.css';

interface TableElementsProps {
  data: IClient[];
  showDrawer: (record: IClient) => void;
  deleteHandler: (record: IClient) => void;
  spinner: boolean;
}

export const ClientsTableElements: React.FC<TableElementsProps> = ({
  data,
  showDrawer,
  deleteHandler,
  spinner,
}) => {
  const user = useAppSelector((state) => state.users.user?.user);
  const columns: ColumnsType<IClient> = [
    {
      title: 'ИИН/БИН',
      dataIndex: 'bin',
      key: 'bin',
      render: (bin: string, record) => (
        <Typography.Text copyable>{bin || record.iin}</Typography.Text>
      ),
    },
    {
      title: 'Наименование',
      dataIndex: 'name',
      key: 'companyName',
      sorter: (firtRec, scndRec) => {
        return firtRec.name > scndRec.name ? 1 : -1;
      },
      filterDropdown: ({ setSelectedKeys, selectedKeys, confirm }) => {
        return (
          <Input
            placeholder="Введите текст..."
            value={selectedKeys[0]}
            onChange={(e) => {
              setSelectedKeys(e.target.value ? [e.target.value] : []);
              confirm({ closeDropdown: false });
            }}
            onPressEnter={(e) => {
              confirm({ closeDropdown: true });
              e.stopPropagation();
            }}
          />
        );
      },
      className: 'nameClass',
      filterIcon: () => {
        return <SearchOutlined />;
      },
      onFilter: (value: string | number | boolean, record: IClient) => {
        return record.name.toLowerCase().includes(String(value).toLowerCase());
      },
    },
    {
      title: 'Вид деятельности',
      dataIndex: 'typeOfEconomicActivity',
      key: 'typeOfEconomicActivity',
      sorter: (firtRec, scndRec) => {
        return firtRec.name > scndRec.name ? 1 : -1;
      },
      className: 'typeOfEconomicActivity',
    },
    {
      title: 'Статус',
      dataIndex: 'status',
      key: 'status',
    },
    {
      title: 'Начало контракта',
      dataIndex: 'registrationDate',
      key: 'registrationDate',
      render: (date: string) => new Date(date).toLocaleDateString(),
      sorter: (firtRec, scndRec) => {
        return firtRec.registrationDate > scndRec.registrationDate ? 1 : -1;
      },
    },
    {
      title: 'Дата завершения',
      dataIndex: 'expirationDate',
      key: 'expirationDate',
      render: (date: string) => (date ? new Date(date).toLocaleDateString() : '-'),
      sorter: (firtRec, scndRec) => {
        return firtRec.expirationDate > scndRec.expirationDate ? 1 : -1;
      },
    },
    {
      key: 'more',
      title: 'Действие',
      render: (record: IClient) => {
        return (
          <Button onClick={() => showDrawer(record)} type="primary">
            {' '}
            Подробнее
          </Button>
        );
      },
    },
  ];

  const deleteColumn = {
    key: 'delete',
    title: 'Удаление',
    render: (record: IClient) => {
      return record.status !== 'Удалён' ? (
        <Button className="ant-custom-btn" onClick={() => deleteHandler(record)}>
          <DeleteOutlined style={{ color: 'red', fontSize: '20px' }} />
        </Button>
      ) : null;
    },
  };

  if (user?.roleId === process.env.REACT_APP_MAIN_ROLE) columns.push(deleteColumn);

  return (
    <Table
      loading={spinner}
      locale={{
        triggerDesc: 'Нажмите для сортировки по убыванию',
        triggerAsc: 'Нажмите для сортировки по возрастанию',
        cancelSort: 'Отменить сортировку текста',
      }}
      dataSource={data}
      columns={columns}
      rowKey={(obj) => obj._id}
    />
  );
};
