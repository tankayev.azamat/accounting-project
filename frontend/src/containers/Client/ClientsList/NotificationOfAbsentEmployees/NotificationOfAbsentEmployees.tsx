import { Avatar, Badge, Popover } from 'antd';
import { TeamOutlined } from '@ant-design/icons';

interface Props {
  clientsNumber: number | null;
  onClickHandler: () => void;
}

const NotificationOfAbsentEmployees = ({ clientsNumber, onClickHandler }: Props) => {
  const information = (
    <div>Имеются клиенты, у которых не назначены сотрудники. Нажмите чтобы посмотреть.</div>
  );
  return (
    <Popover style={{ width: 100 }} content={information} title="Внимание!" trigger="hover">
      <div onClick={onClickHandler}>
        <Badge count={clientsNumber}>
          <Avatar
            shape="square"
            size="large"
            icon={<TeamOutlined style={{ margin: 'none', color: 'red' }} />}
          />
        </Badge>
      </div>
    </Popover>
  );
};

export default NotificationOfAbsentEmployees;
