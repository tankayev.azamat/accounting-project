import { Descriptions } from 'antd';
import Table, { ColumnsType } from 'antd/lib/table';
import { IServiceForm, IServicePayment } from '../../../../../store/services/ServicePaymentSlice';

interface Props {
  currentServicePayment?: IServicePayment;
}
export const CurrentClientTable = ({ currentServicePayment }: Props) => {
  const columns: ColumnsType<IServiceForm> = [
    {
      key: 'title',
      title: 'Элемент услуги',
      dataIndex: 'title',
    },
    {
      key: 'quantity',
      title: 'Количество',
      dataIndex: 'quantity',
    },
    {
      key: 'price',
      title: 'Цена',
      dataIndex: 'price',
      render: (_, record) => {
        return <p>{record.price.toLocaleString('ru')}</p>;
      },
    },
    {
      key: 'sum',
      title: 'Сумма',
      dataIndex: 'sum',
      render: (_, record) => {
        return <p>{record.sum.toLocaleString('ru')}</p>;
      },
    },
  ];
  return currentServicePayment ? (
    <div>
      <Table
        dataSource={currentServicePayment.serviceElements}
        columns={columns}
        size="middle"
        rowKey={(record) => record.title + Math.random() + Math.random()}
        pagination={false}
      />
      <Descriptions size={'small'} style={{ marginTop: '30px' }}>
        <Descriptions.Item label="Начало абонплаты">
          {new Date(currentServicePayment.startDate).toLocaleDateString()}
        </Descriptions.Item>
        <Descriptions.Item label="Сумма абонплаты">
          {currentServicePayment.totalSum.toLocaleString('ru')}
        </Descriptions.Item>
      </Descriptions>
    </div>
  ) : (
    <p>Не установлена абонентская плата</p>
  );
};
