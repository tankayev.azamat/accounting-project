import { ChangeEvent, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import {
  Button,
  Col,
  Collapse,
  DatePickerProps,
  Descriptions,
  Form,
  Row,
  Table,
  Typography,
} from 'antd';
import { ColumnsType } from 'antd/lib/table';
import { IPosition } from 'app/store/services/PositionSlice';
import { openNotification } from '../../../../components/helpers/helpers';
import { useAppDispatch, useAppSelector } from '../../../../hooks';
import {
  createNewHocService,
  getHocServiceCurrentClient,
  IHocService,
} from '../../../../store/services/ClientAddHocServiceSlice';
import {
  addEmployeesToCurrentClient,
  getCurrentClient,
} from '../../../../store/services/ClientsListSlice';
import { IEmployee } from '../../../../store/services/EmployeesListSlice';
import {
  getServicePayment,
  getServicePaymentByClient,
} from '../../../../store/services/ServicePaymentSlice';
import { AddServicePayment } from '../../AddServicePayment/AddServicePayment';
import { EditServicePayment } from '../../EditServicePayment/EditServicePayment';
import { ClientAddHocService } from '../ClientAddHocService/ClientAddHocService';
import { CurrentClientTable } from './CurrentClientTable/CurrentClientTable';
import { EmployeesAddDrawer } from './EmployeesAddDrawer/EmployeesAddDrawer';

import './CurrentClientInfo.css';

const { Panel } = Collapse;
const { Title } = Typography;

export const CurrentClientInfo = () => {
  const { currentClient, loading } = useAppSelector((state) => state.clients);
  const user = useAppSelector((state) => state.users.user?.user);

  const { currentServicePaymentList } = useAppSelector((state) => state.servicePayment);
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getServicePayment());
  }, [dispatch]);

  const { currentClientHocService, isLoadingHoc } = useAppSelector((state) => state.hocService);
  const [isAddDrawer, setIsAddDrawer] = useState(false);
  const [accompanyingEmployee, setaAccompanyingEmployee] = useState('');
  const [form] = Form.useForm();
  const [formOfHocService] = Form.useForm();
  const params = useParams();
  const [isDrawer, setIsDrawer] = useState(false);

  interface formState {
    serviceElements: string;
    startDate: Date | null;
    endDate: Date | null;
    payDate: Date | null;
    employeeId: string[];
    description: string;
    sum: number;
    clientId: string | undefined;
  }

  const [addHocService, setAddHocService] = useState<formState>({
    serviceElements: '',
    startDate: new Date(),
    endDate: null,
    payDate: null,
    employeeId: [],
    description: '',
    sum: 0,
    clientId: params.id,
  });

  useEffect(() => {
    dispatch(getHocServiceCurrentClient(params.id));
  }, [dispatch, params.id]);

  useEffect(() => {
    dispatch(getServicePaymentByClient(params.id));
  }, [dispatch, params.id]);

  const showDrawerMenu = () => {
    setIsDrawer(true);
  };

  const [isModalOpen, setIsModalOpen] = useState(false);

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleOk = () => {
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  const [isEditModalOpen, setIsEditModalOpen] = useState(false);
  const openEditModal = () => {
    setIsEditModalOpen(true);
  };
  const closeEditModal = () => {
    setIsEditModalOpen(false);
  };

  const onDateChange: DatePickerProps['onChange'] = (date) => {
    const copy = { ...addHocService };
    copy.startDate = date ? date.toDate() : new Date();
    setAddHocService(copy);
  };

  const onEndDateChange: DatePickerProps['onChange'] = (date) => {
    const copy = { ...addHocService };
    copy.endDate = date ? date.toDate() : null;
    setAddHocService(copy);
  };

  const onPayDateChange: DatePickerProps['onChange'] = (date) => {
    const copy = { ...addHocService };
    copy.payDate = date ? date.toDate() : null;
    setAddHocService(copy);
  };

  const closeDrawerHandler = () => {
    setIsDrawer(false);
    formOfHocService.resetFields([
      'serviceElements',
      'startDate',
      'endDate',
      'payDate',
      'employeeId',
      'description',
      'sum',
      'clientId',
    ]);
    setAddHocService({
      serviceElements: '',
      startDate: new Date(),
      endDate: null,
      payDate: null,
      employeeId: [],
      description: '',
      sum: 0,
      clientId: params.id,
    });
  };

  const inputChangeHandler = (
    e: ChangeEvent<HTMLInputElement> | ChangeEvent<HTMLTextAreaElement>
  ) => {
    const { name, value } = e.target;
    setAddHocService((prevState) => ({ ...prevState, [name]: value.trim() }));
  };

  const selectChangeHandler = (value: any) => {
    const copy = { ...addHocService };
    copy.employeeId = value;
    setAddHocService(copy);
  };

  const selectServiceChangeHandler = (value: any) => {
    const copy = { ...addHocService };
    copy.serviceElements = value;
    setAddHocService(copy);
  };

  const onFinishFailed = () => {
    openNotification('error', 'bottomLeft', 'Не ввели обязательные поля');
  };

  const sendNewHocService = () => {
    const obj = { ...addHocService };
    dispatch(createNewHocService(obj));
    closeDrawerHandler();
    openNotification('success', 'bottomLeft', 'Разовая услуга добавлена успешно');
  };

  const columns: ColumnsType<IHocService> = [
    {
      title: 'Элемент услуги',
      dataIndex: 'serviceElements',
      key: 'serviceElements',
    },
    {
      title: 'Дата расчета',
      dataIndex: 'payDate',
      key: 'payDate',
      render: (render: string) => (render ? new Date(render).toLocaleDateString() : '-'),
    },
    {
      title: 'Описание',
      dataIndex: 'description',
      key: 'description',
      className: 'seviceDescription',
    },
    {
      title: 'Сотрудник',
      dataIndex: 'employeeId',
      key: 'employeeId',
      render: (render: IEmployee[]) =>
        render.map((emp) => `${emp.firstName} ${emp.lastName}`).join(', '),
    },
    {
      title: 'Сумма',
      dataIndex: 'sum',
      key: 'sum',
      render: (_, record) => record.sum.toLocaleString('ru'),
    },
  ];

  const columnsEmployee = [
    {
      title: 'Имя',
      dataIndex: 'firstName',
      key: 'firstName',
    },
    {
      title: 'Фамилия',
      dataIndex: 'lastName',
      key: 'lastName',
    },
    {
      title: 'Должность',
      dataIndex: 'position',
      key: 'position',
      render: (render: IPosition) => {
        return render?.title;
      },
    },
  ];

  const showAddDrawerMenu = () => {
    setIsAddDrawer(true);
  };

  const closeAddDrawerHandler = () => {
    setIsAddDrawer(false);
    form.resetFields(['position', 'employeeName']);
  };

  const selectAccompanyingEmployeeHandler = (value: string) => {
    setaAccompanyingEmployee(value);
  };

  const sendAccompanyingEmployeeHandler = async () => {
    const obj = { id: params.id, data: accompanyingEmployee };
    await dispatch(addEmployeesToCurrentClient(obj));
    await dispatch(getCurrentClient(params.id));
    form.resetFields(['position', 'employeeName']);
    setIsAddDrawer(false);
    openNotification('success', 'bottomLeft', 'Сопровождающий сотрудник добавлен успешно');
  };
  return (
    <>
      <Title level={2}>Информация о клиенте</Title>
      <Row justify="space-evenly">
        <Col span={4}>
          <strong>{currentClient?.iin ? 'ИИН:' : 'БИН:'}</strong>
          <Descriptions style={{ fontSize: '16px' }}>
            <Descriptions.Item>{currentClient?.iin || currentClient?.bin}</Descriptions.Item>
          </Descriptions>
        </Col>
        <Col span={4}>
          <strong>Наименование:</strong>
          <Descriptions style={{ fontSize: '16px' }}>
            <Descriptions.Item>{currentClient?.name}</Descriptions.Item>
          </Descriptions>
        </Col>
        <Col span={4}>
          <strong>Вид деятельности:</strong>
          <Descriptions style={{ fontSize: '16px' }}>
            <Descriptions.Item>{currentClient?.typeOfEconomicActivity}</Descriptions.Item>
          </Descriptions>
        </Col>
        <Col span={4}>
          <strong>Статус:</strong>
          <Descriptions style={{ fontSize: '16px' }}>
            <Descriptions.Item>{currentClient?.status}</Descriptions.Item>
          </Descriptions>
        </Col>
      </Row>
      <Collapse defaultActiveKey={['1']}>
        <Panel header="Сопровождающие сотрудники" key="1">
          {currentClient?.accompanyingEmployees?.length ? (
            <Table
              loading={loading}
              dataSource={currentClient?.accompanyingEmployees}
              rowKey={() => Math.random() + Math.random()}
              columns={columnsEmployee}
            />
          ) : (
            <p>Не добавлен сопровождающий сотрудник</p>
          )}
          {user?.roleId === process.env.REACT_APP_MAIN_ROLE ||
          user?.positionId?._id === process.env.REACT_APP_LEAD_POSITION ? (
            <Button type="primary" size="large" onClick={() => showAddDrawerMenu()}>
              Добавить
            </Button>
          ) : null}
        </Panel>
        <Panel header="Абонентская плата" key="2">
          <CurrentClientTable currentServicePayment={currentServicePaymentList?.[0]} />
          {user?.roleId === process.env.REACT_APP_MAIN_ROLE ? (
            <>
              {currentServicePaymentList.length > 0 ? (
                <Button
                  type="primary"
                  style={{ marginTop: '30px' }}
                  size="large"
                  onClick={openEditModal}
                >
                  Редактировать
                </Button>
              ) : (
                <Button
                  type="primary"
                  style={{ marginTop: '30px' }}
                  size="large"
                  onClick={showModal}
                >
                  Добавить
                </Button>
              )}
            </>
          ) : null}
        </Panel>
        <Panel header="Разовые услуги" key="3">
          {currentClientHocService.length === 0 ? (
            <p>Разовые услуги отсутствуют</p>
          ) : (
            <Table
              loading={isLoadingHoc}
              rowKey={(record) => record._id}
              dataSource={currentClientHocService}
              size="small"
              columns={columns}
            />
          )}
          {user?.roleId === process.env.REACT_APP_MAIN_ROLE ? (
            <Button onClick={showDrawerMenu} type="primary" size="large">
              Добавить
            </Button>
          ) : null}
        </Panel>
      </Collapse>

      <ClientAddHocService
        closeDrawerHandler={closeDrawerHandler}
        isDrawer={isDrawer}
        form={formOfHocService}
        sendNewHocService={sendNewHocService}
        onFinishFailed={onFinishFailed}
        newHocService={addHocService}
        inputChangeHandler={inputChangeHandler}
        onDateChange={onDateChange}
        onEndDateChange={onEndDateChange}
        onPayDateChange={onPayDateChange}
        selectChangeHandler={selectChangeHandler}
        selectServiceChangeHandler={selectServiceChangeHandler}
      />
      <AddServicePayment
        isModalOpen={isModalOpen}
        clientId={params.id || undefined}
        handleOk={handleOk}
        handleCancel={handleCancel}
      />
      <EditServicePayment
        isEditModalOpen={isEditModalOpen}
        closeEditModal={closeEditModal}
        clientId={params.id}
      />
      <EmployeesAddDrawer
        closeDrawerHandler={closeAddDrawerHandler}
        isDrawer={isAddDrawer}
        accompanyingEmployee={accompanyingEmployee}
        selectAccompanyingEmployeeHandler={selectAccompanyingEmployeeHandler}
        onFinishFailed={onFinishFailed}
        sendAccompanyingEmployeeHandler={sendAccompanyingEmployeeHandler}
        form={form}
        value={accompanyingEmployee}
      />
    </>
  );
};
