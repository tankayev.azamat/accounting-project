import { ChangeEvent } from 'react';
import { Button, DatePicker, DatePickerProps, Drawer, Form, Input, Select, Space } from 'antd';
import { FormInstance } from 'antd/es/form/Form';
import moment from 'moment';
import { IClient } from '../../../../store/services/ClientsListSlice';

interface Props {
  closeDrawerHandler?: () => void;
  onDateChange?: DatePickerProps['onChange'];
  inputChangeHandler?: (e: ChangeEvent<HTMLInputElement>) => void;
  onFinishFailed?: () => void;
  sendEditClientHandler?: () => void;
  form?: FormInstance;
  isDrawer?: boolean;
  selecthangeHandler: (value: string) => void;
  currentClient: IClient | null;
  onRegDateChange: DatePickerProps['onChange'];
}

export const CurrentClientEdit = ({
  closeDrawerHandler,
  isDrawer,
  form,
  sendEditClientHandler,
  onFinishFailed,
  inputChangeHandler,
  selecthangeHandler,
  currentClient,
  onDateChange,
  onRegDateChange,
}: Props) => {
  const { Option } = Select;
  return (
    <Drawer
      title="Изменить данные клиента"
      placement="right"
      onClose={closeDrawerHandler}
      open={isDrawer}
    >
      <Space direction="vertical">
        <Form
          form={form}
          name="basic"
          labelCol={{ span: 10 }}
          wrapperCol={{ span: 16 }}
          initialValues={{ remember: false }}
          onFinish={sendEditClientHandler}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          {currentClient?.iin ? (
            <Form.Item
              label="ИИН"
              name="iin"
              rules={[
                {
                  required: true,
                  message: 'Пожалуйста, введите ИИН',
                },
                {
                  pattern: new RegExp(/^[0-9]{1,}$/),
                  message: 'ИИН должен состоять только из цифр',
                },
                {
                  max: 12,
                  message: 'Макимальное количество символов - 12',
                },
                {
                  min: 12,
                  message: 'Минимальное количество символов - 12',
                },
              ]}
              initialValue={currentClient?.iin}
            >
              <Input name="iin" onChange={inputChangeHandler} />
            </Form.Item>
          ) : (
            <Form.Item
              label="БИН"
              name="bin"
              rules={[
                {
                  required: true,
                  message: 'Пожалуйста, введите БИН',
                },
                {
                  pattern: new RegExp(/^[0-9]{1,}$/),
                  message: 'БИН должен состоять только из цифр',
                },
                {
                  max: 12,
                  message: 'Макимальное количество символов - 12',
                },
                {
                  min: 12,
                  message: 'Минимальное количество символов - 12',
                },
              ]}
              initialValue={currentClient?.bin}
            >
              <Input name="bin" onChange={inputChangeHandler} />
            </Form.Item>
          )}
          <Form.Item
            label="Наименование"
            name="name"
            rules={[
              { required: true, message: 'Пожалуйста, введите наименование' },
              {
                pattern: new RegExp(/^\S/),
                message: 'поле не должно содержать лишних пробелов',
              },
            ]}
            initialValue={currentClient?.name}
          >
            <Input name="name" onChange={inputChangeHandler} />
          </Form.Item>
          <Form.Item
            label="Вид деятельности"
            name="typeOfEconomicActivity"
            rules={[
              { required: true, message: 'Пожалуйста, введите "Вид деятельности" компании' },
              {
                pattern: new RegExp(/^\S/),
                message: 'поле не должно содержать лишних пробелов',
              },
            ]}
            initialValue={currentClient?.typeOfEconomicActivity}
          >
            <Input name="typeOfEconomicActivity" onChange={inputChangeHandler} />
          </Form.Item>
          <Form.Item
            label="Статус"
            name="status"
            rules={[{ required: true, message: 'Пожалуйста, выберите статус' }]}
            initialValue={currentClient?.status}
          >
            <Select style={{ display: 'block', borderRadius: '9px' }} onChange={selecthangeHandler}>
              <Option value="Активный">Активный</Option>
              <Option value="Неактивный">Неактивный</Option>
            </Select>
          </Form.Item>
          <Form.Item
            label="Дата начало"
            name="registrationDate"
            rules={[
              { required: true, message: 'Пожалуйста, введите дату начала контракта' },
              {
                pattern: new RegExp(/^\S/),
                message: 'поле не должно содержать лишних пробелов',
              },
            ]}
            initialValue={
              currentClient?.registrationDate ? moment(currentClient?.registrationDate) : undefined
            }
          >
            <DatePicker format={'DD.MM.YYYY'} onChange={onRegDateChange} />
          </Form.Item>
          <Form.Item
            label="Дата окончания"
            name="expirationDate"
            rules={[
              {
                pattern: new RegExp(/^\S/),
                message: 'поле не должно содержать лишних пробелов',
              },
            ]}
            initialValue={
              currentClient?.expirationDate ? moment(currentClient?.expirationDate) : undefined
            }
          >
            <DatePicker format={'DD.MM.YYYY'} onChange={onDateChange} />
          </Form.Item>

          <Form.Item wrapperCol={{ offset: 0 }}>
            <Button type="primary" htmlType="submit" style={{ margin: '15px' }} value="large">
              Сохранить
            </Button>
            <Button
              style={{ margin: '15px' }}
              onClick={closeDrawerHandler}
              value="large"
              type="primary"
              danger
            >
              Отмена
            </Button>
          </Form.Item>
        </Form>
      </Space>
    </Drawer>
  );
};
