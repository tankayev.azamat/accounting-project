import { useEffect, useState } from 'react';
import { Button, Form, Input, Modal, Select, Space } from 'antd';
import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';
import { openNotification } from '../../../components/helpers/helpers';
import { useAppDispatch, useAppSelector } from '../../../hooks';
import { getServiceElementsList } from '../../../store/services/ServiceElementsSlice';
import {
  editServicePayment,
  getServicePaymentByClient,
  IServiceForm,
} from '../../../store/services/ServicePaymentSlice';

import './EditServicePayment.css';

interface IEditServiceProps {
  isEditModalOpen: boolean;
  closeEditModal: () => void;
  clientId: string | undefined;
}

interface EditedValuesTypes {
  editedServicePayment: IServiceForm[];
  startDate: Date;
  clientId: string | undefined;
  totalSum: number;
}

export const EditServicePayment = ({
  isEditModalOpen,
  clientId,
  closeEditModal,
}: IEditServiceProps) => {
  const { currentServicePaymentList } = useAppSelector((state) => state.servicePayment);
  const { serviceElementsList } = useAppSelector((state) => state.serviceElements);
  const [paymentOption, setPaymentOption] = useState([
    {
      label: '',
      value: '',
    },
  ]);
  const [form] = Form.useForm();
  const dispatch = useAppDispatch();
  const [fields, setFields] = useState([]);

  useEffect(() => {
    dispatch(getServicePaymentByClient(clientId));
  }, [dispatch]);
  useEffect(() => {
    dispatch(getServiceElementsList());
  }, [dispatch]);
  useEffect(() => {
    const options = serviceElementsList.map((option) => {
      const paymentOption = {
        label: option.title,
        value: option.title,
      };
      return paymentOption;
    });
    setPaymentOption(options);
  }, [serviceElementsList]);

  const onFinishSubmit = async (values: EditedValuesTypes) => {
    if (values.editedServicePayment.length === 0) {
      openNotification('warning', 'bottomLeft', 'Добавьте абонплату');
      return;
    }
    const countSum = values.editedServicePayment
      .map((item) => item.price * item.quantity)
      .reduce((acc, num) => acc + num, 0);
    const copiedService: IServiceForm[] = values.editedServicePayment.map((service) => {
      return { ...service, sum: service.quantity * service.price };
    });

    const editedValues = {
      id: currentServicePaymentList[0]?._id,
      data: {
        serviceElements: copiedService,
        startDate: new Date(),
        clientId: clientId,
        totalSum: countSum,
      },
    };
    await dispatch(editServicePayment(editedValues));
    await dispatch(getServicePaymentByClient(clientId));
    setFields([]);
    form.resetFields();
    closeEditModal();
  };
  const cancelHandler = () => {
    setFields([]);
    form.resetFields();
    closeEditModal();
  };
  return (
    <>
      <Modal
        title="Добавление абонплаты в разбивке данных"
        open={isEditModalOpen}
        onOk={() => form.submit()}
        onCancel={cancelHandler}
        width={1000}
      >
        <Form
          form={form}
          fields={fields}
          layout="vertical"
          autoComplete="off"
          initialValues={currentServicePaymentList[0]?.serviceElements}
          onFinish={onFinishSubmit}
          style={{ width: '900px' }}
        >
          <Form.List
            name="editedServicePayment"
            initialValue={currentServicePaymentList[0]?.serviceElements}
          >
            {(fields, { add, remove }) => (
              <>
                {fields.map(({ key, name, ...restField }) => (
                  <Space key={key} style={{ display: 'flex', marginBottom: 8 }} align="baseline">
                    Элемент услуги
                    <Form.Item
                      {...restField}
                      name={[name, 'title']}
                      rules={[{ required: true, message: 'Пожалуйста, выберите элемент услуги' }]}
                      style={{ width: '160px' }}
                    >
                      <Select
                        style={{ display: 'block', borderRadius: '9px', width: '160px' }}
                        options={paymentOption}
                      />
                    </Form.Item>
                    Количество
                    <Form.Item
                      {...restField}
                      name={[name, 'quantity']}
                      style={{ width: '160px' }}
                      rules={[
                        { required: true, message: 'Пожалуйста, введите количество' },
                        {
                          pattern: new RegExp(/^0*?[1-9]\d*$/),
                          message: 'Количество должно состоять только из цифр больше 0',
                        },
                        {
                          pattern: new RegExp(/^\S/),
                          message: 'Поле не должно содержать лишних пробелов',
                        },
                      ]}
                    >
                      <Input name="quantity" />
                    </Form.Item>
                    Цена
                    <Form.Item
                      {...restField}
                      name={[name, 'price']}
                      style={{ width: '200px' }}
                      rules={[
                        { required: true, message: 'Пожалуйста, введите цену' },
                        {
                          pattern: new RegExp(/^0*?[1-9]\d*$/),
                          message: 'Цена должна состоять только из цифр больше 0',
                        },
                        {
                          pattern: new RegExp(/^\S/),
                          message: 'Поле не должно содержать лишних пробелов',
                        },
                      ]}
                    >
                      <Input name="price" />
                    </Form.Item>
                    <MinusCircleOutlined className="minusCircle" onClick={() => remove(name)} />
                  </Space>
                ))}
                <Form.Item>
                  <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                    Добавить абонентскую плату
                  </Button>
                </Form.Item>
              </>
            )}
          </Form.List>
        </Form>
      </Modal>
    </>
  );
};
