import { ChangeEvent, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { Breadcrumb, Col, Form, Row, Tabs, Typography } from 'antd';
import { openNotification } from '../../../components/helpers/helpers';
import { useAppDispatch, useAppSelector } from '../../../hooks';
import { createNewRates, getRates, resetStatus } from '../../../store/services/RatesSlice';
import { CreateNewRates } from './CreateNewRates/CreateNewRates';
import { RatesNewEmployee } from './RatesEmployeeList/RatesNewEmpoyee';
import { RatesList } from './RatesList/RatesList';

export const Rates: React.FC = () => {
  const { Title } = Typography;
  const [isDrawer, setIsDrawer] = useState(false);
  const dispatch = useAppDispatch();
  const { message, successfully, hasError } = useAppSelector((state) => state.rates);
  const [RatesState, setRatesState] = useState({
    elementsOfService: '',
    price: 0,
  });
  const [elementCodeData, setElementCode] = useState('');
  const [form] = Form.useForm();

  useEffect(() => {
    dispatch(getRates());
  }, [dispatch]);

  useEffect(() => {
    if (successfully) {
      openNotification('success', 'bottomLeft', message);
      setTimeout(() => {
        dispatch(resetStatus());
      }, 1000);
    }
    if (hasError) {
      openNotification('error', 'bottomLeft', message);
      setTimeout(() => {
        dispatch(resetStatus());
      }, 1000);
    }
  }, [successfully, hasError]);

  const closeDrawerHandler = () => {
    setIsDrawer(false);
    form.resetFields(['elementsOfService', 'price']);
    setRatesState({
      elementsOfService: '',
      price: 0,
    });
  };

  const inputChangeHandler = (e: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setRatesState((prevState) => ({ ...prevState, [name]: value.trim() }));
  };
  const showDrawerMenu = () => {
    setIsDrawer(true);
  };

  const selectHandler = (value: string) => {
    setElementCode(value);
  };

  const sendNewRatesHandler = () => {
    const data = {
      elementsOfService: elementCodeData,
      price: RatesState.price,
    };
    dispatch(createNewRates(data));
    closeDrawerHandler();
  };

  const onFinishFailed = () => {
    openNotification('warning', 'bottomLeft', 'Пожалуйста, введите обязательные поля');
  };

  const items = [
    {
      label: 'Тарифы для услуг',
      key: 'item-1',
      children: <RatesList showDrawerMenu={showDrawerMenu} />,
    },
    {
      label: 'Тарифы для должностей',
      key: 'item-3',
      children: <RatesNewEmployee />,
    },
  ];

  return (
    <>
      <Breadcrumb>
        <Breadcrumb.Item>
          <Link to="/">Главная</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>Тарифы</Breadcrumb.Item>
      </Breadcrumb>
      <Row>
        <Col xs={24} md={{ span: 20, offset: 1 }}>
          <Title>Тарифы</Title>
          <Form form={form} />
        </Col>
      </Row>
      <Tabs defaultActiveKey="1" items={items} />
      <CreateNewRates
        form={form}
        selectHandler={selectHandler}
        inputChangeHandler={inputChangeHandler}
        newRates={RatesState}
        onFinishFailed={onFinishFailed}
        sendNewRatesHandler={sendNewRatesHandler}
        closeDrawerHandler={closeDrawerHandler}
        isDrawer={isDrawer}
      />
    </>
  );
};
