import { ChangeEvent, useState } from 'react';
import { Button, Form, Modal, Table } from 'antd';
import { ColumnsType } from 'antd/lib/table';
import { EditOutlined, ExclamationCircleOutlined, FileAddOutlined } from '@ant-design/icons';
import { openNotification } from '../../../../components/helpers/helpers';
import { useAppDispatch, useAppSelector } from '../../../../hooks';
import {
  editedRates,
  editRatesEmployeePut,
  IRatesEmployee,
} from '../../../../store/services/RatesEmployeeSlice';
import { EditRatesEmployee } from '../EditRatesEmployee/EditRatesEmployee';
interface Props {
  showDrawerMenu: () => void;
}

export const RatesEmployeeList = ({ showDrawerMenu }: Props) => {
  const [isEditEmployeeDrawer, setIsEditEmployeeDrawer] = useState(false);
  const { open, ratesEmployeeList, loading } = useAppSelector((state) => state.ratesEmployee);
  const [editRatesEmployee, setEditRatesEmployee] = useState({
    _id: '',
    position: '',
    percent: 0,
  });
  const [form] = Form.useForm();
  const dispatch = useAppDispatch();

  const showDrawer = (value: any) => {
    setIsEditEmployeeDrawer(true);
    setEditRatesEmployee(value);
  };

  const columns: ColumnsType<IRatesEmployee> = [
    {
      title: 'Должность',
      dataIndex: 'position',
      key: 'position',
    },
    {
      title: 'Процент',
      dataIndex: 'percent',
      key: 'percent',
      render: (_, record) => record.percent.toLocaleString('ru') + ' %',
    },
    {
      key: 'more',
      title: 'Действие',
      render: (_, record) => {
        return (
          <Button onClick={() => showDrawer(record)} type="primary">
            {' '}
            <EditOutlined />
          </Button>
        );
      },
    },
  ];

  const inputEditEmployeeChangeHandler = (e: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setEditRatesEmployee((prevState) => ({ ...prevState, [name]: value }));
  };

  const closeEditEmployeeDrawerHandler = () => {
    setIsEditEmployeeDrawer(false);
    form.resetFields(['percent', '_id', 'position']);
    setEditRatesEmployee({
      _id: '',
      position: '',
      percent: 0,
    });
  };

  const sendEditEmployeeRatesHandler = () => {
    const onOk = async () => {
      dispatch(editedRates(editRatesEmployee));
      dispatch(editRatesEmployeePut(editRatesEmployee));
      closeEditEmployeeDrawerHandler();
      open === false;
    };

    const hideModal = () => {
      open === false;
    };

    Modal.confirm({
      title: 'Предупреждение',
      icon: <ExclamationCircleOutlined />,
      content: 'Вы действительно хотите изменить тариф по должности ?',
      okText: 'OK',
      cancelText: 'Отменить',
      open: open,
      onOk: onOk,
      onCancel: hideModal,
    });
  };

  const onFinishFailedEmployee = () => {
    openNotification('warning', 'bottomLeft', 'Пожалуйста, заполните обязательные поля');
  };

  return (
    <>
      <Button type="primary" onClick={showDrawerMenu} icon={<FileAddOutlined />} size="large">
        Добавить тариф для должности
      </Button>
      <Table
        loading={loading}
        rowKey={(record) => record._id}
        dataSource={ratesEmployeeList}
        size="small"
        columns={columns}
        style={{ width: '400px', marginTop: '20px' }}
      />
      <Form form={form} />
      <EditRatesEmployee
        closeEditEmployeeDrawerHandler={closeEditEmployeeDrawerHandler}
        isEditEmployeeDrawer={isEditEmployeeDrawer}
        formEditEmployee={form}
        sendEditEmployeeRatesHandler={sendEditEmployeeRatesHandler}
        onFinishFailedEmployee={onFinishFailedEmployee}
        EditRatesEmployee={editRatesEmployee}
        inputEditEmployeeChangeHandler={inputEditEmployeeChangeHandler}
      />
    </>
  );
};
