import { ChangeEvent, useEffect, useState } from 'react';
import { Form } from 'antd';
import { openNotification } from '../../../../components/helpers/helpers';
import { useAppDispatch, useAppSelector } from '../../../../hooks';
import {
  createNewRatesEmployee,
  getRatesEmployee,
  resetStatus,
} from '../../../../store/services/RatesEmployeeSlice';
import { CreateNewRatesEmployee } from '../CreateNewRatesEmployee/CreateNewRatesEmployee';
import { RatesEmployeeList } from './RatesEmployeeList';

export function RatesNewEmployee() {
  const [isDrawer, setIsDrawer] = useState(false);
  const { successfully, message, hasError } = useAppSelector((state) => state.ratesEmployee);
  const [newRates, setNewRates] = useState({
    position: '',
    percent: 0,
  });
  const [form] = Form.useForm();
  const dispatch = useAppDispatch();

  useEffect(() => {
    if (successfully) {
      openNotification('success', 'bottomLeft', message);
      setTimeout(() => {
        dispatch(resetStatus());
      }, 1000);
    }
    if (hasError) {
      openNotification('error', 'bottomLeft', message);
      setTimeout(() => {
        dispatch(resetStatus());
      }, 1000);
    }
  }, [successfully, hasError]);

  useEffect(() => {
    dispatch(getRatesEmployee());
  }, [dispatch]);

  const inputChangeHandler = (e: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setNewRates((prevState) => ({ ...prevState, [name]: value }));
  };

  const closeDrawerHandler = () => {
    setIsDrawer(false);
    form.resetFields(['position', 'percent']);
    setNewRates({
      position: '',
      percent: 0,
    });
  };

  const showDrawerMenu = () => {
    setIsDrawer(true);
  };

  const sendRatesHandler = () => {
    dispatch(createNewRatesEmployee(newRates));
    closeDrawerHandler();
  };

  const selectHandler = (value: string) => {
    const copyNewEmployee = { ...newRates };
    copyNewEmployee.position = value;
    setNewRates(copyNewEmployee);
  };

  const onFinishFailed = () => {
    openNotification('warning', 'bottomLeft', 'Пожалуйста, заполните обязательные поля');
  };

  return (
    <>
      <RatesEmployeeList showDrawerMenu={showDrawerMenu} />
      <CreateNewRatesEmployee
        form={form}
        closeDrawerHandler={closeDrawerHandler}
        isDrawer={isDrawer}
        sendNewRatesEmployeeHandler={sendRatesHandler}
        onFinishFailed={onFinishFailed}
        newRatesEmployee={newRates}
        inputChangeHandler={inputChangeHandler}
        selectHandler={selectHandler}
      />
    </>
  );
}
