import { ChangeEvent, useEffect } from 'react';
import { Button, Drawer, Form, FormInstance, Input, Space } from 'antd';

interface Props {
  closeEditEmployeeDrawerHandler?: () => void;
  inputEditEmployeeChangeHandler?: (e: ChangeEvent<HTMLInputElement>) => void;
  EditRatesEmployee?: {
    _id: string;
    position: string;
    percent: number;
  };
  onFinishFailedEmployee?: () => void;
  sendEditEmployeeRatesHandler?: () => void;
  formEditEmployee?: FormInstance;
  isEditEmployeeDrawer?: boolean;
}

export const EditRatesEmployee = ({
  closeEditEmployeeDrawerHandler,
  isEditEmployeeDrawer,
  formEditEmployee,
  sendEditEmployeeRatesHandler,
  onFinishFailedEmployee,
  EditRatesEmployee,
  inputEditEmployeeChangeHandler,
}: Props) => {
  useEffect(() => {
    formEditEmployee ? formEditEmployee.setFieldsValue(EditRatesEmployee) : null;
  }, [formEditEmployee, EditRatesEmployee]);

  return (
    <>
      <Drawer
        title="Редактировать тариф для должности"
        placement="right"
        onClose={closeEditEmployeeDrawerHandler}
        open={isEditEmployeeDrawer}
      >
        <Space direction="vertical">
          <Form
            form={formEditEmployee}
            name="basic"
            labelCol={{ span: 2 }}
            wrapperCol={{ span: 20 }}
            initialValues={{ remember: false }}
            onFinish={sendEditEmployeeRatesHandler}
            onFinishFailed={onFinishFailedEmployee}
            autoComplete="off"
          >
            <label>
              <span style={{ color: 'red' }}>*</span> Должность:
            </label>
            <Form.Item
              name="position"
              rules={[{ required: true, message: 'Пожалуйста, выберите должность' }]}
              initialValue={EditRatesEmployee?.position}
            >
              <Input disabled />
            </Form.Item>
            <label>
              <span style={{ color: 'red' }}>*</span> Процент:
            </label>
            <Form.Item
              name="percent"
              rules={[
                { required: true, message: 'Пожалуйста, введите процент для тарифа' },
                {
                  pattern: new RegExp(/^[0-9]{1,}$/),
                  message: 'Процент должен состоять только из цифр',
                },
                {
                  pattern: new RegExp(/^\S/),
                  message: 'поле не должно содержать лишних пробелов',
                },
              ]}
              initialValue={EditRatesEmployee?.percent}
            >
              <Input
                name="percent"
                placeholder="Процент"
                onChange={inputEditEmployeeChangeHandler}
              />
            </Form.Item>
            <Form.Item wrapperCol={{ offset: 0 }}>
              <Button type="primary" htmlType="submit" style={{ margin: '15px' }} value="large">
                Сохранить
              </Button>
              <Button
                style={{ margin: '15px' }}
                onClick={closeEditEmployeeDrawerHandler}
                value="large"
                type="primary"
                danger
              >
                Отмена
              </Button>
            </Form.Item>
          </Form>
        </Space>
      </Drawer>
    </>
  );
};
