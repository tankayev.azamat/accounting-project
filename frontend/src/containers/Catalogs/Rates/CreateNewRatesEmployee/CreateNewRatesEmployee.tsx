import { ChangeEvent, useEffect, useState } from 'react';
import { Button, Drawer, Form, FormInstance, Input, Select, Space } from 'antd';
import { useAppDispatch, useAppSelector } from '../../../../hooks';
import { getPositions } from '../../../../store/services/PositionSlice';

interface Props {
  closeDrawerHandler?: () => void;
  inputChangeHandler?: (e: ChangeEvent<HTMLInputElement>) => void;
  newRatesEmployee?: {
    position: string;
    percent: number;
  };
  onFinishFailed?: () => void;
  sendNewRatesEmployeeHandler?: () => void;
  form?: FormInstance;
  isDrawer?: boolean;
  selectHandler?: (value: string) => void;
}

export const CreateNewRatesEmployee = ({
  closeDrawerHandler,
  isDrawer,
  form,
  sendNewRatesEmployeeHandler,
  onFinishFailed,
  newRatesEmployee,
  inputChangeHandler,
  selectHandler,
}: Props) => {
  const { positions } = useAppSelector((state) => state.positions);
  const { Option } = Select;
  const dispatch = useAppDispatch();
  const [isDisabled, setIsDisabled] = useState(true);
  const changeFormHandler = () => {
    const hasErrors = form?.getFieldsError().some(({ errors }) => errors.length);
    setIsDisabled(hasErrors ? hasErrors : false);
  };

  useEffect(() => {
    dispatch(getPositions());
  }, [dispatch]);

  return (
    <>
      <Drawer
        title="Добавить новый тариф для должности"
        placement="right"
        onClose={closeDrawerHandler}
        open={isDrawer}
      >
        <Space direction="vertical">
          <Form
            form={form}
            name="basic"
            labelCol={{ span: 2 }}
            wrapperCol={{ span: 20 }}
            initialValues={{ remember: false }}
            onFinish={sendNewRatesEmployeeHandler}
            onFinishFailed={onFinishFailed}
            onFieldsChange={changeFormHandler}
            autoComplete="off"
          >
            <label>
              <span style={{ color: 'red' }}>*</span> Должность:
            </label>
            <Form.Item
              name="position"
              rules={[{ required: true, message: 'Пожалуйста, выберите должность' }]}
            >
              <Select value={newRatesEmployee?.position} onChange={selectHandler}>
                {positions.map((service) => (
                  <Option key={service._id} value={service.title}>
                    {service.title}
                  </Option>
                ))}
              </Select>
            </Form.Item>
            <label>
              <span style={{ color: 'red' }}>*</span> Процент:
            </label>
            <Form.Item
              name="percent"
              rules={[
                { required: true, message: 'Пожалуйста, введите процент для тарифа' },
                {
                  pattern: new RegExp(/^[0-9]{1,}$/),
                  message: 'Процент должен состоять только из цифр',
                },
                {
                  pattern: new RegExp(/^\S/),
                  message: 'поле не должно содержать лишних пробелов',
                },
              ]}
            >
              <Input
                name="percent"
                placeholder="Процент"
                value={newRatesEmployee?.percent}
                onChange={inputChangeHandler}
              />
            </Form.Item>
            <Form.Item wrapperCol={{ offset: 0 }}>
              <Button
                disabled={isDisabled}
                type="primary"
                htmlType="submit"
                style={{ margin: '15px' }}
                value="large"
              >
                Сохранить
              </Button>
              <Button
                style={{ margin: '15px' }}
                onClick={closeDrawerHandler}
                value="large"
                type="primary"
                danger
              >
                Отмена
              </Button>
            </Form.Item>
          </Form>
        </Space>
      </Drawer>
    </>
  );
};
