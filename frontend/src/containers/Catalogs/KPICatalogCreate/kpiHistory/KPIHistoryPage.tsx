import { useEffect } from 'react';
import { shallowEqual } from 'react-redux';
import { Col, Collapse, Modal, Row, Table } from 'antd';
import Title from 'antd/lib/typography/Title';
import { useAppDispatch, useAppSelector } from '../../../../hooks';
import { getKpiHistoryById, resetState } from '../../../../store/services/KpiHistoriesSlice';

interface KpiHistoryProps {
  showModal: boolean;
  closeModal: () => void;
}
export const KPIHistoryPage = ({ showModal, closeModal }: KpiHistoryProps) => {
  const dispatch = useAppDispatch();
  const { kpiHistory, loading } = useAppSelector((state) => state.kpiHistories);
  const { kpiId } = useAppSelector((state) => state.kpi, shallowEqual);
  useEffect(() => {
    dispatch(resetState());
    dispatch(getKpiHistoryById(kpiId));
  }, [dispatch, showModal]);
  const cancelHandler = () => {
    dispatch(resetState());
    closeModal();
  };

  const columns = [
    {
      title: 'Вид',
      dataIndex: 'view',
      key: 'view',
    },
    {
      title: 'Цель',
      dataIndex: 'goal',
      key: 'goal',
    },
    {
      title: 'Описание',
      dataIndex: 'description',
      key: 'description',
    },
    {
      title: 'Примечание',
      dataIndex: 'comment',
      key: 'comment',
    },
    {
      title: 'Вес',
      dataIndex: 'weight',
      key: 'weight',
    },
  ];
  const { Panel } = Collapse;
  return (
    <Modal
      open={showModal}
      okText=""
      onCancel={cancelHandler}
      cancelText="Закрыть"
      width={'1200px'}
    >
      <Title>История КРІ для должности {kpiHistory[0]?.positionId.title}</Title>
      <Row>
        <Col xs={24} md={{ span: 22, offset: 1 }}>
          {kpiHistory?.map((history, index: number) => {
            return (
              <div key={index + 1}>
                <Collapse>
                  <Panel
                    header={`Дата изменения ${new Date(history.dateOfEvent).toLocaleString()}`}
                    key={index + 1}
                  >
                    <h4>Бонус за выполнение 100% КРI: {history.percentForKpi}%</h4>
                    <Table
                      loading={loading}
                      columns={columns}
                      dataSource={history.kpiElements}
                      rowKey={(record) => record.goal + Math.random() + Math.random()}
                      pagination={false}
                    />
                  </Panel>
                </Collapse>
              </div>
            );
          })}
        </Col>
      </Row>
    </Modal>
  );
};
