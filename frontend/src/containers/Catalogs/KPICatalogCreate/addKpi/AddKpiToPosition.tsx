import { useEffect, useState } from 'react';
import { shallowEqual } from 'react-redux';
import {
  Button,
  Col,
  Descriptions,
  Form,
  Input,
  InputNumber,
  Modal,
  Row,
  Select,
  Space,
} from 'antd';
import Title from 'antd/lib/typography/Title';
import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';
import { openNotification } from '../../../../components/helpers/helpers';
import { useAppDispatch, useAppSelector } from '../../../../hooks';
import { getKpiKeyIndicatorElements } from '../../../../store/services/KpiKeyIndicatorsSlice';
import { addKpi, Elements, INewKpi } from '../../../../store/services/KpiSlice';
interface KpiProps {
  positionId: string;
  showModal: boolean;
  closeModal: () => void;
}
interface InputValues {
  kpiElements: Elements[];
  percentForKpi: number;
}
export const AddKpiToPosition = ({ positionId, showModal, closeModal }: KpiProps) => {
  const dispatch = useAppDispatch();
  const { TextArea } = Input;
  const [fields, setFields] = useState([]);
  const [form] = Form.useForm();
  const [kpiOption, setKpiOption] = useState([
    {
      label: '',
      value: '',
    },
  ]);
  const [totalWeight, setTotalWeigth] = useState(0);
  const { kpiKeyIndicatorElements } = useAppSelector(
    (state) => state.kpiIndicatorKeys,
    shallowEqual
  );
  useEffect(() => {
    dispatch(getKpiKeyIndicatorElements());
  }, [dispatch]);

  useEffect(() => {
    const options = kpiKeyIndicatorElements.map((option): any => {
      const kpiOption = {
        label: option.title,
        value: option.title,
      };
      return kpiOption;
    });
    setKpiOption(options);
  }, [kpiKeyIndicatorElements]);

  const onFinish = (values: InputValues) => {
    const countWeight = values.kpiElements
      .map((item) => item.weight)
      .reduce((acc, number) => acc + number);
    if (countWeight <= 100) {
      setTotalWeigth(countWeight);
    }
    if (countWeight === 100) {
      const obj: INewKpi = {
        positionId,
        kpiElements: values.kpiElements,
        percentForKpi: values.percentForKpi,
      };
      dispatch(addKpi(obj));
      setFields([]);
      form.resetFields();
      closeModal();
    } else {
      openNotification('warning', 'bottomLeft', 'Итоговый вес всего КРІ не равен 100');
    }
  };

  const cancelHandler = () => {
    setFields([]);
    form.resetFields();
    closeModal();
  };

  return (
    <div style={{ position: 'relative' }}>
      <Modal
        open={showModal}
        onOk={() => form.submit()}
        okText="Сохранить"
        onCancel={cancelHandler}
        width={'1200px'}
        cancelText="Отмена"
      >
        <Title>Добавление КРІ</Title>
        <Row>
          <Col xs={24} md={{ span: 22, offset: 1 }}>
            <Descriptions style={{ position: 'absolute', top: 0, right: 10 }}>
              <Descriptions.Item label="Итого">{totalWeight}</Descriptions.Item>
            </Descriptions>
            <Form
              fields={fields}
              name="dynamic_form_nest_item"
              onFinish={onFinish}
              autoComplete="off"
              form={form}
              style={{ marginTop: '20px' }}
            >
              <Form.List name="kpiElements">
                {(fields, { add, remove }) => (
                  <>
                    {fields.map(({ key, name, ...restField }) => (
                      <Space
                        key={key}
                        style={{
                          display: 'flex',
                          justifyContent: 'space-between',
                          marginBottom: 8,
                        }}
                      >
                        <Form.Item
                          {...restField}
                          name={[name, 'view']}
                          rules={[
                            {
                              required: true,
                              pattern: new RegExp(/^\S/),
                              message: 'Выберите вид КРІ',
                            },
                          ]}
                        >
                          <Select style={{ width: 120 }} options={kpiOption} />
                        </Form.Item>
                        <Form.Item
                          {...restField}
                          name={[name, 'goal']}
                          rules={[
                            {
                              required: true,
                              pattern: new RegExp(/^\S/),
                              message: 'Пожалуйста, введите Цель',
                            },
                          ]}
                        >
                          <TextArea
                            placeholder="Цель"
                            style={{ width: '250px' }}
                            autoSize={{ minRows: 3, maxRows: 8 }}
                          />
                        </Form.Item>
                        <Form.Item
                          {...restField}
                          name={[name, 'description']}
                          rules={[
                            {
                              required: true,
                              pattern: new RegExp(/^\S/),
                              message: 'Пожалуйста, введите описание',
                            },
                          ]}
                        >
                          <TextArea
                            placeholder="Описание"
                            style={{ width: '250px' }}
                            autoSize={{ minRows: 3, maxRows: 8 }}
                          />
                        </Form.Item>
                        <Form.Item
                          {...restField}
                          name={[name, 'comment']}
                          rules={[
                            {
                              pattern: new RegExp(/^\S/),
                              message: 'Поле не должно содержать лишних пробелов',
                            },
                          ]}
                        >
                          <TextArea
                            style={{ width: '250px' }}
                            placeholder="Примечание"
                            autoSize={{ minRows: 3, maxRows: 8 }}
                          />
                        </Form.Item>
                        <Form.Item
                          {...restField}
                          name={[name, 'weight']}
                          rules={[
                            { required: true, message: 'Пожалуйста, Введите вес показателя' },
                            {
                              pattern: new RegExp(/^[0-9]{1,}$/),
                              message: 'Вес должен состоять только из цифр',
                            },
                            {
                              pattern: new RegExp(/^\S/),
                              message: 'Поле не должно содержать лишних пробелов',
                            },
                          ]}
                        >
                          <InputNumber placeholder="вес показателя" max="100" min="0" />
                        </Form.Item>
                        <MinusCircleOutlined onClick={() => remove(name)} />
                      </Space>
                    ))}
                    <Form.Item>
                      <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                        Добавить элемент КРІ
                      </Button>
                    </Form.Item>
                  </>
                )}
              </Form.List>
              <Form.Item
                label="% за выполнение 100% КРI"
                name="percentForKpi"
                rules={[
                  {
                    pattern: new RegExp(/^\S/),
                    message: 'поле не должно содержать лишних пробелов',
                  },
                  {
                    pattern: new RegExp(/^[0-9]{1,}$/),
                    message: 'Процент должен состоять только из цифр',
                  },
                ]}
              >
                <Input max="50" placeholder="Бонус за 100% КРI" />
              </Form.Item>
            </Form>
          </Col>
        </Row>
      </Modal>
    </div>
  );
};
