import { useEffect, useState } from 'react';
import { shallowEqual } from 'react-redux';
import { Link } from 'react-router-dom';
import { Breadcrumb, Table, Typography } from 'antd';
import { Button, Form, Select } from 'antd';
import { openNotification } from '../../../components/helpers/helpers';
import { useAppDispatch, useAppSelector } from '../../../hooks';
import { getKpiById, resetAddedStatus, resetKpiElements } from '../../../store/services/KpiSlice';
import { getPositions } from '../../../store/services/PositionSlice';
import { AddKpiToPosition } from './addKpi/AddKpiToPosition';
import { EditKpi } from './editKpi/EditKpi';
import { KPIHistoryPage } from './kpiHistory/KPIHistoryPage';

import './KPICatalogCreate.css';

export const KPICatalogCreate = () => {
  const { Title } = Typography;
  const { positions } = useAppSelector((state) => state.positions);
  const {
    kpiElements,
    percentForKpi,
    addedSuccessfully,
    hasError,
    message,
    kpiId,
    updatedSuccessfully,
    loading,
  } = useAppSelector((state) => state.kpi, shallowEqual);

  const dispatch = useAppDispatch();
  const { Option } = Select;
  const [isSelected, setIsSelected] = useState(false);
  const [currentSelectedPos, setCurrentSelectedPos] = useState('');
  const [showModal, setShowModal] = useState(false);
  const [showEditModal, setShowEditModal] = useState(false);
  const [showHistoryModal, setShowHistoryModal] = useState(false);
  useEffect(() => {
    dispatch(getPositions());
    form.resetFields();
    return () => {
      dispatch(resetKpiElements());
    };
  }, [dispatch]);
  useEffect(() => {
    if (addedSuccessfully) {
      openNotification('success', 'bottomLeft', message);
      setTimeout(() => {
        dispatch(resetAddedStatus());
      }, 1000);
    }

    if (hasError) {
      openNotification('error', 'bottomLeft', message);
      setTimeout(() => {
        dispatch(resetAddedStatus());
      }, 1000);
    }
    if (updatedSuccessfully) {
      dispatch(getKpiById(currentSelectedPos));
      openNotification('success', 'bottomLeft', message);
      setTimeout(() => {
        dispatch(resetAddedStatus());
      }, 1000);
    }
  }, [addedSuccessfully, hasError, updatedSuccessfully]);
  const [form] = Form.useForm();

  const columns = [
    {
      title: 'Вид',
      dataIndex: 'view',
      key: 'view',
    },
    {
      title: 'Цель',
      dataIndex: 'goal',
      key: 'goal',
    },
    {
      title: 'Описание',
      dataIndex: 'description',
      key: 'description',
    },
    {
      title: 'Примечание',
      dataIndex: 'comment',
      key: 'comment',
    },
    {
      title: 'Вес',
      dataIndex: 'weight',
      key: 'weight',
    },
  ];

  const selectPositionHandler = (value: string) => {
    dispatch(getKpiById(value));
    setCurrentSelectedPos(value);
    setIsSelected(true);
    dispatch(resetKpiElements());
  };
  const showEditModalHandler = () => {
    setShowEditModal(!showEditModal);
  };
  const showModalHandler = () => {
    setShowModal(!showModal);
  };
  const showHistoryModalHandler = () => {
    setShowHistoryModal(!showHistoryModal);
  };

  return (
    <div style={{ position: 'relative' }}>
      <Breadcrumb>
        <Breadcrumb.Item>
          <Link to="/">Главная</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>Справочник КРІ</Breadcrumb.Item>
      </Breadcrumb>
      <Title>Справочник КРІ</Title>
      <Form form={form}>
        <Form.Item label="KPI для должности" name="position">
          <Select
            style={{ width: '300px' }}
            placeholder="Выберите должность"
            onChange={selectPositionHandler}
          >
            {positions.map((position) => (
              <Option key={position._id} value={position._id}>
                {position.title}
              </Option>
            ))}
          </Select>
        </Form.Item>
        {isSelected ? (
          <>
            {kpiElements.length !== 0 ? (
              <>
                <Form.Item style={{ position: 'absolute', top: '87px', left: '450px' }}>
                  <Button
                    style={{
                      marginRight: '40px',
                    }}
                    onClick={showEditModalHandler}
                    type="primary"
                    htmlType="submit"
                  >
                    Редактировать
                  </Button>

                  <Button
                    style={{
                      marginRight: '40px',
                    }}
                    onClick={showHistoryModalHandler}
                    htmlType="submit"
                  >
                    История
                  </Button>
                </Form.Item>
                <h3>Бонус за выполнение 100% КРI: {percentForKpi}%</h3>
                <Table
                  loading={loading}
                  rowKey={(record) => record.goal + Math.random() + Math.random()}
                  columns={columns}
                  dataSource={kpiElements}
                />

                <EditKpi
                  selectedPosition={currentSelectedPos}
                  kpiId={kpiId}
                  showModal={showEditModal}
                  closeModal={showEditModalHandler}
                />
                <KPIHistoryPage showModal={showHistoryModal} closeModal={showHistoryModalHandler} />
              </>
            ) : (
              <>
                <p className="textKpi">Нет созданных КРІ</p>
                <Form.Item>
                  <Button
                    onClick={showModalHandler}
                    style={{
                      marginRight: '40px',
                      position: 'absolute',
                      top: '-178px',
                      left: '450px',
                    }}
                    type="primary"
                    htmlType="submit"
                  >
                    Создать KPI
                  </Button>
                </Form.Item>
                <AddKpiToPosition
                  positionId={currentSelectedPos}
                  showModal={showModal}
                  closeModal={showModalHandler}
                />
              </>
            )}
          </>
        ) : (
          <p className="textKpi">Не выбрана должность</p>
        )}
      </Form>
    </div>
  );
};
