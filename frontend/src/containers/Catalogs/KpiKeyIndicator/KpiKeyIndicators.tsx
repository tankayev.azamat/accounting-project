import { ChangeEvent, useEffect, useState } from 'react';
import { shallowEqual } from 'react-redux';
import { Link } from 'react-router-dom';
import { Breadcrumb, Button, Col, Form, Row, Table, Typography } from 'antd';
import { openNotification } from '../../../components/helpers/helpers';
import { useAppDispatch, useAppSelector } from '../../../hooks';
import {
  addKpiKeyIndicator,
  getKpiKeyIndicatorElements,
  resetAddedStatus,
} from '../../../store/services/KpiKeyIndicatorsSlice';
import { AddNewKpiIndicatorElement } from './AddNewKpiIndicatorElement/AddNewKpiIndicatorElement';

export const KpiKeyIndicators = () => {
  const { Title } = Typography;
  const dispatch = useAppDispatch();
  const { kpiKeyIndicatorElements, addedSuccessfully, message, hasError, loading } = useAppSelector(
    (state) => state.kpiIndicatorKeys,
    shallowEqual
  );

  useEffect(() => {
    dispatch(getKpiKeyIndicatorElements());
  }, [dispatch]);

  useEffect(() => {
    if (addedSuccessfully) {
      openNotification('success', 'bottomLeft', message);
      setTimeout(() => {
        dispatch(resetAddedStatus());
      }, 1000);
    }
    if (hasError) {
      openNotification('error', 'bottomLeft', message);
      setTimeout(() => {
        dispatch(resetAddedStatus());
      }, 1000);
    }
  }, [addedSuccessfully, hasError]);
  const columns = [
    {
      title: 'Ключевые показатели',
      dataIndex: 'title',
      key: 'title',
    },
  ];

  const [isDrawer, setIsDrawer] = useState(false);

  const [newElement, setNewElement] = useState({
    title: '',
  });

  const [form] = Form.useForm();

  const closeDrawerHandler = () => {
    setIsDrawer(false);
    form.resetFields(['title', 'elementCode']);
    setNewElement({
      title: '',
    });
  };

  const inputChangeHandler = (e: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setNewElement((prevState) => ({ ...prevState, [name]: value.trim() }));
  };
  const showDrawerMenu = () => {
    setIsDrawer(true);
  };

  const sendNewElemetHandler = () => {
    const newServiceElement = {
      title: newElement.title,
    };
    dispatch(addKpiKeyIndicator(newServiceElement));
    form.resetFields(['title']);
    closeDrawerHandler();
  };

  const onFinishFailed = () => {
    openNotification('error', 'bottomLeft', 'Пожалуйста, введите обязательные поля');
  };

  return (
    <>
      <Breadcrumb>
        <Breadcrumb.Item>
          <Link to="/">Главная</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>Ключевые показатели KPI</Breadcrumb.Item>
      </Breadcrumb>
      <Title>Справочник видов ключевых показателей KPI</Title>
      <Row>
        <Col xs={24} md={{ span: 16, offset: 1 }}>
          <Button size="large" style={{ margin: '10px 0' }} type="primary" onClick={showDrawerMenu}>
            Добавить элемент
          </Button>
          <Table
            loading={loading}
            style={{ width: '400px' }}
            dataSource={kpiKeyIndicatorElements}
            columns={columns}
            rowKey={(record) => record._id}
          />
        </Col>
      </Row>
      <AddNewKpiIndicatorElement
        isDrawer={isDrawer}
        closeDrawerHandler={closeDrawerHandler}
        sendNewElementHandler={sendNewElemetHandler}
        inputChangeHandler={inputChangeHandler}
        form={form}
        onFinishFailed={onFinishFailed}
        newElement={newElement}
      />
    </>
  );
};
