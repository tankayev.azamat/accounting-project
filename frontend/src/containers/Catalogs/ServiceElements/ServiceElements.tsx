import { ChangeEvent, useEffect, useState } from 'react';
import { shallowEqual } from 'react-redux';
import { Link } from 'react-router-dom';
import { Breadcrumb, Button, Col, Form, Row, Typography } from 'antd';
import { openNotification } from '../../../components/helpers/helpers';
import { useAppDispatch, useAppSelector } from '../../../hooks';
import {
  addNewServiceElement,
  getServiceElementsList,
  resetStatus,
} from '../../../store/services/ServiceElementsSlice';
import { AddNewServiceElement } from './addNewServiceElement/AddNewServiceElement';
import { ServiceElementsList } from './serviceElementsList/ServiceElementsList';

import './ServiceElements.css';

export const ServiceElements = () => {
  const { Title } = Typography;
  const dispatch = useAppDispatch();
  const { serviceElementsList, successfully, message, hasError, loading } = useAppSelector(
    (state) => state.serviceElements,
    shallowEqual
  );

  const [isDrawer, setIsDrawer] = useState(false);

  const [newElement, setNewElement] = useState({
    title: '',
  });
  const [form] = Form.useForm();

  useEffect(() => {
    if (successfully) {
      openNotification('success', 'bottomLeft', message);
      setTimeout(() => {
        dispatch(resetStatus());
      }, 1000);
    }
    if (hasError) {
      openNotification('error', 'bottomLeft', message);
      setTimeout(() => {
        dispatch(resetStatus());
      }, 1000);
    }
  }, [successfully, hasError]);

  useEffect(() => {
    dispatch(getServiceElementsList());
    if (successfully) {
      dispatch(getServiceElementsList());
    }
  }, [dispatch, ServiceElementsList, successfully]);

  const closeDrawerHandler = () => {
    setIsDrawer(false);
    form.resetFields(['title']);
    setNewElement({
      title: '',
    });
  };

  const inputChangeHandler = (e: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setNewElement((prevState) => ({ ...prevState, [name]: value.trim() }));
  };
  const showDrawerMenu = () => {
    setIsDrawer(true);
  };

  const sendNewElemetHandler = () => {
    const newServiceElement = {
      title: newElement.title,
    };
    dispatch(addNewServiceElement(newServiceElement));
    form.resetFields(['title']);
    closeDrawerHandler();
  };

  const onFinishFailed = () => {
    openNotification('error', 'bottomLeft', 'Пожалуйста, введите обязательные поля');
  };

  return (
    <>
      <Breadcrumb>
        <Breadcrumb.Item>
          <Link to="/">Главная</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>Элементы услуг</Breadcrumb.Item>
      </Breadcrumb>
      <Title>Справочник элементов услуг</Title>
      <Row>
        <Col xs={24} md={{ span: 16, offset: 1 }}>
          <Button className="addElementBtn" size="large" type="primary" onClick={showDrawerMenu}>
            Добавить элемент
          </Button>
          <ServiceElementsList loading={loading} elements={serviceElementsList} />

          <AddNewServiceElement
            isDrawer={isDrawer}
            closeDrawerHandler={closeDrawerHandler}
            sendNewElementHandler={sendNewElemetHandler}
            inputChangeHandler={inputChangeHandler}
            form={form}
            onFinishFailed={onFinishFailed}
            newElement={newElement}
          />
        </Col>
      </Row>
    </>
  );
};
