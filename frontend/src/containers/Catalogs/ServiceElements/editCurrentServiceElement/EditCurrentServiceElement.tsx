import { ChangeEvent, useEffect } from 'react';
import { Button, Drawer, Form, Input, Space } from 'antd';
import { FormInstance } from 'antd/es/form/Form';

interface Props {
  closeEditingDrawerHandler?: () => void;
  editingInputChangeHandler: (e: ChangeEvent<HTMLInputElement>) => void;
  currentServiceElement?: {
    title: string;
  };
  form?: FormInstance;
  isDrawer?: boolean;
  sendEditElementHandler?: () => void;
}

export const EditCurrentServiceElementFunc = ({
  isDrawer,
  currentServiceElement,
  form,
  closeEditingDrawerHandler,
  editingInputChangeHandler,
  sendEditElementHandler,
}: Props) => {
  useEffect(() => {
    form ? form.setFieldsValue(currentServiceElement) : null;
  }, [form, currentServiceElement]);
  return (
    <>
      <Drawer
        title="Редактировать элемент услуги"
        placement="right"
        onClose={closeEditingDrawerHandler}
        open={isDrawer}
      >
        <Space direction="vertical">
          <Form
            form={form}
            name="basic"
            preserve={false}
            labelCol={{ span: 10 }}
            wrapperCol={{ span: 16 }}
            initialValues={currentServiceElement}
            onFinish={sendEditElementHandler}
            autoComplete="off"
          >
            <Form.Item
              label="Наименование"
              name="title"
              rules={[
                { required: true, message: 'Пожалуйста, введите наименование элемента' },
                {
                  pattern: new RegExp(/^\S/),
                  message: 'поле не должно содержать лишних пробелов',
                },
              ]}
            >
              <Input name="title" onChange={editingInputChangeHandler} />
            </Form.Item>
            <Form.Item wrapperCol={{ offset: 0 }}>
              <Button type="primary" htmlType="submit" style={{ margin: '15px' }} value="large">
                Сохранить
              </Button>
              <Button
                style={{ margin: '15px' }}
                onClick={closeEditingDrawerHandler}
                value="large"
                type="primary"
                danger
              >
                Отмена
              </Button>
            </Form.Item>
          </Form>
        </Space>
      </Drawer>
    </>
  );
};
