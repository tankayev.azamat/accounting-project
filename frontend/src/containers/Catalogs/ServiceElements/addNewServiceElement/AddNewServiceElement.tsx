import { ChangeEvent, useState } from 'react';
import { Button, Drawer, Form, Input, Space } from 'antd';
import { FormInstance } from 'antd/es/form/Form';

interface Props {
  closeDrawerHandler?: () => void;
  inputChangeHandler?: (e: ChangeEvent<HTMLInputElement>) => void;
  newElement?: {
    title: string;
  };
  onFinishFailed?: () => void;
  sendNewElementHandler?: () => void;
  form?: FormInstance;
  isDrawer?: boolean;
}

export const AddNewServiceElement = ({
  closeDrawerHandler,
  isDrawer,
  form,
  sendNewElementHandler,
  onFinishFailed,
  newElement,
  inputChangeHandler,
}: Props) => {
  const [isDisabled, setIsDisabled] = useState(true);
  const changeFormHandler = () => {
    const hasErrors = form?.getFieldsError().some(({ errors }) => errors.length);
    setIsDisabled(hasErrors ? hasErrors : false);
  };
  return (
    <>
      <Drawer
        title="Добавить новый элемент услуги"
        placement="right"
        onClose={closeDrawerHandler}
        open={isDrawer}
      >
        <Space direction="vertical">
          <Form
            form={form}
            name="basic"
            labelCol={{ span: 10 }}
            wrapperCol={{ span: 16 }}
            initialValues={{ remember: false }}
            onFinish={sendNewElementHandler}
            onFinishFailed={onFinishFailed}
            onFieldsChange={changeFormHandler}
            autoComplete="off"
          >
            <Form.Item
              label="Наименование"
              name="title"
              rules={[
                { required: true, message: 'Пожалуйста, введите наименование элемента' },
                {
                  pattern: new RegExp(/^\S/),
                  message: 'поле не должно содержать лишних пробелов',
                },
              ]}
            >
              <Input value={newElement?.title} name="title" onChange={inputChangeHandler} />
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 0 }}>
              <Button
                disabled={isDisabled}
                type="primary"
                htmlType="submit"
                style={{ margin: '15px' }}
                value="large"
              >
                Сохранить
              </Button>
              <Button
                style={{ margin: '15px' }}
                onClick={closeDrawerHandler}
                value="large"
                type="primary"
                danger
              >
                Отмена
              </Button>
            </Form.Item>
          </Form>
        </Space>
      </Drawer>
    </>
  );
};
