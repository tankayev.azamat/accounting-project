import { ChangeEvent, useState } from 'react';
import { Button, Form, Modal, Table } from 'antd';
import { EditOutlined, ExclamationCircleOutlined } from '@ant-design/icons';
import { openNotification } from '../../../../components/helpers/helpers';
import { useAppDispatch, useAppSelector } from '../../../../hooks';
import { editedPosition, editPosition, IPosition } from '../../../../store/services/PositionSlice';
import { EditJobDirectory } from '../EditJobDirectory/EditJobDirectory';

export const JobDirectoryList = () => {
  const [isEditDrawer, setIsEditDrawer] = useState(false);
  const { positions, openEditModal, loading } = useAppSelector((state) => state.positions);
  const [editJobDirectory, setEditJobDirectory] = useState({
    _id: '',
    title: '',
    headPositionId: '',
  });
  const [form] = Form.useForm();
  const dispatch = useAppDispatch();
  const showDrawer = (value: any) => {
    setIsEditDrawer(true);
    setEditJobDirectory(value);
  };

  const columns = [
    {
      title: 'Наименование должности',
      dataIndex: 'title',
      key: 'title',
    },
    {
      key: 'more',
      title: 'Действие',
      render: (record: IPosition) => {
        return (
          <Button onClick={() => showDrawer(record)} type="primary">
            {' '}
            <EditOutlined />
          </Button>
        );
      },
    },
  ];

  const inputEditChangeHandler = (e: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setEditJobDirectory((prevState) => ({ ...prevState, [name]: value }));
  };

  const closeEditDrawerHandler = () => {
    setIsEditDrawer(false);
    form.resetFields(['title', '_id', 'headPositionId']);
    setEditJobDirectory({
      title: '',
      headPositionId: '',
      _id: '',
    });
  };

  const sendEditDirectoryHandler = () => {
    const onOk = async () => {
      dispatch(editPosition(editJobDirectory));
      dispatch(editedPosition(editJobDirectory));
      closeEditDrawerHandler();
      openEditModal === false;
    };

    const hideModal = () => {
      openEditModal === false;
    };

    Modal.confirm({
      title: 'Предупреждение',
      icon: <ExclamationCircleOutlined />,
      content: 'Вы действительно хотите изменить данные о должности ?',
      okText: 'OK',
      cancelText: 'Отменить',
      open: openEditModal,
      onOk: onOk,
      onCancel: hideModal,
    });
  };

  const onFinishFailed = () => {
    openNotification('warning', 'bottomLeft', 'Пожалуйста, заполните обязательные поля');
  };
  return (
    <>
      <Table
        loading={loading}
        dataSource={positions}
        rowKey={(render) => render._id}
        columns={columns}
        style={{ width: '400px', marginTop: '20px' }}
      />
      <Form form={form} />
      <EditJobDirectory
        closeEditDrawerHandler={closeEditDrawerHandler}
        isEditDrawer={isEditDrawer}
        form={form}
        sendEditDirectoryHandler={sendEditDirectoryHandler}
        onFinishFailed={onFinishFailed}
        editJobDirectory={editJobDirectory}
        inputEditChangeHandler={inputEditChangeHandler}
      />
    </>
  );
};
