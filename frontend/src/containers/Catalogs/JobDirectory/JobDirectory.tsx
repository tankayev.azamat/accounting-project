import { ChangeEvent, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { Breadcrumb, Button, Col, Form, Row, Typography } from 'antd';
import { FileAddOutlined } from '@ant-design/icons';
import { openNotification } from '../../../components/helpers/helpers';
import { useAppDispatch, useAppSelector } from '../../../hooks';
import { createPosition, getPositions, resetStatus } from '../../../store/services/PositionSlice';
import { CreateJobDirectory } from './CreateJobDirectory/CreateJobDirectory';
import { JobDirectoryList } from './JobDirectoryList/JobDirectoryList';

export const JobDirectory: React.FC = () => {
  const { Title } = Typography;
  const [isDrawer, setIsDrawer] = useState(false);
  const [form] = Form.useForm();
  const dispatch = useAppDispatch();
  const { message, successfully, hasError } = useAppSelector((state) => state.positions);

  const [jobDirectory, setJobDirectory] = useState({
    title: '',
    headPositionId: null,
  });

  useEffect(() => {
    dispatch(getPositions());
  }, [dispatch]);

  useEffect(() => {
    if (successfully) {
      openNotification('success', 'bottomLeft', message);
      setTimeout(() => {
        dispatch(resetStatus());
      }, 1000);
    }
    if (hasError) {
      openNotification('error', 'bottomLeft', message);
      setTimeout(() => {
        dispatch(resetStatus());
      }, 1000);
    }
  }, [successfully, hasError]);

  const inputChangeHandler = (e: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setJobDirectory((prevState) => ({ ...prevState, [name]: value }));
  };

  const showDrawerMenu = () => {
    setIsDrawer(true);
  };

  const closeDrawerHandler = () => {
    setIsDrawer(false);
    form.resetFields(['title', '_id', 'headPositionId']);
    setJobDirectory({
      title: '',
      headPositionId: null,
    });
  };

  const sendNewDirectoryHandler = () => {
    dispatch(createPosition(jobDirectory));
    closeDrawerHandler();
  };

  const onFinishFailed = () => {
    openNotification('warning', 'bottomLeft', 'Пожалуйста, заполните обязательные поля');
  };
  return (
    <>
      <Breadcrumb>
        <Breadcrumb.Item>
          <Link to="/">Главная</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>Должности</Breadcrumb.Item>
      </Breadcrumb>
      <Row>
        <Col xs={24} md={{ span: 20, offset: 1 }}>
          <Title>Список должностей</Title>
          <Button type="primary" onClick={showDrawerMenu} icon={<FileAddOutlined />} size="large">
            Добавить должность
          </Button>
          <JobDirectoryList />
        </Col>
      </Row>

      <CreateJobDirectory
        closeDrawerHandler={closeDrawerHandler}
        isDrawer={isDrawer}
        form={form}
        sendNewDirectoryHandler={sendNewDirectoryHandler}
        onFinishFailed={onFinishFailed}
        newDirectory={jobDirectory}
        inputChangeHandler={inputChangeHandler}
      />
    </>
  );
};
