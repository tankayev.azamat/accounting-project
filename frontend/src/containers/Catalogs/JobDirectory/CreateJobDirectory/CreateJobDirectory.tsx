import { ChangeEvent, useState } from 'react';
import { Button, Drawer, Form, FormInstance, Input, Space } from 'antd';

interface Props {
  closeDrawerHandler?: () => void;
  inputChangeHandler?: (e: ChangeEvent<HTMLInputElement>) => void;
  newDirectory?: {
    title: string;
    headPositionId: string | null;
  };
  onFinishFailed?: () => void;
  sendNewDirectoryHandler?: () => void;
  form?: FormInstance;
  isDrawer?: boolean;
}

export const CreateJobDirectory = ({
  closeDrawerHandler,
  isDrawer,
  form,
  sendNewDirectoryHandler,
  onFinishFailed,
  newDirectory,
  inputChangeHandler,
}: Props) => {
  const [isDisabled, setIsDisabled] = useState(true);
  const changeFormHandler = () => {
    const hasErrors = form?.getFieldsError().some(({ errors }) => errors.length);
    setIsDisabled(hasErrors ? hasErrors : false);
  };
  return (
    <>
      <Drawer
        title="Добавить новую должность"
        placement="right"
        onClose={closeDrawerHandler}
        open={isDrawer}
      >
        <Space direction="vertical">
          <Form
            form={form}
            name="basic"
            labelCol={{ span: 14 }}
            wrapperCol={{ span: 20 }}
            initialValues={{ remember: false }}
            onFinish={sendNewDirectoryHandler}
            onFinishFailed={onFinishFailed}
            onFieldsChange={changeFormHandler}
            autoComplete="off"
          >
            <Form.Item
              label="Наименование"
              name="title"
              rules={[
                { required: true, message: 'Пожалуйста, введите наименование должности' },
                {
                  pattern: new RegExp(/^\S/),
                  message: 'поле не должно содержать лишних пробелов',
                },
              ]}
            >
              <Input
                name="title"
                placeholder="Наименование"
                value={newDirectory?.title}
                onChange={inputChangeHandler}
              />
            </Form.Item>
            <Form.Item>
              <Button
                disabled={isDisabled}
                type="primary"
                htmlType="submit"
                style={{ margin: '15px' }}
                value="large"
              >
                Сохранить
              </Button>
              <Button
                style={{ margin: '15px' }}
                onClick={closeDrawerHandler}
                value="large"
                type="primary"
                danger
              >
                Отмена
              </Button>
            </Form.Item>
          </Form>
        </Space>
      </Drawer>
    </>
  );
};
