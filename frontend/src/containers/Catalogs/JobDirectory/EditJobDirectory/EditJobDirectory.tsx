import { ChangeEvent, useEffect } from 'react';
import { Button, Drawer, Form, FormInstance, Input, Space } from 'antd';

interface Props {
  closeEditDrawerHandler?: () => void;
  inputEditChangeHandler?: (e: ChangeEvent<HTMLInputElement>) => void;
  editJobDirectory?: {
    _id: string;
    title: string | undefined;
    headPositionId: string | undefined;
  };
  onFinishFailed?: () => void;
  sendEditDirectoryHandler?: () => void;
  form?: FormInstance;
  isEditDrawer?: boolean;
}

export const EditJobDirectory = ({
  closeEditDrawerHandler,
  isEditDrawer,
  form,
  sendEditDirectoryHandler,
  onFinishFailed,
  editJobDirectory,
  inputEditChangeHandler,
}: Props) => {
  useEffect(() => {
    form ? form.setFieldsValue(editJobDirectory) : null;
  }, [form, editJobDirectory]);
  return (
    <>
      <Drawer
        title="Редактировать должность"
        placement="right"
        onClose={closeEditDrawerHandler}
        open={isEditDrawer}
      >
        <Space direction="vertical">
          <Form
            form={form}
            name="basic"
            labelCol={{ span: 14 }}
            wrapperCol={{ span: 20 }}
            initialValues={{ remember: false }}
            onFinish={sendEditDirectoryHandler}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <Form.Item
              label="Наименование"
              name="title"
              rules={[
                { required: true, message: 'Пожалуйста, введите наименование должности' },
                {
                  pattern: new RegExp(/^\S/),
                  message: 'поле не должно содержать лишних пробелов',
                },
              ]}
              initialValue={editJobDirectory?.title}
            >
              <Input name="title" placeholder="Наименование" onChange={inputEditChangeHandler} />
            </Form.Item>
            <Form.Item>
              <Button type="primary" htmlType="submit" style={{ margin: '15px' }} value="large">
                Сохранить
              </Button>
              <Button
                style={{ margin: '15px' }}
                onClick={closeEditDrawerHandler}
                value="large"
                type="primary"
                danger
              >
                Отмена
              </Button>
            </Form.Item>
          </Form>
        </Space>
      </Drawer>
    </>
  );
};
