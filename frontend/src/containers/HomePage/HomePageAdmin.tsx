import { useEffect } from 'react';
import { Button, Card, Col, Row, Spin, Typography } from 'antd';
import { useAppDispatch, useAppSelector } from '../../hooks';
import {
  clearMainPageArrayState,
  getEmployeeMainPage,
  getEmployeeMainPagePayment,
} from '../../store/services/EmployeesListSlice';

export function HomePageAdmin() {
  const dispatch = useAppDispatch();
  const { employeeAndHeadEmployee, loading, totalPaymentSum } = useAppSelector(
    (state) => state.employees
  );
  const { Title } = Typography;

  useEffect(() => {
    dispatch(getEmployeeMainPage());
    dispatch(getEmployeeMainPagePayment());

    return () => {
      dispatch(clearMainPageArrayState());
    };
  }, [dispatch]);

  return (
    <Row gutter={[16, 16]}>
      {loading ? (
        <Spin />
      ) : (
        <>
          <Col span={24}>
            <Title level={2}>Информация по пирамидам</Title>
          </Col>
          <Col span={24}>
            <Title level={4}>
              Общая абонентская плата по активным клиентам: {totalPaymentSum?.toLocaleString('ru')}{' '}
              ₸
            </Title>
          </Col>
          {employeeAndHeadEmployee?.map((empl) => (
            <Col span={6} key={empl._doc._id}>
              <Card
                title={`Пирамида ${empl.index}`}
                bordered
                style={{
                  width: '100%',
                  maxWidth: '300px',
                  height: '300px',
                  borderRadius: '20px',
                  border: '1px solid black',
                }}
              >
                <p>{`Ведущий бухгалтер: ${empl._doc.firstName} ${empl._doc.lastName}`}</p>
                <p>{`Количество сотрудников: ${empl.emp.length}`}</p>
                <Button
                  style={{
                    marginTop: '60px',
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}
                  type="primary"
                >
                  Подробнее
                </Button>
              </Card>
            </Col>
          ))}
        </>
      )}
    </Row>
  );
}
