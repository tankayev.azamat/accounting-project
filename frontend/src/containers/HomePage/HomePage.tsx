import { useAppSelector } from '../../hooks';
import { HomePageAdmin } from './HomePageAdmin';
import { HomePageEmployee } from './HomePageEmployee';

export function HomePage() {
  const user = useAppSelector((state) => state.users.user?.user);
  if (user?.roleId === process.env.REACT_APP_MAIN_ROLE) return <HomePageAdmin />;
  return <HomePageEmployee />;
}
