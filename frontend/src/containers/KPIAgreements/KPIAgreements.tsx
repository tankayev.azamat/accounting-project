import { Link, useNavigate } from 'react-router-dom';
import { Breadcrumb, Button, Select, Typography } from 'antd';
import Table from 'antd/lib/table';

interface IData {
  id: number;
  month: string;
  numOfEmployees: number;
  numOfKPI: number;
  status: string;
}
const mockData = [
  {
    id: 1,
    month: 'Январь',
    numOfEmployees: 23,
    numOfKPI: 13,
    status: 'Закрыт',
  },
  {
    id: 2,
    month: 'Февраль',
    numOfEmployees: 33,
    numOfKPI: 29,
    status: 'В процессе',
  },
  {
    id: 3,
    month: 'Март',
    numOfEmployees: 40,
    numOfKPI: 40,
    status: 'Закрыт',
  },
  {
    id: 4,
    month: 'Апрель',
    numOfEmployees: 38,
    numOfKPI: 37,
    status: 'В процессе',
  },
];

const optionForSelect = [
  {
    value: 2022,
    label: 2022,
  },
  {
    value: 2023,
    label: 2023,
  },
];

export const KPIAgreements = () => {
  const { Title } = Typography;
  const navigate = useNavigate();
  const openMonthlyAgreements = (record: IData) => {
    navigate(`/kpi_agreements/${record.id}`);
  };
  const columns = [
    {
      title: '№',
      dataIndex: 'id',
      key: 'id',
    },
    {
      title: 'Месяц',
      dataIndex: 'month',
      key: 'month',
    },
    {
      title: 'Кол-во сотрудников',
      dataIndex: 'numOfEmployees',
      key: 'numOfEmployees',
    },
    {
      title: 'Кол-во KPI',
      dataIndex: 'numOfKPI',
      key: 'numOfKPI',
    },
    {
      title: 'Статус',
      dataIndex: 'status',
      key: 'status',
    },
    {
      title: 'Действие',
      key: 'action',
      render: (record: IData) => {
        return (
          <Button type="primary" onClick={() => openMonthlyAgreements(record)}>
            {' '}
            Подробнее
          </Button>
        );
      },
    },
  ];
  return (
    <div>
      <Breadcrumb>
        <Breadcrumb.Item>
          <Link to="/">Главная</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>Согласование</Breadcrumb.Item>
      </Breadcrumb>
      <Title>Согласование</Title>
      <div
        style={{
          marginBottom: 20,
        }}
      >
        <Select
          style={{ width: '90px' }}
          options={optionForSelect}
          size="large"
          placeholder="Выберите год"
        />
      </div>
      <Table
        dataSource={mockData}
        columns={columns}
        size="middle"
        rowKey={(record) => record.id + Math.random() + Math.random()}
        pagination={false}
      />
    </div>
  );
};
