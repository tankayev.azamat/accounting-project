import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from '../../axiosApi';
import { IEmployee } from './EmployeesListSlice';

export interface IHocService {
  _id: string;
  serviceElements: string | undefined;
  sum: number;
  startDate: Date | null;
  endDate?: Date | null;
  payDate?: Date | null;
  description: string | null;
  employeeId: IEmployee[];
  clientId: string;
}
export interface INewHocService {
  serviceElements: string | undefined;
  sum: number;
  startDate: Date | null;
  endDate?: Date | null;
  payDate?: Date | null;
  description: string | null;
  employeeId: string[];
  clientId: string | undefined;
}
interface ServiceRatesState {
  hocServiceList: IHocService[];
  isLoadingHoc: boolean;
  error: string | null;
  open: boolean;
  currentClientHocService: IHocService[];
}

type id = string | undefined;

export const getHocService = createAsyncThunk('get/client/hocService', async () => {
  const res = await axios.get('/admin/client/hocService/');
  return res.data;
});

export const getHocServiceCurrentClient = createAsyncThunk(
  'get/current/client/hocService',
  async (id: id) => {
    const res = await axios.get(`/admin/client/hocService?client=${id}`);
    return res.data;
  }
);

export const createNewHocService = createAsyncThunk(
  'create/client/hocService',
  async (newData: INewHocService) => {
    const res = await axios.post('/admin/client/hocService/', newData);
    return res.data;
  }
);

const initialState: ServiceRatesState = {
  hocServiceList: [],
  currentClientHocService: [],
  isLoadingHoc: false,
  error: '',
  open: false,
};

const HocServiceSlice = createSlice({
  name: 'hoc/service',
  initialState,
  extraReducers: (builder) => {
    builder
      .addCase(getHocService.pending, (state) => {
        state.isLoadingHoc = true;
      })
      .addCase(getHocService.rejected, (state, action) => {
        state.error = action.error.message || 'error get query';
        state.isLoadingHoc = false;
      })
      .addCase(getHocService.fulfilled, (state, action) => {
        state.isLoadingHoc = false;
        state.hocServiceList = action.payload;
      })
      .addCase(createNewHocService.pending, (state) => {
        state.isLoadingHoc = true;
      })
      .addCase(createNewHocService.rejected, (state, action) => {
        state.isLoadingHoc = false;
        state.error = action.error.message || 'error post query';
      })
      .addCase(createNewHocService.fulfilled, (state, action) => {
        state.currentClientHocService.push(action.payload);
        state.isLoadingHoc = false;
      })
      .addCase(getHocServiceCurrentClient.pending, (state) => {
        state.isLoadingHoc = true;
      })
      .addCase(getHocServiceCurrentClient.rejected, (state, action) => {
        state.error = action.error.message || 'error get hoc current service';
        state.isLoadingHoc = false;
      })
      .addCase(getHocServiceCurrentClient.fulfilled, (state, action) => {
        state.isLoadingHoc = false;
        state.currentClientHocService = action.payload;
      });
  },
  reducers: {},
});
export default HocServiceSlice.reducer;
