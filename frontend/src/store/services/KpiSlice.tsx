import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { AxiosError } from 'axios';
import axios from '../../axiosApi';
import { IEmployee } from './EmployeesListSlice';

interface errorType {
  message: string;
}
export interface IKpi {
  _id: string;
  positionId: string;
  kpiElements?: Elements[];
}
export interface Elements {
  view: string;
  goal: string;
  description: string;
  comment: string;
  weight: number;
}
interface IKpiState {
  kpiError: errorType | null;
  global?: string | null;
  hasError: boolean;
  loading: boolean;
  kpiElements: Elements[];
  percentForKpi: number;
  kpiId: string | null;
  addedSuccessfully: boolean;
  updatedSuccessfully: boolean;
  message: string | null;
  employee: IEmployee[];
}
export interface INewKpi {
  positionId: string;
  kpiElements: Elements[];
  percentForKpi: number;
}

interface EditingData {
  id: string | null;
  data: {
    kpiElements: Elements[];
    percentForKpi: number;
    positionId: string;
  };
}
type id = string | undefined;

export const addKpi = createAsyncThunk('post/kpi', async (kpiData: INewKpi) => {
  const res = await axios.post('/admin/kpi/', kpiData);
  return res.data;
});

export const editKpiFetch = createAsyncThunk('put/kpi', async (payload: EditingData) => {
  const res = await axios.put(`/admin/kpi/${payload.id}`, payload.data);
  return res.data;
});

export const getKpiById = createAsyncThunk('get/kpi/id', async (id: id) => {
  const res = await axios.get(`/admin/kpi/${id}`);
  return res.data;
});

export const getKpiFromEmployee = createAsyncThunk('get/kpiFromEmployee', async (_, thunkApi) => {
  try {
    const res = await axios.get(`/employee/kpi/`);
    return res.data;
  } catch (e) {
    const err = e as AxiosError;
    if (err.response && err.response.data) {
      thunkApi.dispatch(KpiSlice.actions.catchKpiError(err.response.data));
    } else {
      thunkApi.dispatch(KpiSlice.actions.globalKpiError(err.message));
    }
  }
});

export const getEmployee = createAsyncThunk('get/employee', async (_, thunkApi) => {
  try {
    const res = await axios.get('/employee/kpi/employee');
    return res.data;
  } catch (e) {
    const err = e as AxiosError;
    if (err.response && err.response.data) {
      thunkApi.dispatch(KpiSlice.actions.catchKpiError(err.response.data));
    } else {
      thunkApi.dispatch(KpiSlice.actions.globalKpiError(err.message));
    }
  }
});

const initialState: IKpiState = {
  kpiError: null,
  global: null,
  hasError: false,
  loading: false,
  kpiElements: [],
  percentForKpi: 0,
  kpiId: null,
  addedSuccessfully: false,
  updatedSuccessfully: false,
  message: null,
  employee: [],
};

const KpiSlice = createSlice({
  name: 'kpi',
  initialState,
  reducers: {
    resetKpiElements: (state) => {
      state.kpiElements = [];
      state.percentForKpi = 0;
    },
    resetAddedStatus: (state) => {
      state.addedSuccessfully = false;
      state.hasError = false;
      state.message = null;
      state.updatedSuccessfully = false;
    },
    catchKpiError: (state, action) => {
      state.kpiError = action.payload;
    },
    globalKpiError: (state, action) => {
      state.global = action.payload;
    },
    clearKpiState: (state) => {
      state.kpiElements = [];
      state.kpiError = null;
      state.percentForKpi = 0;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(addKpi.pending, (state) => {
        state.loading = true;
      })
      .addCase(addKpi.fulfilled, (state, action) => {
        state.addedSuccessfully = true;
        state.kpiElements = action.payload.newKpi.kpiElements;
        state.percentForKpi = action.payload.newKpi.percentForKpi;
        state.message = action.payload.message;
        state.kpiId = action.payload.newKpi._id;
        state.loading = false;
      })
      .addCase(getKpiById.pending, (state) => {
        state.loading = true;
      })
      .addCase(getKpiById.fulfilled, (state, action) => {
        state.loading = false;
        if (action.payload[0]?.kpiElements) {
          state.kpiElements = action.payload[0].kpiElements;
          state.percentForKpi = action.payload[0].percentForKpi;
          state.kpiId = action.payload[0]._id;
        } else {
          state.kpiElements = action.payload;
        }
      })
      .addCase(editKpiFetch.pending, (state) => {
        state.loading = true;
      })
      .addCase(editKpiFetch.fulfilled, (state, action) => {
        state.loading = false;
        state.updatedSuccessfully = true;
        state.message = action.payload.message;
      })
      .addCase(getKpiFromEmployee.pending, (state) => {
        state.kpiError = null;
        state.loading = true;
      })
      .addCase(getKpiFromEmployee.fulfilled, (state, action) => {
        state.loading = false;
        state.kpiError = null;
        if (action.payload[0]?.kpiElements) {
          state.kpiElements = action.payload[0].kpiElements;
          state.percentForKpi = action.payload[0].percentForKpi;
          state.kpiId = action.payload[0]._id;
          state.kpiError = null;
        } else {
          state.kpiElements = action.payload;
          state.kpiError = null;
        }
      })
      .addCase(getEmployee.pending, (state) => {
        state.loading = true;
      })
      .addCase(getEmployee.fulfilled, (state, action) => {
        state.loading = false;
        state.employee = action.payload;
      });
  },
});
export const { resetKpiElements, resetAddedStatus, clearKpiState } = KpiSlice.actions;
export default KpiSlice.reducer;
