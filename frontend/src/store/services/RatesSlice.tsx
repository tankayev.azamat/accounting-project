import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { AxiosError } from 'axios';
import axios from '../../axiosApi';

export interface IRates {
  _id: string;
  elementsOfService: string;
  price: number;
}
export interface INewRates {
  elementsOfService: string;
  price: number;
}
interface ServiceRatesState {
  ratesList: IRates[];
  loading: boolean;
  error: string | null;
  open: boolean;
  message: string | null;
  successfully: boolean;
  hasError: boolean;
}

export const getRates = createAsyncThunk('get/rates', async () => {
  const res = await axios.get('/admin/rates/');
  return res.data;
});

export const createNewRates = createAsyncThunk(
  'create/rates',
  async (newData: INewRates, thunkApi) => {
    try {
      const res = await axios.post('/admin/rates', newData);
      return res.data;
    } catch (e) {
      const err = e as AxiosError;
      if (err.response && err.response.data) {
        thunkApi.dispatch(RatesSlice.actions.catchError(err.response.data));
      }
    }
  }
);

export const editRatesPut = createAsyncThunk(
  'edit/positions/',
  async (directory: IRates, thunkApi) => {
    try {
      const res = await axios.put(`/admin/rates/${directory._id}`, directory);
      return res.data;
    } catch (e) {
      const err = e as AxiosError;
      if (err.response && err.response.data) {
        thunkApi.dispatch(RatesSlice.actions.catchError(err.response.data));
      }
    }
  }
);

const initialState: ServiceRatesState = {
  ratesList: [],
  loading: false,
  error: '',
  open: false,
  message: null,
  successfully: false,
  hasError: false,
};

const RatesSlice = createSlice({
  name: 'rates',
  initialState,
  extraReducers: (builder) => {
    builder
      .addCase(getRates.pending, (state) => {
        state.loading = true;
      })
      .addCase(getRates.rejected, (state, action) => {
        state.error = action.error.message || 'error get query';
        state.loading = false;
      })
      .addCase(getRates.fulfilled, (state, action) => {
        state.loading = false;
        state.ratesList = action.payload;
      })
      .addCase(createNewRates.pending, (state) => {
        state.loading = true;
      })
      .addCase(createNewRates.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message || 'error post query';
      })
      .addCase(createNewRates.fulfilled, (state, action) => {
        if (action.payload.rate) {
          state.ratesList.push(action.payload.rate);
          state.loading = false;
          state.message = action.payload.text;
          state.successfully = true;
        } else {
          state.hasError = true;
          state.message = action.payload.text;
          state.loading = false;
        }
      })
      .addCase(editRatesPut.pending, (state) => {
        state.loading = true;
      })
      .addCase(editRatesPut.rejected, (state, action) => {
        state.error = action.error.message || 'error edit position';
        state.loading = false;
      })
      .addCase(editRatesPut.fulfilled, (state, action) => {
        state.loading = false;
        state.message = action.payload.text;
        state.successfully = true;
      });
  },
  reducers: {
    editedRates: (state, action) => {
      const copy = state.ratesList;
      const index = state.ratesList.findIndex((element) => element._id === action.payload._id);
      copy[index] = action.payload;
      state.ratesList = copy;
    },
    resetStatus: (state) => {
      state.successfully = false;
      state.message = null;
      state.hasError = false;
    },
    catchError: (state, action) => {
      state.hasError = true;
      state.message = action.payload;
    },
  },
});

export const { editedRates, resetStatus } = RatesSlice.actions;
export default RatesSlice.reducer;
