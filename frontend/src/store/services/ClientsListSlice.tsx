import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { AxiosError } from 'axios';
import axios from '../../axiosApi';
import { IEmployee } from './EmployeesListSlice';

export interface IClient {
  _id: string;
  iin: string;
  bin: string;
  name: string;
  typeOfEconomicActivity: string;
  status: string;
  registrationDate: Date;
  expirationDate: Date;
  accompanyingEmployees: IEmployee[];
}

export interface INewClient {
  bin: string;
  iin: string;
  name: string;
  typeOfEconomicActivity: string;
  status: string;
  registrationDate: Date;
  expirationDate: Date | null;
  accompanyingEmployees: IEmployee[];
}

export type EmployeeIdType = string | undefined;

interface ClientsState {
  clients: IClient[];
  currentClient: IClient | null;
  loading: boolean;
  error: string | null;
  open: boolean;
  message: string | null;
  successfully: boolean;
  hasError: boolean;
}

type id = string | undefined;
type payload = {
  id: string | undefined;
  status: string;
};

type payloadEdit = {
  id: string | undefined;
  data: {
    iin: string | undefined;
    bin: string | undefined;
    name: string | undefined;
    typeOfEconomicActivity: string | undefined;
    status: string | undefined;
    registrationDate: Date | undefined;
    expirationDate: Date | undefined;
  };
};

type payloadAddEmployees = {
  id: string | undefined;
  data: EmployeeIdType;
};

export const getClients = createAsyncThunk('get/clients', async () => {
  const res = await axios.get('/admin/clients/');
  return res.data;
});

export const getClientsFromEmployee = createAsyncThunk('get/employeeClients', async () => {
  const res = await axios.get('/employee/clients/');
  return res.data;
});

export const getClientFilteredStatus = createAsyncThunk(
  'get/clients/status',
  async (status?: string[]) => {
    const res = await axios.get(`/admin/clients?status=${status}`);
    return res.data;
  }
);

export const getClientWithoutEmployees = createAsyncThunk(
  'get/clients/withoutAccompanyingEmployees',
  async (clients: IClient[]) => {
    const res = await axios.get(`/admin/clients?accompanyingEmployees=${clients}`);
    return res.data;
  }
);

export const getCurrentClient = createAsyncThunk('get/client', async (id: id) => {
  const res = await axios.get(`/admin/clients/${id}`);
  return res.data;
});

export const addEmployeesToCurrentClient = createAsyncThunk(
  'add/client/employees',
  async (payload: payloadAddEmployees) => {
    const res = await axios.patch(
      `/admin/clients/${payload.id}/?accompanyingEmployees=${payload.data}`,
      { accompanyingEmployee: payload.data },
      {
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
        },
      }
    );
    return res.data;
  }
);

export const editCurrentClient = createAsyncThunk(
  'put/client',
  async (payload: payloadEdit, thunkApi) => {
    try {
      const res = await axios.put(`/admin/clients/${payload.id}`, payload.data);
      return res.data;
    } catch (e) {
      const err = e as AxiosError;
      if (err.response && err.response.data) {
        thunkApi.dispatch(ClientsListSlice.actions.catchError(err.response.data));
      }
    }
  }
);

export const addClient = createAsyncThunk('add/clients', async (newData: INewClient, thunkApi) => {
  try {
    const res = await axios.post('/admin/clients/', newData);
    return res.data;
  } catch (e) {
    const err = e as AxiosError;
    if (err.response && err.response.data) {
      thunkApi.dispatch(ClientsListSlice.actions.catchError(err.response.data));
    }
  }
});

export const deleteClient = createAsyncThunk('delete/clients', async (payload: payload) => {
  const res = await axios.patch(
    `/admin/clients/${payload.id}`,
    { status: payload.status },
    {
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
    }
  );
  return res.data;
});

const initialState: ClientsState = {
  clients: [],
  currentClient: null,
  loading: false,
  error: '',
  open: false,
  message: null,
  successfully: false,
  hasError: false,
};

const ClientsListSlice = createSlice({
  name: 'clientsList',
  initialState,
  reducers: {
    setCurrentClient: (state, action: PayloadAction<IClient>) => {
      state.currentClient = action.payload;
    },
    clearClientArrayState: (state) => {
      state.clients = [];
    },
    resetStatus: (state) => {
      state.successfully = false;
      state.message = null;
    },
    catchError: (state, action) => {
      state.hasError = true;
      state.message = action.payload;
    },
  },

  extraReducers: (builder) => {
    builder
      .addCase(getClients.pending, (state) => {
        state.loading = true;
      })
      .addCase(getClients.fulfilled, (state, action) => {
        state.loading = false;
        state.clients = action.payload;
      })
      .addCase(getClientsFromEmployee.pending, (state) => {
        state.loading = true;
      })
      .addCase(getClientsFromEmployee.fulfilled, (state, action) => {
        state.loading = false;
        state.clients = action.payload;
      })
      .addCase(addClient.pending, (state) => {
        state.loading = true;
      })
      .addCase(addClient.fulfilled, (state, action) => {
        state.loading = false;
        state.successfully = true;
        state.message = action.payload.message;
        state.clients.push(action.payload.newClient);
      })
      .addCase(getCurrentClient.pending, (state) => {
        state.loading = true;
      })
      .addCase(getCurrentClient.fulfilled, (state, action) => {
        state.loading = false;
        state.currentClient = action.payload;
      })
      .addCase(deleteClient.pending, (state) => {
        state.loading = true;
      })
      .addCase(deleteClient.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message || 'Something went wrong';
      })
      .addCase(deleteClient.fulfilled, (state, action) => {
        state.loading = false;
        const index = state.clients.findIndex((id) => id._id === action.payload?._id);
        state.clients.splice(index, 1);
      })
      .addCase(editCurrentClient.pending, (state) => {
        state.loading = true;
      })
      .addCase(editCurrentClient.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message || 'Something went wrong';
      })
      .addCase(editCurrentClient.fulfilled, (state, action) => {
        state.loading = false;
        state.message = action.payload.message;
        state.successfully = true;
        state.currentClient = action.payload.client;
      })
      .addCase(getClientFilteredStatus.pending, (state) => {
        state.loading = true;
      })
      .addCase(getClientFilteredStatus.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message || 'Something went wrong';
      })
      .addCase(getClientFilteredStatus.fulfilled, (state, action) => {
        state.loading = false;
        state.clients = action.payload;
      })
      .addCase(addEmployeesToCurrentClient.pending, (state) => {
        state.loading = true;
      })
      .addCase(addEmployeesToCurrentClient.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message || 'Something went wrong';
      })
      .addCase(addEmployeesToCurrentClient.fulfilled, (state, action) => {
        state.loading = false;
        state.currentClient?.accompanyingEmployees.push(action.payload);
      })
      .addCase(getClientWithoutEmployees.pending, (state) => {
        state.loading = true;
      })
      .addCase(getClientWithoutEmployees.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message || 'Something went wrong';
      })
      .addCase(getClientWithoutEmployees.fulfilled, (state, action) => {
        state.loading = false;
        state.clients = action.payload;
      });
  },
});

export const { setCurrentClient, resetStatus, clearClientArrayState } = ClientsListSlice.actions;
export default ClientsListSlice.reducer;
