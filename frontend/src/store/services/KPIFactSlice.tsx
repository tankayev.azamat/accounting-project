import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from '../../axiosApi';

interface ModifiedIKPIFact {
  kpiFactElements: [
    {
      view: string;
      goal: string;
      description: string;
      comment: string;
      weight: number;
      factWeight: number | null;
      employeeComment?: string | null;
    }
  ];
}

export interface IKPIFact {
  _id: string;
  employeeId: string;
  year: number;
  month: string;
  status: string;
  kpiFactElements: ModifiedIKPIFact | undefined;
  createdDate: string;
  hocPayments: number;
  totalSalary: number;
}

export interface IKPIFactElements {
  view: string;
  goal: string;
  description: string;
  comment: string;
  weight: number;
  factWeight: number | null;
  employeeComment?: string | null;
}

export interface INewKPIFact {
  employeeId: string;
  year: number;
  month: string;
  kpiFactElements: IKPIFactElements[];
  hocPayments: number;
  totalSalary: number;
}

export interface IKPIFactState {
  error: string | null;
  loading: boolean;
  kpiFacts: IKPIFact[];
  years: number[];
  successfully: boolean;
  message: string | null;
  selectedYear: number | null;
  employeesKpiFacts: IKPIFact[];
  kpiFactsOfYear: IKPIFact[];
  kpiFactOfMonth: IKPIFact | null;
  currentKpiFact: IKPIFact | null | undefined;
  currentKpiFactElements: IKPIFactElements[];
}

export const initialState: IKPIFactState = {
  error: null,
  loading: false,
  kpiFacts: [],
  years: [],
  successfully: false,
  message: null,
  selectedYear: null,
  employeesKpiFacts: [],
  kpiFactsOfYear: [],
  kpiFactOfMonth: null,
  currentKpiFact: null,
  currentKpiFactElements: [],
};

export const addKpiFact = createAsyncThunk('post/kpiFact', async (payload: INewKPIFact) => {
  const res = await axios.post('/employee/kpiFact', payload);
  return res.data;
});

export const getAllYears = createAsyncThunk('get/allyears', async () => {
  const res = await axios.get(`/employee/kpiFact/allYears`);
  return res.data;
});

export const getKPIFactsOfEmployee = createAsyncThunk('get/kpiFactOfEmployee', async () => {
  const res = await axios.get(`/employee/kpiFact/employeesKpiFacts`);
  return res.data;
});

export const getFactsOfYear = createAsyncThunk('get/factsOfYear', async (year: number) => {
  const res = await axios.get(`/employee/kpiFact/${year}`);
  return res.data;
});

export const getKPIFact = createAsyncThunk('get/kpiFact', async (id: string | undefined) => {
  const res = await axios.get(`/employee/kpi/${id}`);
  return res.data;
});

const KPIFactSlice = createSlice({
  name: 'factKpi',
  initialState,
  reducers: {
    setCurrentKpi: (state, action) => {
      state.currentKpiFact = action.payload;
    },
    clearFactKpiState: (state) => {
      state.employeesKpiFacts = [];
      state.error = null;
    },
    setSelectedYearHandler: (state, action) => {
      state.selectedYear = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getAllYears.pending, (state) => {
        state.loading = true;
      })
      .addCase(getAllYears.rejected, (state) => {
        state.loading = false;
      })
      .addCase(getAllYears.fulfilled, (state, action) => {
        state.years = action.payload;
        state.loading = false;
      })
      .addCase(addKpiFact.pending, (state) => {
        state.loading = true;
      })
      .addCase(addKpiFact.rejected, (state) => {
        state.loading = false;
      })
      .addCase(addKpiFact.fulfilled, (state, action) => {
        state.kpiFacts.push(action.payload);
        state.loading = false;
      })
      .addCase(getKPIFactsOfEmployee.pending, (state) => {
        state.loading = true;
      })
      .addCase(getKPIFactsOfEmployee.fulfilled, (state, action) => {
        state.employeesKpiFacts = action.payload;
      })
      .addCase(getFactsOfYear.pending, (state) => {
        state.loading = true;
      })
      .addCase(getFactsOfYear.rejected, (state) => {
        state.loading = false;
      })
      .addCase(getFactsOfYear.fulfilled, (state, action) => {
        state.loading = false;
        state.kpiFactsOfYear = action.payload;
      })
      .addCase(getKPIFact.pending, (state) => {
        state.loading = true;
      })
      .addCase(getKPIFact.fulfilled, (state, action) => {
        state.loading = false;
        state.currentKpiFact = action.payload;
        state.currentKpiFactElements = action.payload.kpiFactElements[0].kpiFactElements;
      });
  },
});

export const { setCurrentKpi, clearFactKpiState, setSelectedYearHandler } = KPIFactSlice.actions;

export default KPIFactSlice.reducer;
