import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { AxiosError } from 'axios';
import axios from '../../axiosApi';
import { IClient } from './ClientsListSlice';
import { IPosition } from './PositionSlice';
export interface IEmployee {
  _id: string;
  firstName: string;
  lastName: string;
  position: IPosition;
  status: string;
  fixedSalary: number;
  headId?: string | null;
  dateOfDismissal?: Date | null;
  dateOfEmployment: Date;
  employeePosition?: string;
}
export interface INewEmployee {
  firstName: string;
  lastName: string;
  position: string;
  status: string;
  fixedSalary: number;
  headId?: string | null;
  dateOfDismissal?: Date | null;
  dateOfEmployment: Date;
}

interface EmployeeAndHead {
  emp: IEmployee[];
  _doc: IEmployee;
  index: number;
}
interface IEmployeesState {
  error: string | null;
  loading: boolean;
  employees: IEmployee[];
  employeesFiltered: IEmployee[];
  currentEmployee: IEmployee | null;
  open: boolean;
  employeesWorks: IEmployee[];
  addedSuccessfully: boolean;
  hasErrors: boolean;
  message: string | null;
  editedSuccessfully: boolean;
  clientsOfCurrentEmployee: IClient[];
  employeeAndHeadEmployee: EmployeeAndHead[];
  totalPaymentSum: number | null;
}

type id = string | undefined;

type payloadEdit = {
  id: string | undefined;
  data: {
    firstName: string | undefined;
    lastName: string | undefined;
    position: IPosition | undefined;
    status: string | undefined;
    fixedSalary: number | undefined;
    headId?: string | null;
    dateOfDismissal?: Date | null;
    dateOfEmployment: Date | undefined;
  };
};

export const getEmployeeMainPage = createAsyncThunk('get/employees/mainPage', async () => {
  const res = await axios.get('/main');
  return res.data;
});

export const getEmployeeMainPagePayment = createAsyncThunk(
  'get/employees/mainPage/payment',
  async () => {
    const res = await axios.get('/main?payment=payment');
    return res.data;
  }
);

export const getEmployees = createAsyncThunk('get/employees', async () => {
  const res = await axios.get('/admin/employees/');
  return res.data;
});

export const editCurrentEmployee = createAsyncThunk(
  'put/client',
  async (payload: payloadEdit, thunkApi) => {
    try {
      const res = await axios.put(`/admin/employees/${payload.id}`, payload.data);
      return res.data;
    } catch (e) {
      const err = e as AxiosError;

      if (err.response && err.response.data) {
        thunkApi.dispatch(employeesSlice.actions.catchError(err.response.data));
      } else {
        thunkApi.dispatch(employeesSlice.actions.globalError(err.message));
      }
    }
  }
);

export const getEmployeeFilteredStatus = createAsyncThunk(
  'get/employee/status',
  async (status?: string[]) => {
    const res = await axios.get(`/admin/employees?status=${status}`);
    return res.data;
  }
);

export const getEmployeesByPositionId = createAsyncThunk(
  'get/employees/position',
  async (positionId: id) => {
    const res = await axios.get(`/admin/employees?position=${positionId}`);
    return res.data;
  }
);

export const getEmployeesWorks = createAsyncThunk('get/employees/works', async () => {
  const res = await axios.get(`/admin/employees?works=works`);
  return res.data;
});

export const getClientsOfEmployee = createAsyncThunk(
  'get/clientsOfEmployee',
  async (clientsOfEmployeeId: id) => {
    const res = await axios.get(`/admin/employees?clientsOfEmployeeId=${clientsOfEmployeeId}`);
    return res.data;
  }
);

export const getEmployeesId = createAsyncThunk('get/employees/id', async (id: id) => {
  const res = await axios.get(`/admin/employees/${id}`);
  return res.data;
});

export const getEmployeeSubordinates = createAsyncThunk('get/employees/subordinates', async () => {
  const res = await axios.get(`/employee/subordinates`);
  return res.data;
});

export const addEmployee = createAsyncThunk(
  'add/employee',
  async (payload: INewEmployee, thunkApi) => {
    try {
      const res = await axios.post('/admin/employees/', payload);
      return res.data;
    } catch (e) {
      const err = e as AxiosError;
      if (err.response && err.response.data) {
        thunkApi.dispatch(employeesSlice.actions.catchError(err.response.data));
      } else {
        thunkApi.dispatch(employeesSlice.actions.globalError(err.message));
      }
    }
  }
);
type payload = {
  id: string | undefined;
  status: string;
};

export const deleteEmployee = createAsyncThunk(
  'delete/employee',
  async (payload: payload, thunkApi) => {
    try {
      const res = await axios.patch(
        `/admin/employees/${payload.id}`,
        { status: payload.status },
        {
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
          },
        }
      );
      return res.data;
    } catch (e) {
      const err = e as AxiosError;
      if (err.response && err.response.data) {
        thunkApi.dispatch(employeesSlice.actions.catchError(err.response.data));
      } else {
        thunkApi.dispatch(employeesSlice.actions.globalError(err.message));
      }
    }
  }
);

const initialState: IEmployeesState = {
  error: null,
  loading: false,
  employees: [],
  employeesFiltered: [],
  currentEmployee: null,
  open: false,
  employeesWorks: [],
  addedSuccessfully: false,
  hasErrors: false,
  message: null,
  editedSuccessfully: false,
  clientsOfCurrentEmployee: [],
  employeeAndHeadEmployee: [],
  totalPaymentSum: null,
};

const employeesSlice = createSlice({
  name: 'employees',
  initialState,
  reducers: {
    setCurrentEmployee: (state, action: PayloadAction<IEmployee>) => {
      state.currentEmployee = action.payload;
    },
    catchError: (state, action) => {
      state.hasErrors = true;
      state.message = action.payload?.message;
    },
    globalError: (state, action) => {
      state.hasErrors = true;
      state.message = action.payload;
    },
    resetNotificationStatus: (state) => {
      state.addedSuccessfully = false;
      state.editedSuccessfully = false;
      state.message = null;
      state.hasErrors = false;
    },
    clearEmplyeesArrayState: (state) => {
      state.employees = [];
    },
    clearMainPageArrayState: (state) => {
      state.employeeAndHeadEmployee = [];
      state.totalPaymentSum = null;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getEmployees.pending, (state) => {
        state.loading = true;
      })
      .addCase(getEmployees.fulfilled, (state, action) => {
        state.loading = false;
        state.employees = action.payload;
      })
      .addCase(getEmployees.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message || 'Something went wrong';
      })
      .addCase(addEmployee.pending, (state) => {
        state.loading = true;
      })
      .addCase(addEmployee.fulfilled, (state, action) => {
        state.employees.push(action.payload.newEmployee);
        state.addedSuccessfully = true;
        state.message = action.payload.message;
        state.loading = false;
      })
      .addCase(addEmployee.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message || 'Что-то пошло не так';
      })
      .addCase(getEmployeesId.pending, (state) => {
        state.loading = true;
      })
      .addCase(getEmployeesId.rejected, (state, action) => {
        state.error = action.error.message || 'Something went wrong';
        state.loading = false;
      })
      .addCase(getEmployeesId.fulfilled, (state, action) => {
        state.loading = false;
        state.currentEmployee = action.payload;
      })
      .addCase(getEmployeesByPositionId.pending, (state) => {
        state.loading = true;
      })
      .addCase(getEmployeesByPositionId.rejected, (state, action) => {
        state.error = action.error.message || 'Something went wrong';
        state.loading = false;
      })
      .addCase(getEmployeesByPositionId.fulfilled, (state, action) => {
        state.loading = false;
        state.employeesFiltered = action.payload;
      })
      .addCase(deleteEmployee.pending, (state) => {
        state.loading = true;
      })
      .addCase(deleteEmployee.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message || 'Something went wrong';
      })
      .addCase(deleteEmployee.fulfilled, (state, action) => {
        state.loading = false;
        const index = state.employees.findIndex((id) => id._id === action.payload?.clients._id);
        state.addedSuccessfully = true;
        state.message = action.payload.message;
        state.employees.splice(index, 1);
      })
      .addCase(getEmployeeFilteredStatus.pending, (state) => {
        state.loading = true;
      })
      .addCase(getEmployeeFilteredStatus.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message || 'Something went wrong';
      })
      .addCase(getEmployeeFilteredStatus.fulfilled, (state, action) => {
        state.loading = false;
        state.employees = action.payload;
      })
      .addCase(editCurrentEmployee.pending, (state) => {
        state.loading = true;
      })
      .addCase(editCurrentEmployee.rejected, (state) => {
        state.loading = false;
      })
      .addCase(editCurrentEmployee.fulfilled, (state, action) => {
        state.loading = false;
        state.currentEmployee = action.payload.employees;
        state.editedSuccessfully = true;
        if (action.payload.message) {
          state.message = action.payload?.message;
        }
      })
      .addCase(getEmployeesWorks.pending, (state) => {
        state.loading = true;
      })
      .addCase(getEmployeesWorks.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message || 'error get works params';
      })
      .addCase(getEmployeesWorks.fulfilled, (state, action) => {
        state.loading = false;
        state.employeesWorks = action.payload;
      })
      .addCase(getClientsOfEmployee.pending, (state) => {
        state.loading = true;
      })
      .addCase(getClientsOfEmployee.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message || 'Something went wrong';
      })
      .addCase(getClientsOfEmployee.fulfilled, (state, action) => {
        state.loading = false;
        state.clientsOfCurrentEmployee = action.payload;
      })
      .addCase(getEmployeeMainPage.pending, (state) => {
        state.loading = true;
      })
      .addCase(getEmployeeMainPage.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message || 'Something went wrong';
      })
      .addCase(getEmployeeMainPage.fulfilled, (state, action) => {
        state.loading = false;
        state.employeeAndHeadEmployee = action.payload;
      })
      .addCase(getEmployeeMainPagePayment.pending, (state) => {
        state.loading = true;
      })
      .addCase(getEmployeeMainPagePayment.rejected, (state, action) => {
        state.error = action.error.message || 'Something went wrong';
        state.loading = false;
      })
      .addCase(getEmployeeMainPagePayment.fulfilled, (state, action) => {
        state.totalPaymentSum = action.payload.totalPayment;
        state.loading = false;
      })
      .addCase(getEmployeeSubordinates.pending, (state) => {
        state.loading = true;
      })
      .addCase(getEmployeeSubordinates.rejected, (state, action) => {
        state.error = action.error.message || 'Something went wrong';
        state.loading = false;
      })
      .addCase(getEmployeeSubordinates.fulfilled, (state, action) => {
        state.employees = action.payload;
        state.loading = false;
      });
  },
});

export const {
  setCurrentEmployee,
  resetNotificationStatus,
  clearEmplyeesArrayState,
  clearMainPageArrayState,
} = employeesSlice.actions;
export default employeesSlice.reducer;
