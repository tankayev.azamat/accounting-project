import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { AxiosError } from 'axios';
import axios from '../../axiosApi';

export interface IRatesEmployee {
  _id: string;
  position: string;
  percent: number;
}
export interface INewRatesEmployee {
  position: string;
  percent: number;
}
interface ServiceRatesState {
  ratesEmployeeList: IRatesEmployee[];
  loading: boolean;
  error: string | null;
  message: string | null;
  successfully: boolean;
  hasError: boolean;
  open: boolean;
}

export const getRatesEmployee = createAsyncThunk('get/rates/employee', async () => {
  const res = await axios.get('/admin/ratesEmployee/');
  return res.data;
});

export const createNewRatesEmployee = createAsyncThunk(
  'create/rates/employee',
  async (newData: INewRatesEmployee, thunkApi) => {
    try {
      const res = await axios.post('/admin/ratesEmployee', newData);
      return res.data;
    } catch (e) {
      const err = e as AxiosError;
      if (err.response && err.response.data) {
        thunkApi.dispatch(RatesEmployeeSlice.actions.catchError(err.response.data));
      }
    }
  }
);

export const editRatesEmployeePut = createAsyncThunk(
  'edit/positions/employee',
  async (directory: IRatesEmployee, thunkApi) => {
    try {
      const res = await axios.put(`/admin/ratesEmployee/${directory._id}`, directory);
      return res.data;
    } catch (e) {
      const err = e as AxiosError;
      if (err.response && err.response.data) {
        thunkApi.dispatch(RatesEmployeeSlice.actions.catchError(err.response.data));
      }
    }
  }
);

const initialState: ServiceRatesState = {
  ratesEmployeeList: [],
  loading: false,
  error: '',
  message: null,
  successfully: false,
  hasError: false,
  open: false,
};

const RatesEmployeeSlice = createSlice({
  name: 'rates',
  initialState,
  extraReducers: (builder) => {
    builder
      .addCase(getRatesEmployee.pending, (state) => {
        state.loading = true;
      })
      .addCase(getRatesEmployee.rejected, (state, action) => {
        state.error = action.error.message || 'error get query';
        state.loading = false;
      })
      .addCase(getRatesEmployee.fulfilled, (state, action) => {
        state.loading = false;
        state.ratesEmployeeList = action.payload;
      })
      .addCase(createNewRatesEmployee.pending, (state) => {
        state.loading = true;
      })
      .addCase(createNewRatesEmployee.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message || 'error post query';
      })
      .addCase(createNewRatesEmployee.fulfilled, (state, action) => {
        if (action.payload.rate) {
          state.ratesEmployeeList.push(action.payload.rate);
          state.loading = false;
          state.message = action.payload.message;
          state.successfully = true;
        } else {
          state.message = action.payload.message;
          state.hasError = true;
          state.loading = false;
        }
      })
      .addCase(editRatesEmployeePut.pending, (state) => {
        state.loading = true;
      })
      .addCase(editRatesEmployeePut.rejected, (state, action) => {
        state.error = action.error.message || 'error edit position';
        state.loading = false;
      })
      .addCase(editRatesEmployeePut.fulfilled, (state, action) => {
        state.loading = false;
        state.message = action.payload.message;
        state.successfully = true;
      });
  },
  reducers: {
    editedRates: (state, action) => {
      const copy = state.ratesEmployeeList;
      const index = state.ratesEmployeeList.findIndex(
        (element) => element._id === action.payload._id
      );
      copy[index] = action.payload;
      state.ratesEmployeeList = copy;
    },
    resetStatus: (state) => {
      state.successfully = false;
      state.message = null;
      state.hasError = false;
    },
    catchError: (state, action) => {
      state.hasError = true;
      state.message = action.payload;
    },
  },
});

export const { editedRates, resetStatus } = RatesEmployeeSlice.actions;
export default RatesEmployeeSlice.reducer;
