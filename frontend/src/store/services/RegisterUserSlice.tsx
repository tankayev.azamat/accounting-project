import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { AxiosError } from 'axios';
import axios from '../../axiosApi';

interface newAccount {
  login: string;
  password: string;
  employeeId: string | undefined;
  positionId: string | undefined;
}

interface IUser {
  _id: string;
  login: string;
  password: string;
  roleId: string;
  employeeId?: string;
  positionId?: string;
}

interface IUserState {
  message: string | null;
  successfully: boolean;
  hasError: boolean;
  loadingReg: boolean;
  userData: IUser | null;
}

const initialState: IUserState = {
  message: null,
  successfully: false,
  hasError: false,
  loadingReg: false,
  userData: null,
};

export const registerUser = createAsyncThunk(
  'user/register',
  async (newAccount: newAccount, thunkApi) => {
    try {
      const res = await axios.post('/users/register', newAccount);
      return res.data;
    } catch (e) {
      const err = e as AxiosError;
      if (err.response?.data) {
        thunkApi.dispatch(UsersRegisterSlice.actions.catchError(err.response.data));
      }
    }
  }
);

interface EditAccount {
  _id: string;
  password: string;
}

export const correctPassword = createAsyncThunk(
  'user/password/edit',
  async (EditAccount: EditAccount, thunkApi) => {
    try {
      const res = await axios.patch(`/users/password/${EditAccount._id}`, {
        password: EditAccount.password,
      });
      return res.data;
    } catch (e) {
      const err = e as AxiosError;
      if (err.response?.data) {
        thunkApi.dispatch(UsersRegisterSlice.actions.catchError(err.response.data));
      }
    }
  }
);

interface payload {
  id: string | undefined;
}

export const getUserEmployeeId = createAsyncThunk('get/user/employee', async (payload: payload) => {
  const res = await axios.get(`/users/${payload.id}`);
  return res.data;
});

const UsersRegisterSlice = createSlice({
  name: 'users/register',
  initialState,
  reducers: {
    catchError: (state, action) => {
      state.hasError = true;
      state.message = action.payload;
    },
    resetMessage: (state) => {
      state.message = null;
      state.hasError = false;
      state.successfully = false;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(registerUser.pending, (state) => {
        state.loadingReg = true;
      })
      .addCase(registerUser.fulfilled, (state, action) => {
        state.loadingReg = false;
        state.successfully = true;
        state.message = action.payload.message;
        state.userData = action.payload.user;
      })
      .addCase(getUserEmployeeId.pending, (state) => {
        state.loadingReg = true;
      })
      .addCase(getUserEmployeeId.fulfilled, (state, action) => {
        state.loadingReg = false;
        state.userData = action.payload;
      })
      .addCase(correctPassword.pending, (state) => {
        state.loadingReg = true;
      })
      .addCase(correctPassword.fulfilled, (state, action) => {
        state.loadingReg = false;
        state.successfully = true;
        state.message = action.payload.message;
        state.userData = action.payload.changeUserPass;
      });
  },
});

export const { resetMessage, catchError } = UsersRegisterSlice.actions;
export default UsersRegisterSlice.reducer;
