import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { AxiosError } from 'axios';
import axios from '../../axiosApi';

export interface IKpiKeyIndicator {
  _id: string;
  title: string;
}

interface IKpiKeyIndicatorState {
  hasError: boolean;
  loading: boolean;
  kpiKeyIndicatorElements: IKpiKeyIndicator[];
  addedSuccessfully: boolean;
  message: string | null;
}
interface INewKpi {
  title: string;
}

type id = string | undefined;

export const addKpiKeyIndicator = createAsyncThunk(
  'post/kpiKeyIndicator',
  async (kpiKeyIndicatorData: INewKpi, thunkApi) => {
    try {
      const res = await axios.post('/admin/kpiKeyIndicator/', kpiKeyIndicatorData);
      return res.data;
    } catch (e) {
      const err = e as AxiosError;
      if (err.response && err.response.data) {
        thunkApi.dispatch(KpiKeyIndicatorSlice.actions.catchError(err.response.data));
      }
    }
  }
);

export const getKpiKeyIndicatorElements = createAsyncThunk('get/kpiKeyindicator', async () => {
  const res = await axios.get(`/admin/kpiKeyIndicator/`);
  return res.data;
});

export const getKpiKeyIndicatorById = createAsyncThunk('get/kpiKeyindicator/id', async (id: id) => {
  const res = await axios.get(`/admin/kpiKeyIndicator/${id}`);
  return res.data;
});

const initialState: IKpiKeyIndicatorState = {
  hasError: false,
  loading: false,
  kpiKeyIndicatorElements: [],
  addedSuccessfully: false,
  message: null,
};

const KpiKeyIndicatorSlice = createSlice({
  name: 'kpiKeyIndicator',
  initialState,
  reducers: {
    resetKpiKeyIndicatorElements: (state) => {
      state.kpiKeyIndicatorElements = [];
    },
    resetAddedStatus: (state) => {
      state.addedSuccessfully = false;
      state.hasError = false;
      state.message = null;
    },
    catchError: (state, action) => {
      state.hasError = true;
      state.message = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(addKpiKeyIndicator.pending, (state) => {
        state.loading = true;
      })
      .addCase(addKpiKeyIndicator.fulfilled, (state, action) => {
        state.addedSuccessfully = true;
        const copy = state.kpiKeyIndicatorElements;
        copy.push(action.payload.element);
        state.kpiKeyIndicatorElements = copy;
        state.message = action.payload.message;
        state.loading = false;
      })
      .addCase(getKpiKeyIndicatorById.pending, (state) => {
        state.loading = true;
      })
      .addCase(getKpiKeyIndicatorById.fulfilled, (state, action) => {
        state.kpiKeyIndicatorElements = action.payload;
        state.loading = false;
      })
      .addCase(getKpiKeyIndicatorElements.pending, (state) => {
        state.loading = true;
      })
      .addCase(getKpiKeyIndicatorElements.fulfilled, (state, action) => {
        state.kpiKeyIndicatorElements = action.payload;
        state.loading = false;
      });
  },
});
export const { resetKpiKeyIndicatorElements, resetAddedStatus, catchError } =
  KpiKeyIndicatorSlice.actions;
export default KpiKeyIndicatorSlice.reducer;
