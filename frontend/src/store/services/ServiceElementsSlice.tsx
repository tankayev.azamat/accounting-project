import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { AxiosError } from 'axios';
import axios from '../../axiosApi';

export interface IServiceElements {
  _id: string;
  title: string;
}
export interface INewServiceElement {
  title: string;
}
interface ServiceElementsState {
  serviceElementsList: IServiceElements[];
  currentServiceElement: IServiceElements[];
  loading: boolean;
  error: string | null;
  successfully: boolean;
  open: boolean;
  message: string | null;
  hasError: boolean;
}

export const getServiceElementsList = createAsyncThunk('get/serviceElements', async () => {
  const res = await axios.get('/admin/serviceElements/');
  return res.data;
});

export const addNewServiceElement = createAsyncThunk(
  'add/serviceElement',
  async (newData: INewServiceElement, thunkApi) => {
    try {
      const res = await axios.post('/admin/serviceElements/', newData);
      return res.data;
    } catch (e) {
      const err = e as AxiosError;
      if (err.response && err.response.data) {
        thunkApi.dispatch(ServiceElementsSlice.actions.catchError(err.response.data));
      }
    }
  }
);

export const editFetchCurrentServiceElement = createAsyncThunk(
  'put/currentServiceElement',
  async (payload: IServiceElements, thunkApi) => {
    try {
      const res = await axios.put(`/admin/serviceElements/${payload._id}`, payload);
      return res.data;
    } catch (e) {
      const err = e as AxiosError;
      if (err.response && err.response.data) {
        thunkApi.dispatch(ServiceElementsSlice.actions.catchError(err.response.data));
      }
    }
  }
);

const initialState: ServiceElementsState = {
  serviceElementsList: [],
  currentServiceElement: [],
  loading: false,
  error: '',
  successfully: false,
  open: false,
  message: null,
  hasError: false,
};

const ServiceElementsSlice = createSlice({
  name: 'serviceElementsList',
  initialState,
  reducers: {
    resetStatus: (state) => {
      state.successfully = false;
      state.message = null;
    },
    editedElement: (state, action) => {
      const copy = state.serviceElementsList;
      const index = state.serviceElementsList.findIndex(
        (element) => element._id === action.payload._id
      );
      copy[index] = action.payload;
      state.serviceElementsList = copy;
    },
    catchError: (state, action) => {
      state.hasError = true;
      state.message = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getServiceElementsList.pending, (state) => {
        state.loading = true;
      })
      .addCase(getServiceElementsList.fulfilled, (state, action) => {
        state.loading = false;
        state.serviceElementsList = action.payload;
      })
      .addCase(addNewServiceElement.pending, (state) => {
        state.loading = true;
      })
      .addCase(addNewServiceElement.fulfilled, (state, action) => {
        state.successfully = true;
        state.loading = false;
        state.message = action.payload.text;
        state.serviceElementsList.push(action.payload.element);
      })
      .addCase(editFetchCurrentServiceElement.pending, (state) => {
        state.loading = true;
      })
      .addCase(editFetchCurrentServiceElement.fulfilled, (state, action) => {
        state.loading = false;
        state.successfully = true;
        state.message = action.payload.text;
      });
  },
});
export const { resetStatus, editedElement } = ServiceElementsSlice.actions;
export default ServiceElementsSlice.reducer;
