import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { AxiosError } from 'axios';
import axios from '../../axiosApi';
import { checkMonth } from '../../components/helpers/helpers';
import { IClient } from './ClientsListSlice';
interface IServiceForm {
  _id: string;
  title: string;
  quantity: number;
  factQuantity: number;
  price: number;
  priceForEmployee: number;
  sum: number;
}

export interface ActiveClientsFactOfService {
  _id: string;
  startDate: Date;
  totalSum: number;
  clientId: IClient;
  factTotalSum: number;
  difference: number;
  serviceElements: IServiceForm[];
  status: string;
}

export interface FactForMonth {
  _id: string;
  year: Date;
  month: number;
  activeClientsFactOfService: Array<ActiveClientsFactOfService>;
  status: string;
  monthfactTotalSum: number;
  monthTotalServicePaymentsFee: number;
  difference: number;
}
interface IFactForMonthState {
  error: string | null;
  hasErrors: boolean;
  loading: boolean;
  factForMonths: FactForMonth[];
  currentClient: ActiveClientsFactOfService | null;
  currentMonth: FactForMonth | null;
  years: number[];
  selectedYear: number | null;
  monthClosed: boolean;
  deleted: boolean;
  edited: boolean;
  added: boolean;
  successfully: boolean;
  message: string | null;
}

export interface INewFact {
  year: number;
  month: number;
}

export const getFactForMonth = createAsyncThunk(
  'get/monthWithFacts',
  async (year: number | undefined, thunkApi) => {
    try {
      const res = await axios.get(`/admin/factForMonth/${year}`);
      return res.data;
    } catch (e) {
      const err = e as AxiosError;

      if (err.response && err.response.data) {
        thunkApi.dispatch(FactForMonth.actions.catchError(err.response.data));
      } else {
        thunkApi.dispatch(FactForMonth.actions.globalError(err.message));
      }
    }
  }
);

export const setNewFactForMonth = createAsyncThunk(
  'set/factToClient',
  async (payload: INewFact, thunkApi) => {
    try {
      const res = await axios.post(`/admin/factForMonth/`, payload);
      return res.data;
    } catch (e) {
      const err = e as AxiosError;

      if (err?.response && err.response?.data) {
        thunkApi.dispatch(FactForMonth.actions.catchError(err.response?.data));
      } else {
        thunkApi.dispatch(FactForMonth.actions.globalError(err?.message));
      }
    }
  }
);
type EditingFact = {
  id: string | undefined;
  data: any;
};
export const editFactForMonth = createAsyncThunk(
  'edit/factForMonth',
  async (payload: EditingFact, thunkApi) => {
    try {
      const res = await axios.put(`/admin/factForMonth/currentMonth/${payload.id}`, payload.data);
      return res.data;
    } catch (e) {
      const err = e as AxiosError;

      if (err?.response && err.response?.data) {
        thunkApi.dispatch(FactForMonth.actions.catchError(err.response?.data));
      } else {
        thunkApi.dispatch(FactForMonth.actions.globalError(err?.message));
      }
    }
  }
);
export const getAllYears = createAsyncThunk('get/allyears', async () => {
  const res = await axios.get(`/admin/factForMonth/AllYears`);
  return res.data;
});
export const getCurrentMonth = createAsyncThunk(
  'get/CurrentMonth',
  async (id: string | undefined) => {
    const res = await axios.get(`/admin/factForMonth/currentMonth/${id}`);
    return res.data;
  }
);
export const deleteCurrentMonth = createAsyncThunk(
  'delete/CurrentMonth',
  async (id: string | undefined) => {
    const res = await axios.delete(`/admin/factForMonth/currentMonth/${id}`);
    return res.data;
  }
);
export const closePeriodOfCurrentMonth = createAsyncThunk(
  'put/closePeriodOfCurrentMonth',
  async (id: string | undefined) => {
    const res = await axios.patch(`/admin/factForMonth/currentMonth/${id}`);
    return res.data;
  }
);
const initialState: IFactForMonthState = {
  error: null,
  hasErrors: false,
  loading: false,
  factForMonths: [],
  years: [],
  currentClient: null,
  selectedYear: null,
  currentMonth: null,
  successfully: false,
  deleted: false,
  edited: false,
  added: false,
  monthClosed: false,
  message: null,
};

const FactForMonth = createSlice({
  name: 'factOfService',
  initialState,
  reducers: {
    resetNotification: (state) => {
      state.hasErrors = false;
      state.successfully = false;
      state.message = null;
      state.edited = false;
      state.deleted = false;
      state.added = false;
      state.monthClosed = false;
      state.loading = false;
    },
    catchError: (state, action) => {
      state.hasErrors = true;
      state.message = action.payload.message;
    },
    globalError: (state, action) => {
      state.hasErrors = true;
      state.message = action.payload;
    },
    setCurrentMonth: (state, action) => {
      state.currentMonth = action.payload;
    },
    setCurrentClient: (state, action) => {
      state.currentClient = action.payload;
    },
    resetCurrentClient: (state) => {
      state.currentClient = null;
    },
    resetSelecterYear: (state) => {
      state.selectedYear = null;
    },
    resetCurrentMonth: (state) => {
      state.currentMonth = null;
    },
    resetCurrentYearFacts: (state) => {
      state.factForMonths = [];
    },

    setSelectedYear: (state, action) => {
      state.selectedYear = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getFactForMonth.pending, (state) => {
        state.loading = true;
      })
      .addCase(getFactForMonth.fulfilled, (state, action) => {
        const modified = action?.payload.map((item: FactForMonth) => {
          return { ...item, month: checkMonth(item.month) };
        });
        state.factForMonths = modified;
        state.loading = false;
      })
      .addCase(setNewFactForMonth.pending, (state) => {
        state.loading = true;
      })
      .addCase(setNewFactForMonth.fulfilled, (state, action) => {
        if (action.payload !== undefined) {
          state.loading = false;
          state.successfully = true;
          state.message = action.payload?.message;
          state.added = true;
        }
      })
      .addCase(getCurrentMonth.pending, (state) => {
        state.loading = true;
      })
      .addCase(getCurrentMonth.fulfilled, (state, action) => {
        state.loading = false;
        state.currentMonth = action.payload[0];
      })
      .addCase(editFactForMonth.pending, (state) => {
        state.loading = true;
      })
      .addCase(editFactForMonth.fulfilled, (state, action) => {
        state.loading = false;
        state.currentMonth = action.payload?.updated;
        state.successfully = true;
        state.message = action.payload?.message;
        state.edited = true;
      })
      .addCase(deleteCurrentMonth.pending, (state) => {
        state.loading = true;
      })
      .addCase(deleteCurrentMonth.fulfilled, (state, action) => {
        state.loading = false;
        state.successfully = true;
        state.message = action.payload?.message;
        state.deleted = true;
      })
      .addCase(closePeriodOfCurrentMonth.pending, (state) => {
        state.loading = true;
      })
      .addCase(closePeriodOfCurrentMonth.fulfilled, (state, action) => {
        state.loading = false;
        state.successfully = true;
        state.monthClosed = true;
        state.message = action.payload?.message;
      })
      .addCase(getAllYears.pending, (state) => {
        state.loading = true;
      })
      .addCase(getAllYears.fulfilled, (state, action) => {
        state.loading = false;
        state.years = action.payload;
      });
  },
});
export const {
  resetNotification,
  catchError,
  globalError,
  setCurrentMonth,
  setCurrentClient,
  resetCurrentClient,
  resetCurrentYearFacts,
  resetCurrentMonth,
  resetSelecterYear,
  setSelectedYear,
} = FactForMonth.actions;
export default FactForMonth.reducer;
