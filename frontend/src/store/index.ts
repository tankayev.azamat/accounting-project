import { configureStore, PreloadedState, StateFromReducersMapObject } from '@reduxjs/toolkit';
import { AxiosRequestConfig } from 'axios';
import axios from 'axiosApi';
import ClientsListReducer from '../store/services/ClientsListSlice';
import { userInfo } from '../store/services/UsersSlice';
import ClientAddHocServiceSlice from './services/ClientAddHocServiceSlice';
import EmployeesListSlice from './services/EmployeesListSlice';
import FactForMonthReducer from './services/FactForMonthSlice';
import KPIFactReducer from './services/KPIFactSlice';
import KpiHistoriesSlice from './services/KpiHistoriesSlice';
import KpiKeyIndicatorsReducer from './services/KpiKeyIndicatorsSlice';
import KpiReducer from './services/KpiSlice';
import PositionSlice from './services/PositionSlice';
import RatesEmployeeSlice from './services/RatesEmployeeSlice';
import RatesSlice from './services/RatesSlice';
import RegisterUserSlice from './services/RegisterUserSlice';
import ServiceElementsReducer from './services/ServiceElementsSlice';
import ServicePaymentHistorySlice from './services/ServicePaymentHistorySlice';
import ServicePaymentSlice from './services/ServicePaymentSlice';
import UsersSlice from './services/UsersSlice';

const loadFromLocalStorage = (): userInfo | null => {
  if (localStorage.getItem('token')) {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const userInfo = JSON.parse(localStorage.getItem('token')!);
    return userInfo;
  }
  return null;
};

axios.interceptors.request.use((config: AxiosRequestConfig) => {
  const token: userInfo | null | undefined = initStore().getState().users.user;
  if (!token) return config;
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  config.headers!['Authorization'] = token.tokenData.token;
  return config;
});

const reducer = {
  clients: ClientsListReducer,
  employees: EmployeesListSlice,
  positions: PositionSlice,
  serviceElements: ServiceElementsReducer,
  servicePayment: ServicePaymentSlice,
  servicePaymentHistory: ServicePaymentHistorySlice,
  users: UsersSlice,
  rates: RatesSlice,
  kpi: KpiReducer,
  hocService: ClientAddHocServiceSlice,
  kpiIndicatorKeys: KpiKeyIndicatorsReducer,
  kpiHistories: KpiHistoriesSlice,
  factForMonth: FactForMonthReducer,
  userRegister: RegisterUserSlice,
  ratesEmployee: RatesEmployeeSlice,
  kpiFact: KPIFactReducer,
};

export function initStore(preloadedState?: PreloadedState<RootState>) {
  return configureStore({
    reducer,
    preloadedState: {
      ...preloadedState,
      users: { user: loadFromLocalStorage() },
    },
  });
}

export type RootState = StateFromReducersMapObject<typeof reducer>;
export type Store = ReturnType<typeof initStore>;
export type AppDispatch = Store['dispatch'];
