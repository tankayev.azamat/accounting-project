import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { NavBar } from './components/NavBar/NavBar';
import { PrivateRoute } from './components/PrivateRoute/PrivateRoute';
import { JobDirectory } from './containers/Catalogs/JobDirectory/JobDirectory';
import { KPICatalogCreate } from './containers/Catalogs/KPICatalogCreate/KPICatalogCreate';
import { KpiKeyIndicators } from './containers/Catalogs/KpiKeyIndicator/KpiKeyIndicators';
import { Rates } from './containers/Catalogs/Rates/Rates';
import { ServiceElements } from './containers/Catalogs/ServiceElements/ServiceElements';
import { ClientsList } from './containers/Client/ClientsList/ClientsList';
import { CurrentClient } from './containers/Client/CurrentClient/CurrentClient';
import { CurrentEmployee } from './containers/Employee/CurrentEmployee/CurrentEmployee';
import { EmployeesList } from './containers/Employee/EmployeesList/EmployeesList';
import { AddFactOfService } from './containers/FactOfService/AddFactOfService/AddFactOfService';
import { FactOfService } from './containers/FactOfService/FactOfService';
import { HomePage } from './containers/HomePage/HomePage';
import { KPIAgreements } from './containers/KPIAgreements/KPIAgreements';
import { MonthlyKPIAgreement } from './containers/KPIAgreements/MonthlyKPIAgreement/MonthlyKPIAgreement';
import { CurrenKPIFact } from './containers/KPIForEmployees/CcurrentKPIFact/CurrentKPIFact';
import { KPIForEmployee } from './containers/KPIForEmployees/KPIForEmployee';
import { LoginUser } from './containers/LoginUser/LoginUser';

const App = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/loginUser" element={<LoginUser />} />
        <Route path="/" element={<PrivateRoute component={NavBar} />}>
          <Route index element={<PrivateRoute component={HomePage} />} />
          <Route path="/clients_list" element={<ClientsList />} />
          <Route path="/clients_list/:id" element={<CurrentClient />} />

          <Route
            path="/employees_list"
            element={<PrivateRoute isMatch={true} component={EmployeesList} />}
          />
          <Route
            path="/employees_list/:id"
            element={<PrivateRoute isMatch={true} component={CurrentEmployee} />}
          />
          <Route
            path="/kpi_catalog"
            element={<PrivateRoute isMatch={true} component={KPICatalogCreate} />}
          />
          <Route path="/kpi_employee" element={<KPIForEmployee />} />
          <Route path="/kpi_employee/kpi/:id" element={<CurrenKPIFact />} />
          <Route
            path="/KpiKeyIndicator"
            element={<PrivateRoute isMatch={true} component={KpiKeyIndicators} />}
          />
          <Route
            path="/fact_of_service"
            element={<PrivateRoute isMatch={true} component={FactOfService} />}
          />
          <Route
            path="/fact_of_service/:id"
            element={<PrivateRoute isMatch={true} component={AddFactOfService} />}
          />
          <Route
            path="/service_elements"
            element={<PrivateRoute isMatch={true} component={ServiceElements} />}
          />
          <Route path="/rates" element={<PrivateRoute isMatch={true} component={Rates} />} />
          <Route
            path="/job_directory"
            element={<PrivateRoute isMatch={true} component={JobDirectory} />}
          />
          <Route
            path="/kpi_agreements"
            element={<PrivateRoute isMatch={true} component={KPIAgreements} />}
          />
          <Route
            path="/kpi_agreements/:id"
            element={<PrivateRoute isMatch={true} component={MonthlyKPIAgreement} />}
          />
        </Route>
      </Routes>
    </BrowserRouter>
  );
};

export default App;
