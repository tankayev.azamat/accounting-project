/* eslint-disable prettier/prettier */
const path = require('path');

module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint', 'prettier', 'simple-import-sort', 'deprecation'],
  extends: ['plugin:@typescript-eslint/recommended', 'plugin:prettier/recommended'],
  parserOptions: {
    sourceType: 'module',
    useJSXTextNode: true,
    project: [path.resolve(__dirname, 'tsconfig.eslint.json')],
  },
  rules: {
    'no-underscore-dangle': 0,
    'arrow-body-style': 0,
    'no-unused-expressions': 0,
    'no-plusplus': 0,
    'no-console': 'warn',
    'func-names': 0,
    'comma-dangle': [
      'error',
      {
        arrays: 'always-multiline',
        objects: 'always-multiline',
        imports: 'always-multiline',
        exports: 'always-multiline',
        functions: 'ignore',
      },
    ],
    'no-prototype-builtins': 0,
    'prefer-destructuring': 0,
    'no-else-return': 0,
    'lines-between-class-members': ['error', 'always', { exceptAfterSingleLine: true }],
    '@typescript-eslint/explicit-member-accessibility': 0,
    '@typescript-eslint/no-explicit-any': 0,
    '@typescript-eslint/explicit-function-return-type': 0,
    '@typescript-eslint/no-unused-vars': [
      'error',
      {
        vars: 'all',
        args: 'after-used',
        ignoreRestSiblings: true,
        caughtErrors: 'none',
        argsIgnorePattern: '^(_|doc$|req$|res$|next$|props$|params$|opts$|e$)',
      },
    ],
    '@typescript-eslint/no-use-before-define': 0,
    '@typescript-eslint/no-empty-interface': 0,
    '@typescript-eslint/camelcase': 0,
    '@typescript-eslint/no-empty-function': 0,
    '@typescript-eslint/no-var-requires': 0,
    'simple-import-sort/imports': [
      'warn',
      {
        groups: [
          [
            // Packages. `react` related packages come first.
            '^react',
            '^antd($|/)',
            // Regular Packages. Things that start with a letter (or digit or underscore), or `@` followed by a letter.
            '^@?\\w',
            // Side effect imports.
            '^\\u0000',
            // Alias imports
            '^components/',
            '^models/',
            // Parent imports. Put `..` last.
            '^\\.\\.(?!/?$)',
            '^\\.\\./?$',
            // Other relative imports. Put same-folder imports and `.` last.
            '^\\./(?=.*/)(?!/?$)',
            '^\\.(?!/?$)',
            '^\\./?$',
          ],
          // Another group
          // Style imports.
          ['^.+\\.s?css$', '^.+\\.less$'],
        ],
      },
    ],
    'simple-import-sort/exports': 0,
    'deprecation/deprecation': 'warn',
  },
};
