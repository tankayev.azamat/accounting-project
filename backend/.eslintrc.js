/* eslint-disable prettier/prettier */
const path = require('path');

module.exports = {
  extends: [path.resolve(__dirname, '../.eslintrc.js')],
  plugins: ['formatjs'],
  parserOptions: {
    sourceType: 'module',
    useJSXTextNode: true,
    project: [
      path.resolve(__dirname, 'tsconfig.json'),
      path.resolve(__dirname, 'tsconfig.eslint.json'),
    ],
    ecmaVersion: 2018,
    ecmaFeatures: {
      jsx: true,
    },
  },
  settings: {
    react: {
      version: 'detect',
    },
  },
  rules: {
    '@typescript-eslint/explicit-module-boundary-types': 'off',
    'formatjs/enforce-plural-rules': [
      'error',
      {
        one: true,
        other: true,
        zero: false,
      },
    ],
  },
};
