import { ClientModel } from '../Client';

export interface CreateClientParams {
  iin?: string;
  bin?: string;
  typeOfEconomicActivity: string;
  status: string;
  registrationDate: Date;
  expirationDate: Date | null;
  name: string;
  accompanyingEmployees?: string[];
}

export async function createClient(params: CreateClientParams) {
  let obj = {};
  const {
    iin,
    bin,
    typeOfEconomicActivity,
    status,
    registrationDate,
    expirationDate,
    name,
    accompanyingEmployees,
  } = params;
  if (!typeOfEconomicActivity) {
    throw new Error('Вид деятельности должна быть заполнено.');
  }
  if (!status) {
    throw new Error('Статус должна быть заполнено.');
  }
  if (!name) {
    throw new Error('Наименование клиента должна быть заполнено.');
  }
  if (!bin && !iin) {
    throw new Error('No data on IIN and BIN');
  } else if (!bin) {
    obj = {
      iin,
      typeOfEconomicActivity,
      status,
      registrationDate,
      expirationDate,
      name,
      accompanyingEmployees,
    };
  } else if (!iin) {
    obj = {
      bin,
      typeOfEconomicActivity,
      status,
      registrationDate,
      expirationDate,
      name,
      accompanyingEmployees,
    };
  }

  if (!/^[0-9]{12}$/.test(`${iin || bin}` || '')) {
    throw new Error('IIN or BIN is not correct');
  }

  const newClient = await ClientModel.create(obj);
  return newClient;
}
