import { getModelForClass, modelOptions, prop, Severity } from '@typegoose/typegoose';

export class FactKpiElements {
  @prop({ required: true, type: String })
  view: string;

  @prop({ required: true, type: String })
  goal: string;

  @prop({ required: true, type: String })
  description: string;

  @prop({ required: true, type: String })
  comment: string;

  @prop({ required: true, type: Number })
  weight: number;

  @prop({ required: true, type: Number })
  factWeight: number;

  @prop({ type: String })
  employeeComment?: string | null;
}

@modelOptions({
  options: { allowMixed: Severity.ALLOW, customName: 'KPIFact' },
  schemaOptions: {
    collection: 'kpiFact',
  },
})
export class KPIFact {
  @prop({ type: String })
  public employeeId!: string | undefined;

  @prop({ required: true, type: Number })
  public year!: number;

  @prop({ required: true, type: String })
  public month!: string;

  @prop({ type: String })
  public status!: string;

  @prop({ required: true })
  public kpiFactElements!: Array<FactKpiElements>;

  @prop({ type: String })
  public createdDate!: string;

  @prop({ type: Number, default: 0 })
  public hocPayments?: number;

  @prop({ type: Number })
  public totalSalary?: number;
}

export const KPIFactModel = getModelForClass(KPIFact);
