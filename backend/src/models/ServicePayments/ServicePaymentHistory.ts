import { getModelForClass, modelOptions, prop, Ref, Severity } from '@typegoose/typegoose';
import mongoose from 'mongoose';
import { Client } from '../Client/Client';
import { ServiceElement } from './ServicePayment';

@modelOptions({
  options: { allowMixed: Severity.ALLOW, customName: 'servicePaymentHistory' },
  schemaOptions: {
    collection: 'servicePaymentHistory',
  },
})
export class ServicePaymentHistory {
  @prop({ required: true, type: Date })
  public startDate!: Date;

  @prop({ required: true, ref: () => Client })
  public clientId!: Ref<Client> | null;

  @prop({ required: true, type: Number })
  public totalSum!: number;

  @prop({})
  public serviceElements!: ServiceElement[];

  @prop({ type: mongoose.Types.ObjectId, ref: 'ServicePayment' })
  public servicePaymentId?: string;

  @prop({ type: Date })
  public dateOfEvent?: Date;
}

export const ServicePaymentHistoryModel = getModelForClass(ServicePaymentHistory);
