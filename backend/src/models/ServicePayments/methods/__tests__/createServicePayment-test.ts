import { beforeAll, describe, expect, jest, test } from '@jest/globals';
import { ServicePaymentModel } from '../../../../models/ServicePayments/ServicePayment';
import { createServicePayment } from '../createServicePayment';

jest.setTimeout(180000);

const START_DATE = new Date('2023-01-25T16:08:14.153+00:00');
const CLIENT_ID = '63d0c53cf290426db96f5be8';
const TOTALSUM = 386000;
const SERVICE_ELEMENT = [
  {
    title: 'Операции',
    quantity: 5,
    price: 50000,
    sum: 250000,
  },
];

const servicePayment = {
  serviceElements: SERVICE_ELEMENT,
  startDate: START_DATE,
  clientId: CLIENT_ID,
  totalSum: TOTALSUM,
};

describe('models.ServicePayments.methods.createServicePayment', () => {
  beforeAll(() => {
    ServicePaymentModel.create = jest.fn<any>().mockResolvedValue(servicePayment);
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  test('позитивная проверка на создание Абонентской платы', async () => {
    const newServicePayment = await createServicePayment(servicePayment);
    expect(newServicePayment).toMatchObject(servicePayment);
  });

  describe('негативная проверка на создание Абонентской платы', () => {
    test('ошибка на отсутсвие абонентской платы', async () => {
      const service = {
        serviceElements: [],
        startDate: START_DATE,
        clientId: CLIENT_ID,
        totalSum: TOTALSUM,
      };
      await expect(createServicePayment(service)).rejects.toThrowError(
        'Необходимо создать хотя бы одну абонплату'
      );
    });

    test('ошибка на отсутсвие даты создания', async () => {
      const service = {
        serviceElements: SERVICE_ELEMENT,
        startDate: null,
        clientId: CLIENT_ID,
        totalSum: TOTALSUM,
      };
      await expect(createServicePayment(service)).rejects.toThrowError('Укажите дату создания');
    });

    test('ошибка на отсутсвие id клиента', async () => {
      const service = {
        serviceElements: SERVICE_ELEMENT,
        startDate: START_DATE,
        clientId: null,
        totalSum: TOTALSUM,
      };
      await expect(createServicePayment(service)).rejects.toThrowError('Клиент должен быть указан');
    });

    test('ошибка на отсутсвие общей суммы абонентской платы', async () => {
      const service = {
        serviceElements: SERVICE_ELEMENT,
        startDate: START_DATE,
        clientId: CLIENT_ID,
        totalSum: null,
      };
      await expect(createServicePayment(service)).rejects.toThrowError(
        'Полная сумма по абонлате должно быть заполнено'
      );
    });
  });
});
