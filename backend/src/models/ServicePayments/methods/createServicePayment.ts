import { ServiceElement, ServicePaymentModel } from '../ServicePayment';

interface Props {
  serviceElements: ServiceElement[];
  startDate: Date | null;
  clientId: string | null;
  totalSum: number | null;
}

export async function createServicePayment(props: Props) {
  const { serviceElements, startDate, clientId, totalSum } = props;
  if (!startDate) {
    throw new Error('Укажите дату создания');
  }
  if (!clientId) {
    throw new Error('Клиент должен быть указан');
  }
  if (!totalSum) {
    throw new Error('Полная сумма по абонлате должно быть заполнено');
  }
  if (!serviceElements || serviceElements.length === 0) {
    throw new Error('Необходимо создать хотя бы одну абонплату');
  }

  const service = await ServicePaymentModel.create(props);
  return service;
}
