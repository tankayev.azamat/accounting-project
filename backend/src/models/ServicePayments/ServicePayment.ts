import { getModelForClass, modelOptions, post, prop, Ref, Severity } from '@typegoose/typegoose';
import { Client } from '../Client/Client';
import { ServicePaymentHistoryModel } from './ServicePaymentHistory';

export class ServiceElement {
  @prop({ required: true })
  public title: string;

  @prop({ required: true })
  public quantity: number;

  @prop({ required: true })
  public price: number;

  @prop({ required: true })
  public sum: number;
}

class HistoryElems {
  @prop({ required: true, type: String })
  _id: string;

  @prop({ required: true, type: Date })
  startDate!: Date;

  @prop({ required: true, ref: () => Client })
  clientId!: Ref<Client> | null;

  @prop({ required: true, type: Number })
  totalSum!: number;

  @prop({ type: ServiceElement })
  serviceElements!: ServiceElement[];

  @prop({ required: true, type: Date })
  dateOfEvent: Date;
}
@post<HistoryElems>('save', async (toHistory) => {
  const date = new Date();

  const { serviceElements, totalSum, clientId, startDate } = toHistory;
  try {
    await ServicePaymentHistoryModel.create({
      serviceElements,
      totalSum,
      clientId,
      startDate,
      servicePaymentId: toHistory._id,
      dateOfEvent: date,
    });
  } catch (e) {
    throw e;
  }
})
@post<HistoryElems>('findOneAndUpdate', async (toHistory) => {
  const date = new Date();

  const { serviceElements, totalSum, clientId, startDate } = toHistory;
  try {
    await ServicePaymentHistoryModel.create({
      serviceElements,
      totalSum,
      clientId,
      startDate,
      servicePaymentId: toHistory._id,
      dateOfEvent: date,
    });
  } catch (e) {
    throw e;
  }
})
@modelOptions({
  options: { allowMixed: Severity.ALLOW, customName: 'ServicePayment' },
  schemaOptions: {
    collection: 'servicePayment',
  },
})
export class ServicePayment {
  @prop({ required: true, type: Date })
  public startDate!: Date;

  @prop({ required: true, ref: () => Client })
  public clientId!: Ref<Client> | null;

  @prop({ required: true, type: Number })
  public totalSum!: number;

  @prop({ type: ServiceElement })
  public serviceElements!: ServiceElement[];
}

export const ServicePaymentModel = getModelForClass(ServicePayment);
