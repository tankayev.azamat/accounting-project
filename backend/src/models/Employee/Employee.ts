import {
  DocumentType,
  getModelForClass,
  modelOptions,
  prop,
  Ref,
  Severity,
} from '@typegoose/typegoose';
import { Position } from '../catalogs/Positions/Position';

export enum EmployeeStatus {
  works = 'Работает',
  fired = 'Не работает',
}

@modelOptions({
  options: { allowMixed: Severity.ALLOW, customName: 'Employee' },
  schemaOptions: {
    collection: 'employee',
  },
})
export class Employee {
  @prop({ required: true, type: String })
  public firstName!: string;

  @prop({ required: true, type: String })
  public lastName!: string;

  @prop({ required: true, ref: () => Position })
  public position!: Ref<Position>;

  @prop({ required: true, enum: EmployeeStatus, type: String })
  public status!: string;

  @prop({ required: true, type: Number })
  public fixedSalary!: number;

  @prop({ ref: () => Employee })
  public headId!: Ref<Employee> | null;

  @prop({ required: true, type: Date })
  public dateOfEmployment!: Date;

  @prop({ type: Date || null })
  public dateOfDismissal?: Date | null;
}

export const EmployeeModel = getModelForClass(Employee);
export type EmployeeDocumentType = DocumentType<Employee>;
