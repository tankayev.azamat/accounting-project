import { PositionModel } from '../../catalogs/Positions/Position';
import { EmployeeModel } from '../Employee';

export interface CreateEmployeeParams {
  firstName: string;
  lastName: string;
  position: string;
  status: string;
  fixedSalary: number | string;
  headId: string | null;
  dateOfEmployment: Date;
}

export async function createEmployee(params: CreateEmployeeParams) {
  let { firstName, lastName } = params;
  const { fixedSalary, position, headId, status, dateOfEmployment } = params;

  if (firstName && lastName) {
    firstName = firstName.trim();
    lastName = lastName.trim();
  }
  if (!firstName) {
    throw new Error('Имя должно быть заполнено');
  }

  if (!lastName) {
    throw new Error('Фамилия должна быть заполнена');
  }
  if (!/^\d+$/.test(String(fixedSalary))) {
    throw new Error('Оклад должен состоять только из цифр');
  }
  if (!/^[A-Za-zА-Яа-яёЁ]+(?:[-'\s][A-Za-zА-Яа-яёЁ]+)*$/.test(lastName)) {
    throw new Error('Фамилия должна включать только буквы');
  }
  if (!/^[A-Za-zА-Яа-яёЁ]+(?:[-'\s][A-Za-zА-Яа-яёЁ]+)*$/.test(firstName)) {
    throw new Error('Имя должно включать только буквы');
  }
  if (!position) {
    throw new Error('Не указан id позиции');
  }
  if (!status) {
    throw new Error('Не указан Статус');
  }
  if (!dateOfEmployment) {
    throw new Error('Не указана дата принятия на работу');
  }

  const positionDoc = await PositionModel.findById(position);

  if (positionDoc?.headPositionId && !headId) {
    throw new Error('Начальник не совпадает');
  }
  const newEmployee = await EmployeeModel.create(params);
  return newEmployee;
}
