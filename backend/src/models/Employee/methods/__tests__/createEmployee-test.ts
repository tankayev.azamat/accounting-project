import { beforeAll, describe, expect, jest, test } from '@jest/globals';
import { PositionModel } from '../../../catalogs/Positions/Position';
import { EmployeeModel } from '../../Employee';
import { createEmployee } from '../createEmployee';

jest.setTimeout(180000);

const employee = {
  firstName: 'Василий',
  lastName: 'Цветков',
  position: '845181495146',
  status: 'Работает',
  fixedSalary: 235000,
  headId: null,
  dateOfEmployment: new Date('2023-01-25T05:52:25.138+00:00'),
};

describe('models.Employee.methods.createEmployee', () => {
  beforeAll(async () => {
    EmployeeModel.create = jest.fn<any>().mockResolvedValue(employee);
    PositionModel.findById = jest.fn<any>().mockResolvedValue('845181495146');
  });

  test('позитивная проверка на создание сотрудника', async () => {
    const newEmployee = await createEmployee(employee);
    expect(newEmployee).toMatchObject(employee);
  });

  describe('Негативная проверка на создание сотрудника', () => {
    test('Не передано поле - Имя', async () => {
      employee.firstName = '';
      await expect(createEmployee(employee)).rejects.toThrowError(
        new Error('Имя должно быть заполнено')
      );
    });

    test('Не передано поле - Фамилия', async () => {
      employee.firstName = 'Василий';
      employee.lastName = '';
      await expect(createEmployee(employee)).rejects.toThrowError(
        new Error('Фамилия должна быть заполнена')
      );
    });

    test('Некорректные данные в поле Фамилия', async () => {
      employee.lastName = 'Цветков123';
      await expect(createEmployee(employee)).rejects.toThrowError(
        new Error('Фамилия должна включать только буквы')
      );
    });

    test('Некорректные данные в поле Имя', async () => {
      employee.lastName = 'Цветков';
      employee.firstName = 'Василий123';
      await expect(createEmployee(employee)).rejects.toThrowError(
        new Error('Имя должно включать только буквы')
      );
    });

    test('Не передано поле - Статус', async () => {
      employee.firstName = 'Василий';
      employee.status = '';
      await expect(createEmployee(employee)).rejects.toThrowError(new Error('Не указан Статус'));
    });
  });
});
