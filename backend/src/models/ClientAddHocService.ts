import {
  getModelForClass,
  modelOptions,
  mongoose,
  prop,
  Ref,
  Severity,
} from '@typegoose/typegoose';
import { Client } from './Client/Client';
import { Employee } from './Employee/Employee';

@modelOptions({
  options: { allowMixed: Severity.ALLOW, customName: 'ClientAddHocService' },
  schemaOptions: {
    collection: 'ClientAddHocService',
  },
})
export class ClientAddHocService {
  @prop({ required: true, type: String })
  public serviceElements!: string;

  @prop({ required: true, type: Date || null })
  public startDate!: Date | null;

  @prop({ type: Date || null })
  public endDate?: Date | null;

  @prop({ type: Date || null })
  public payDate?: Date | null;

  @prop({ type: String })
  public description!: string;

  @prop({ required: true, type: Number })
  public sum!: number;

  @prop({ required: true, type: mongoose.Types.ObjectId, ref: 'Employee' })
  public employeeId!: Employee[];

  @prop({ required: true, ref: () => Client })
  public clientId!: Ref<Client> | null;
}

export const ClientAddHocServiceModel = getModelForClass(ClientAddHocService);
