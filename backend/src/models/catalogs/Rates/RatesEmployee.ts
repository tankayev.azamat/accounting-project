import { getModelForClass, modelOptions, prop, Severity } from '@typegoose/typegoose';

@modelOptions({
  options: { allowMixed: Severity.ALLOW, customName: 'RatesEmployee' },
  schemaOptions: {
    collection: 'RatesEmployee',
  },
})
export class RatesEmployeeModel {
  @prop({ required: true, type: String, unique: true })
  public position!: string;

  @prop({ required: true, type: Number })
  public percent!: number;
}

export const RatesEmployee = getModelForClass(RatesEmployeeModel);
