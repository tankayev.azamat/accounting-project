import { getModelForClass, modelOptions, post, prop, Severity } from '@typegoose/typegoose';
import { RatesHistoryModel } from './RatesHistory';

class HistoryElems {
  @prop({ required: true, type: String })
  _id: string;

  @prop({ required: true, type: String })
  elementsOfService: string;

  @prop({ required: true, type: Number })
  price: number;

  @prop({ required: true, type: Date })
  dateOfEvent: Date;
}

@post<HistoryElems>('save', async (toHistory) => {
  const date = new Date();

  const { elementsOfService, price } = toHistory;
  try {
    await RatesHistoryModel.create({
      elementsOfService,
      price,
      rateId: toHistory._id,
      dateOfEvent: date,
    });
  } catch (e) {
    throw e;
  }
})
@modelOptions({
  options: { allowMixed: Severity.ALLOW, customName: 'Rates' },
  schemaOptions: {
    collection: 'Rates',
  },
})
export class RatesModel {
  @prop({ required: true, type: String, unique: true })
  public elementsOfService!: string;

  @prop({ required: true, type: Number })
  public price!: number;
}

export const Rates = getModelForClass(RatesModel);
