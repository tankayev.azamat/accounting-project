import { getModelForClass, modelOptions, post, prop, Severity } from '@typegoose/typegoose';
import { KpiKeyIndicatorHistoryModel } from './KpiKeyIndicatorHistory';

class HistoryElems {
  @prop({ type: String })
  _id: string;

  @prop({ required: true, type: String })
  title: string;

  @prop({ required: true, type: Date })
  dateOfEvent: Date;
}

@post<HistoryElems>('save', async (toHistory) => {
  const date = new Date();

  const { title } = toHistory;
  try {
    await KpiKeyIndicatorHistoryModel.create({
      title,
      kpiKeyIndicatorId: toHistory._id,
      dateOfEvent: date,
    });
  } catch (e) {
    throw e;
  }
})
@modelOptions({
  options: { allowMixed: Severity.ALLOW, customName: 'kpiKeyElement' },
  schemaOptions: {
    collection: 'kpiKeyElement',
  },
})
export class KpiKeyIndicator {
  @prop({ required: true, type: String, unique: true })
  public title!: string;
}

export const KpiKeyIndicatorModel = getModelForClass(KpiKeyIndicator);
