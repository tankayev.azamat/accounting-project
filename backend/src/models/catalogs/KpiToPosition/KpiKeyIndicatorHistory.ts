import { getModelForClass, modelOptions, prop, Severity } from '@typegoose/typegoose';
import mongoose from 'mongoose';

@modelOptions({
  options: { allowMixed: Severity.ALLOW, customName: 'kpiKeyElementHistory' },
  schemaOptions: {
    collection: 'kpiKeyElementHistory',
  },
})
export class KpiKeyIndicatorHistory {
  @prop({ type: mongoose.Types.ObjectId, ref: 'kpiKeyElement' })
  public kpiKeyIndicatorId?: string;

  @prop({ required: true, type: String })
  public title!: string;

  @prop({ type: Date })
  public dateOfEvent?: Date;
}

export const KpiKeyIndicatorHistoryModel = getModelForClass(KpiKeyIndicatorHistory);
