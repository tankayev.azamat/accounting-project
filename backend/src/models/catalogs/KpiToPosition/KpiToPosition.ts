import { getModelForClass, modelOptions, post, prop, Severity } from '@typegoose/typegoose';
import mongoose from 'mongoose';
import { KpiCatalogHistoryModel } from './KpiToPositionHistory';
export class KpiElements {
  @prop({ required: true, type: String })
  view: string;

  @prop({ required: true, type: String })
  goal: string;

  @prop({ required: true, type: String })
  description: string;

  @prop({ required: true, type: String })
  comment: string;

  @prop({ required: true, type: Number })
  weight: number;
}
class HistoryElems {
  @prop({ required: true, type: String })
  _id: string;

  @prop({ required: true, type: KpiElements })
  kpiElements: Array<KpiElements>;

  @prop({ required: true, type: String })
  positionId: string;

  @prop({ required: true, type: Number })
  percentForKpi: number;

  @prop({ required: true, type: Date })
  dateOfEvent: Date;
}
@post<HistoryElems>('save', async (toHistory) => {
  const date = new Date();

  const { kpiElements, percentForKpi, positionId } = toHistory;
  try {
    await KpiCatalogHistoryModel.create({
      kpiElements,
      percentForKpi,
      positionId,
      kpiId: toHistory._id,
      dateOfEvent: date,
    });
  } catch (e) {
    throw e;
  }
})
@post<HistoryElems>('findOneAndUpdate', async (toHistory) => {
  const date = new Date();

  const { kpiElements, percentForKpi, positionId } = toHistory;
  try {
    await KpiCatalogHistoryModel.create({
      kpiElements,
      percentForKpi,
      positionId,
      kpiId: toHistory._id,
      dateOfEvent: date,
    });
  } catch (e) {
    throw e;
  }
})
@modelOptions({
  options: { allowMixed: Severity.ALLOW, customName: 'kpiCatalog' },
  schemaOptions: {
    collection: 'kpiCatalog',
  },
})
export class Kpi {
  @prop({ required: true })
  public kpiElements!: Array<KpiElements>;

  @prop({ type: Number, default: 0 })
  public percentForKpi?: number;

  @prop({ type: mongoose.Types.ObjectId, ref: 'Position' })
  public positionId?: string;
}

export const KpiCatalogModel = getModelForClass(Kpi);
