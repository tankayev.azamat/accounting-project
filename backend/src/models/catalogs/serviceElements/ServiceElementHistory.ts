import { getModelForClass, modelOptions, prop, Severity } from '@typegoose/typegoose';
import mongoose from 'mongoose';

@modelOptions({
  options: { allowMixed: Severity.ALLOW, customName: 'serviceElementHistory' },
  schemaOptions: {
    collection: 'serviceElementHistory',
  },
})
export class ServiceElementHistory {
  @prop({ required: true, type: String })
  public title!: string;

  @prop({ type: mongoose.Types.ObjectId, ref: 'serviceElement' })
  public serviceElementId?: string;

  @prop({ type: Date })
  public dateOfEvent?: Date;
}

export const ServiceElementHistoryModel = getModelForClass(ServiceElementHistory);
