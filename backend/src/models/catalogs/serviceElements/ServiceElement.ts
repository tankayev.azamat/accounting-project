import { getModelForClass, modelOptions, post, prop, Severity } from '@typegoose/typegoose';
import { ServiceElementHistoryModel } from './ServiceElementHistory';
class HistoryElems {
  @prop({ required: true, type: String })
  _id: string;

  @prop({ required: true, type: String })
  title: string;

  @prop({ required: true, type: Date })
  dateOfEvent: Date;
}

@post<HistoryElems>('save', async (toHistory) => {
  const date = new Date();

  const { title } = toHistory;
  try {
    await ServiceElementHistoryModel.create({
      title,
      serviceElementId: toHistory._id,
      dateOfEvent: date,
    });
  } catch (e) {
    throw e;
  }
})
@post<HistoryElems>('update', async (toHistory) => {
  const date = new Date();

  const { title } = toHistory[0];
  try {
    await ServiceElementHistoryModel.create({
      title,
      serviceElementId: toHistory[0]._id,
      dateOfEvent: date,
    });
  } catch (e) {
    throw e;
  }
})
@modelOptions({
  options: { allowMixed: Severity.ALLOW, customName: 'serviceElement' },
  schemaOptions: {
    collection: 'serviceElement',
  },
})
export class ServiceElement {
  @prop({ required: true, type: String, unique: true })
  public title!: string;
}

export const ServiceElementModel = getModelForClass(ServiceElement);
