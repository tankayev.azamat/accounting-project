import { getModelForClass, modelOptions, prop, Severity } from '@typegoose/typegoose';

export enum UserRoles {
  admin = 'admin',
  user = 'user',
}

@modelOptions({
  options: { allowMixed: Severity.ALLOW, customName: 'Role' },
  schemaOptions: {
    collection: 'role',
  },
})
export class Role {
  @prop({ required: true, unique: true, enum: UserRoles, type: String, default: UserRoles.user })
  public title!: string;
}

export const RoleModel = getModelForClass(Role);
