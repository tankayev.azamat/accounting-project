import jwt from 'jsonwebtoken';

export const generateToken = async ({ id, role }: { id: string; role: string }) => {
  const secret = process.env.SECRET;
  const payload = { id, role };
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  return jwt.sign(payload, secret!, { expiresIn: '10h' });
};
