import { getModelForClass, modelOptions, pre, prop, Severity } from '@typegoose/typegoose';
import bcrypt from 'bcrypt';
import mongoose from 'mongoose';
@modelOptions({
  options: { allowMixed: Severity.ALLOW, customName: 'User' },
  schemaOptions: {
    collection: 'user',
  },
})
@pre<User>('save', function (next: () => void) {
  if (this.isModified('password')) {
    const salt = bcrypt.genSaltSync(10);
    this.password = bcrypt.hashSync(this.password, salt);
  }
  next();
})
export class User {
  @prop({ type: String, unique: true, required: true })
  public login!: string;

  @prop({
    type: String,
    required: true,
    validate: {
      validator: (value: string) => {
        return value.length >= 8;
      },
      message: 'Длина пароля должна быть не менее 8 символов',
    },
  })
  public password!: string;

  @prop({ type: mongoose.Types.ObjectId, ref: 'Employee' })
  public employeeId?: string;

  @prop({ type: mongoose.Types.ObjectId, ref: 'Position' })
  public positionId?: string;

  @prop({ required: true, type: mongoose.Types.ObjectId, ref: 'Role' })
  public roleId!: string;

  @prop({ required: true, type: Date, default: new Date().toISOString() })
  public createdDate!: Date;
}

export const UserModel = getModelForClass(User);
