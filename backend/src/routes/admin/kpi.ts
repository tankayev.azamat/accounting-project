import express, { Request, Response } from 'express';
import auth from '../../middleware/auth';
import { checkRole } from '../../middleware/checkRole';
import { KpiCatalogModel } from '../../models/catalogs/KpiToPosition/KpiToPosition';
const router = express.Router();

router.post('/', [auth, checkRole(['admin'])], async (req: Request, res: Response) => {
  try {
    const newKpi = await KpiCatalogModel.create(req.body);
    return res.status(200).send({ message: 'KPI добавлен успешно', newKpi });
  } catch (e) {
    return res.status(400).send({ message: 'KPI не добавлен', e });
  }
});

router.get('/:id', [auth, checkRole(['admin'])], async (req: Request, res: Response) => {
  try {
    const kpi = await KpiCatalogModel.find({ positionId: req.params.id }).exec();

    if (kpi.length === 0) {
      return res.send(kpi[0]);
    }
    return res.send(kpi);
  } catch (e) {
    return res.status(400).send(e);
  }
});

router.put('/:id', [auth, checkRole(['admin'])], async (req: Request, res: Response) => {
  const { kpiElements, percentForKpi, positionId } = req.body;
  try {
    const element = await KpiCatalogModel.findOneAndUpdate(
      { _id: req.params.id },
      {
        kpiElements,
        percentForKpi,
        positionId,
      },
      { new: true }
    ).exec();

    return element
      ? res.send({ message: 'KPI обновлен успешно', element })
      : res.send({ message: 'KPI не обновлен' });
  } catch (e) {
    return res.status(400).send(e);
  }
});
export default router;
