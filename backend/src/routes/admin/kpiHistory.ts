import express, { Request, Response } from 'express';
import { Position } from 'models/catalogs/Positions/Position';
import auth from '../../middleware/auth';
import { checkRole } from '../../middleware/checkRole';
import { KpiCatalogHistoryModel } from '../../models/catalogs/KpiToPosition/KpiToPositionHistory';
const router = express.Router();

router.get('/:id', [auth, checkRole(['admin'])], async (req: Request, res: Response) => {
  try {
    const KpiHistory = await KpiCatalogHistoryModel.find({ kpiId: req.params.id })
      .populate<{
        positionId: Position;
      }>('positionId')
      .exec();

    if (KpiHistory.length === 0) {
      return res.send(KpiHistory[0]);
    }
    return res.send(KpiHistory);
  } catch (e) {
    return res.status(400).send(e);
  }
});

export default router;
