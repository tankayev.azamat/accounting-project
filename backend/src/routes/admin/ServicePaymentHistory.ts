import express, { Request, Response } from 'express';
import auth from '../../middleware/auth';
import { checkRole } from '../../middleware/checkRole';
import { ServicePaymentHistoryModel } from '../../models/ServicePayments/ServicePaymentHistory';

const router = express.Router();

router.get('/:id', [auth, checkRole(['admin'])], async (req: Request, res: Response) => {
  try {
    const PaymentHistory = await ServicePaymentHistoryModel.find({
      servicePaymentId: req.params.id,
    }).exec();

    if (PaymentHistory.length === 0) {
      return res.send(PaymentHistory[0]);
    }
    return res.send(PaymentHistory);
  } catch (e) {
    return res.status(400).send(e);
  }
});

export default router;
