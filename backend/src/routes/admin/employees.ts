import express, { Request, Response } from 'express';
import auth from '../../middleware/auth';
import checkRole from '../../middleware/checkRole';
import { Position, PositionModel } from '../../models/catalogs/Positions/Position';
import { ClientModel, ClientStatus } from '../../models/Client/Client';
import { Employee, EmployeeModel, EmployeeStatus } from '../../models/Employee/Employee';
import { createEmployee } from '../../models/Employee/methods/createEmployee';
import { UserModel } from '../../models/User/User';
const router = express.Router();

router.post('/', async (req: Request, res: Response) => {
  try {
    const newEmployee = await createEmployee(req.body);

    return res.status(200).send({ message: 'Сотрудник создан успешно!', newEmployee });
  } catch (e) {
    return res.status(400).send({ message: 'что-то пошло не так', e });
  }
});

router.get('/', [auth, checkRole(['admin', 'user'])], async (req: Request, res: Response) => {
  const userInfo = req.body.user;
  const user = await UserModel.findById(userInfo.id).populate('roleId');
  if (!user) return res.status(400).send({ message: 'Пользователь не найден' });

  if (req?.query?.status) {
    try {
      const status = (req.query.status as string).split(',');
      const clients = await EmployeeModel.find({
        status: { $in: status },
      })
        .populate<{
          position: Position;
        }>('position')
        .sort({ registrationDate: -1 })
        .exec();
      return res.send(clients);
    } catch (e) {
      return res.status(400).send(e);
    }
  }

  if (req?.query.position) {
    let employees: Employee[] = [];
    try {
      if (user.roleId['title'] === 'user') {
        const emp = await EmployeeModel.findById(user?.employeeId);
        if (!emp)
          return res
            .status(400)
            .send({ message: 'Данные сотрудника находящегося в текущей сессии не найдены' });
        const position = await PositionModel.findById(emp.position);
        if (!position)
          return res
            .status(400)
            .send({ message: 'Не найдена запрашиваемая должность в базе данных' });

        if (position.title === 'Ведущий бухгалтер') {
          employees = await EmployeeModel.find({
            status: EmployeeStatus.works,
            position: req.query.position,
            headId: emp._id,
          }).populate<{
            position: Position;
          }>('position');
          return res.send(employees);
        } else {
          return res.status(400).send({ message: 'У Вас недостаточно прав' });
        }
      } else {
        employees = await EmployeeModel.find({
          status: EmployeeStatus.works,
          position: req.query.position,
        }).populate<{
          position: Position;
        }>('position');
        return res.send(employees);
      }
    } catch (e) {
      return res.status(400).send(e);
    }
  }

  if (req?.query.works) {
    try {
      const employees = await EmployeeModel.find({
        status: EmployeeStatus.works,
      }).populate<{
        position: Position;
      }>('position');
      return res.send(employees);
    } catch (e) {
      return res.status(400).send(e);
    }
  }

  if (req?.query.clientsOfEmployeeId) {
    try {
      const employeeId = req.query.clientsOfEmployeeId;
      if (!employeeId) return res.status(400).send({ message: 'Пользователь не найден' });
      const clients = await ClientModel.find({
        status: ClientStatus.active,
        accompanyingEmployees: { $in: [employeeId] },
      }).exec();
      return res.send(clients);
    } catch (e) {
      return res.status(400).send(e);
    }
  }

  try {
    const employees = await EmployeeModel.find({
      status: [EmployeeStatus.fired, EmployeeStatus.works],
    })
      .populate<{
        position: Position;
      }>('position')
      .sort({ dateOfEmployment: -1 })
      .exec();
    return res.send(employees);
  } catch (e) {
    return res.status(400).send(e);
  }
});

router.get('/:id', [auth, checkRole(['admin'])], async (req: Request, res: Response) => {
  try {
    const employee = await EmployeeModel.findById(req.params.id).populate<{
      position: Position;
    }>('position');
    return res.send(employee);
  } catch (e) {
    return res.status(400).send(e);
  }
});

router.patch('/:id', [auth, checkRole(['admin'])], async (req: Request, res: Response) => {
  const { status } = req.body;
  if (!status) return res.status(400).send({ message: 'Не указан Статус' });
  try {
    const clients = await EmployeeModel.findByIdAndUpdate(req.params.id, {
      status: req.body.status,
    }).exec();
    return res.send({ message: 'Сотруднику успешно присвоен статус "Удален"', clients });
  } catch (e) {
    return res.status(400).send({ message: 'Что-то пошло не так', e });
  }
});

router.put('/:id', [auth, checkRole(['admin'])], async (req: Request, res: Response) => {
  let { firstName, lastName } = req.body;
  const { fixedSalary } = req.body;
  try {
    if (firstName && lastName) {
      firstName = firstName.trim();
      lastName = lastName.trim();
    }

    if (fixedSalary && !/^\d+$/.test(String(fixedSalary)))
      return res.status(400).send({ message: 'Оклад должен состоять только из цифр' });

    if (!/^[A-Za-zА-Яа-яёЁ]+(?:[-'\s][A-Za-zА-Яа-яёЁ]+)*$/.test(lastName))
      return res.status(400).send({ message: 'Фамилия должна включать только буквы' });
    if (!/^[A-Za-zА-Яа-яёЁ]+(?:[-'\s][A-Za-zА-Яа-яёЁ]+)*$/.test(firstName))
      return res.status(400).send({ message: 'Имя должна включать только буквы' });

    if (
      !req.body.firstName &&
      !req.body.lastName &&
      !req.body.fixedSalary &&
      !req.body.status &&
      !req.body.position &&
      !req.body.headId &&
      !req.body.dateOfEmployment &&
      !req.body.dateOfDismissal
    ) {
      return res.sendStatus(400).send({ message: 'не отправляете данные' });
    }
    const employees = await EmployeeModel.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
    })
      .populate<{
        position: Position;
      }>('position')
      .exec();
    return employees ? res.send({ message: 'Данные успешно обновлены', employees }) : null;
  } catch (e) {
    return res.status(400).send({ message: 'Что-то пошло не так', e });
  }
});

export default router;
