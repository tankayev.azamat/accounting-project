import express, { Request, Response } from 'express';
import auth from '../../middleware/auth';
import checkRole from '../../middleware/checkRole';
import { Rates } from '../../models/catalogs/Rates/Rates';
import { Client } from '../../models/Client/Client';
import {
  ActiveClientsFactOfService,
  FactForMonthModel,
} from '../../models/FactsOfServicePayments/FactForMonth';
import { ServicePaymentModel } from '../../models/ServicePayments/ServicePayment';
const router = express.Router();

router.post('/', [auth, checkRole(['admin'])], async (req: Request, res: Response) => {
  const { year, month } = req.body;

  if (!year) return res.status(400).send({ message: 'Дата не заполнена' });
  if (!month) return res.status(400).send({ message: 'Месяц не заполнен' });

  try {
    const isExist = await FactForMonthModel.find({ year: year, month: month }).exec();

    if (isExist.length === 0) {
      const activeClientsWithServiceFee = await ServicePaymentModel.find()
        .populate<{ clientId: Client }>('clientId')
        .exec();
      const filteredActiveClients = activeClientsWithServiceFee.filter(
        (element) => element.clientId.status === 'Активный'
      );

      const prices = await Rates.find().sort({ elementsOfService: 1 }).exec();

      const withPriceForEmployee = filteredActiveClients.map((client) => {
        const modifiedClient = client.serviceElements.map((item) => {
          const price = prices?.filter((price) => price.elementsOfService === item.title);
          const obj = { ...item, priceForEmployee: price[0]?.price, factQuantity: 0 };
          return obj;
        });

        const copy = {
          ...client,
          serviceElements: modifiedClient,
          difference: 0,
          factTotalSum: 0,
          status: 'Ожидает',
        };
        return copy;
      });
      const monthServiceFee = filteredActiveClients
        .map((client) => client.totalSum)
        .reduce((acc, number): number => {
          return acc + number;
        });

      const newFactForMonth = {
        year: year,
        month: month,
        activeClientsFactOfService: withPriceForEmployee,
        status: 'Создан',
        monthfactTotalSum: 0,
        monthTotalServicePaymentsFee: monthServiceFee,
        difference: 0,
      };
      const element = await FactForMonthModel.create(newFactForMonth);
      return res.status(200).send({ message: 'Факт за месяц создан успешно', element });
    }

    return res.status(400).send({ message: `Факт за "${month}" "${year}" года  был создан ранее` });
  } catch (error) {
    return res.status(400).send({
      message: 'Ошибка, факт за месяц не создан, имеется "Элемент Услуги" без тарифа',
      error,
    });
  }
});

router.get('/AllYears', [auth], async (req: Request, res: Response) => {
  try {
    const elements = await FactForMonthModel.find().sort({ year: 1 }).exec();
    const onlyYears = elements.map((element) => element.year);
    const sortedYears = new Set(onlyYears);

    return res.status(200).send(Array.from(sortedYears));
  } catch (error) {
    return res.status(404).send({ message: `Нет созданных Фактов за`, error });
  }
});
router.get('/:year', [auth], async (req: Request, res: Response) => {
  try {
    const elements = await FactForMonthModel.find({ year: req.params.year })
      .sort({ month: 1 })
      .exec();
    if (elements.length === 0) {
      return res
        .status(404)
        .send({ message: `Нет созданных элементов за ${req.params.year} год`, elements });
    }

    return res.status(200).send(elements);
  } catch (error) {
    return res
      .status(404)
      .send({ message: `Нет созданных Фактов за ${req.params.year} год`, error });
  }
});

router.get('/currentMonth/:id', [auth], async (req: Request, res: Response) => {
  try {
    const elements = await FactForMonthModel.find({ _id: req.params.id }).exec();
    if (elements.length === 0) {
      return res.status(404).send({ message: `Нет созданных фактов за выбранный месяц`, elements });
    }
    return res.status(200).send(elements);
  } catch (error) {
    return res
      .status(400)
      .send({ message: `Нет созданных элементов за ${req.params.year} год`, error });
  }
});
router.delete('/currentMonth/:id', [auth], async (req: Request, res: Response) => {
  try {
    const elements = await FactForMonthModel.findByIdAndDelete({ _id: req.params.id }).exec();
    return res.status(200).send({ message: 'Факт за месяц удален успешно', elements });
  } catch (error) {
    return res.status(400).send({ message: `Не удалось удалить факт`, error });
  }
});
router.patch('/currentMonth/:id', [auth], async (req: Request, res: Response) => {
  try {
    const elements = await FactForMonthModel.findByIdAndUpdate(
      { _id: req.params.id },
      { status: 'Обработан' }
    ).exec();
    return res.status(200).send({ message: 'Факт за месяц успешно закрыт', elements });
  } catch (error) {
    return res.status(400).send({ message: `Не удалось закрыть факт за месяц`, error });
  }
});
router.put(
  '/currentMonth/:id',
  [auth, checkRole(['admin'])],
  async (req: Request, res: Response) => {
    const {
      _id,
      status,
      clientId,
      startDate,
      difference,
      totalSum,
      factTotalSum,
      serviceElements,
    }: ActiveClientsFactOfService = req.body;
    if (!totalSum) return res.status(400).send({ message: 'Общая сумма Абонплаты не заполнена' });
    if (!clientId) return res.status(400).send({ message: 'Нет клиента' });
    if (!startDate) return res.status(400).send({ message: 'Нет даты начала абонплаты' });
    if (!difference) return res.status(400).send({ message: 'Не указана разница' });
    if (!status) return res.status(400).send({ message: 'Статус не заполнен' });
    if (!serviceElements)
      return res.status(400).send({ message: 'Не переданы элементы фактов абонплаты' });
    try {
      const monthFactId = req.params.id;
      const factForMonth = await FactForMonthModel.find({ _id: monthFactId }).exec();
      const index = factForMonth[0].activeClientsFactOfService.findIndex(
        (item) => item._id === _id
      );

      factForMonth[0].activeClientsFactOfService[index] = {
        _id,
        status,
        clientId,
        startDate,
        difference,
        factTotalSum,
        totalSum,
        serviceElements,
      };
      const monthFactFee = factForMonth[0].activeClientsFactOfService
        .map((clients) => clients.factTotalSum)
        .reduce((acc, number) => acc + number);
      factForMonth[0].monthfactTotalSum = monthFactFee;
      factForMonth[0].difference =
        factForMonth[0].monthTotalServicePaymentsFee - factForMonth[0].monthfactTotalSum;
      const newFact = factForMonth[0];

      const updated = await FactForMonthModel.findByIdAndUpdate(monthFactId, newFact);

      return res.status(200).send({ message: 'Данные сохранены успешно', updated });
    } catch (e) {
      return res.status(400).send(e);
    }
  }
);
export default router;
