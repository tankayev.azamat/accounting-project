import express, { Request, Response } from 'express';
import auth from '../../middleware/auth';
import checkRole from '../../middleware/checkRole';
import { PositionModel } from '../../models/catalogs/Positions/Position';

const router = express.Router();

router.get('/', [auth], async (req: Request, res: Response) => {
  try {
    const positions = await PositionModel.find().sort({ title: 1 }).exec();
    return res.send(positions);
  } catch (e) {
    return res.status(400).send(e);
  }
});

router.get('/:id', [auth], async (req: Request, res: Response) => {
  try {
    const position = await PositionModel.findById(req.params.id).exec();
    return res.send(position);
  } catch (e) {
    return res.status(400).send(e);
  }
});

router.post('/', [auth, checkRole(['admin'])], async (req: Request, res: Response) => {
  const { title } = req.body;
  try {
    if (!title) return res.status(400).send({ message: 'Наименование не указано.' });
    const headPosition = await PositionModel.findOne({ title: 'Ведущий бухгалтер' });
    const newPosition = await PositionModel.create({
      title: title.trim(),
      headPositionId: headPosition?._id,
    });
    return res.status(200).send({ text: 'Должность добавлена успешно!', newPosition });
  } catch (e) {
    return res.status(400).send({ text: 'Должность не добавлена', e });
  }
});

router.put('/:id', [auth, checkRole(['admin'])], async (req: Request, res: Response) => {
  const { title, headPositionId } = req.body;
  try {
    if (!title) return res.status(400).send({ message: 'Наименование не указано.' });
    const position = await PositionModel.findByIdAndUpdate(req.params.id, {
      title: title.trim(),
      headPositionId,
    }).exec();
    return position ? res.send({ text: 'Должность изменена успешно!', position }) : null;
  } catch (e) {
    return res.status(400).send({ text: 'Должность не изменена', e });
  }
});

export default router;
