import express, { Request, Response } from 'express';
import auth from '../../middleware/auth';
import checkRole from '../../middleware/checkRole';
import { Rates } from '../../models/catalogs/Rates/Rates';

const router = express.Router();

router.post('/', [auth, checkRole(['admin'])], async (req: Request, res: Response) => {
  const { elementsOfService, price } = req.body;
  if (!elementsOfService) return res.status(400).send({ text: 'Элемент услуг не указано.' });
  if (!price) return res.status(400).send({ text: 'Цена не указано.' });
  try {
    const rateUnique = await Rates.findOne({ elementsOfService });
    if (rateUnique) {
      return res.send({ text: `По элементу ${elementsOfService} тариф уже добавлен!` });
    } else {
      const rate = await Rates.create({ elementsOfService, price });
      return res.status(200).send({ text: `Тариф ${elementsOfService} добавлена успешно!`, rate });
    }
  } catch (error) {
    return res.status(400).send({ text: 'Тариф не добавлена', error });
  }
});

router.get('/', [auth], async (req: Request, res: Response) => {
  try {
    const rate = await Rates.find().sort({ elementsOfService: 1 }).exec();
    return res.status(200).send(rate);
  } catch (error) {
    return res.status(400).send(error);
  }
});

router.put('/:id', async (req: Request, res: Response) => {
  const { elementsOfService, price } = req.body;
  if (!elementsOfService) return res.status(400).send({ text: 'Элемент услуг не указано.' });
  if (!price) return res.status(400).send({ text: 'Цена не указано.' });
  try {
    const rate = await Rates.findByIdAndUpdate(req.params.id, {
      elementsOfService,
      price,
    }).exec();
    return rate ? res.status(200).send({ text: 'Тариф изменена успешно!', rate }) : null;
  } catch (e) {
    return res.status(400).send({ text: 'Тариф не изменена', e });
  }
});
export default router;
