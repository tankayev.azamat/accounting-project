import express, { Request, Response } from 'express';
import auth from '../../middleware/auth';
import checkRole from '../../middleware/checkRole';
import { Client } from '../../models/Client/Client';
import { createServicePayment } from '../../models/ServicePayments/methods/createServicePayment';
import { ServicePaymentModel } from '../../models/ServicePayments/ServicePayment';

const router = express.Router();

router.get('/', [auth], async (req: Request, res: Response) => {
  if (req.query.client) {
    const service = await ServicePaymentModel.find({
      clientId: req.query.client,
    })
      .populate<{ clientId: Client }>('clientId')
      .exec();
    return res.status(200).send(service);
  }
  try {
    const services = await ServicePaymentModel.find().exec();
    return res.send(services);
  } catch (e) {
    return res.status(400).send({ message: 'Абонплата не найдена' });
  }
});

router.post('/', [auth, checkRole(['admin'])], async (req: Request, res: Response) => {
  try {
    const service = await createServicePayment(req.body);
    return res.status(200).send({ service, message: 'Абонентская плата добавлена успешно' });
  } catch (e) {
    return res.status(400).send({ message: 'Абонентская плата не добавлена', e });
  }
});

router.put('/:id', [auth, checkRole(['admin'])], async (req: Request, res: Response) => {
  const { serviceElements, startDate, clientId, totalSum } = req.body;
  if (!req.params.id) {
    return res.status(400).send({ message: 'Не переданы параметры поиска Клиента' });
  }
  if (!startDate) {
    return res.status(400).send({ message: 'Укажите дату редактирования.' });
  }
  if (!clientId) {
    return res.status(400).send({ message: 'Клиент должен быть указан.' });
  }
  if (!totalSum) {
    return res.status(400).send({ message: 'Полная сумма по абонлате должно быть заполнено.' });
  }
  if (!serviceElements || serviceElements.length === 0) {
    return res.status(400).send({ message: 'Необходимо создать хотя бы одну абонплату' });
  }

  try {
    const element = await ServicePaymentModel.findOneAndUpdate(
      { _id: req.params.id },
      {
        serviceElements,
        startDate,
        clientId,
        totalSum,
      },
      { new: true }
    ).exec();

    return res.status(200).send({ message: 'Абонентская плата обновлена успешно', element });
  } catch (e) {
    return res.status(400).send({ message: 'Абонентская плата не обновлена', e });
  }
});
export default router;
