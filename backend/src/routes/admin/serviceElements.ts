import express, { Request, Response } from 'express';
import auth from '../../middleware/auth';
import checkRole from '../../middleware/checkRole';
import { ServiceElementModel } from '../../models/catalogs/serviceElements/ServiceElement';

const router = express.Router();

router.post('/', [auth, checkRole(['admin'])], async (req: Request, res: Response) => {
  if (!req.body.title) return res.status(400).send('Вы не указали наименование!');
  try {
    if (req.body.title) {
      req.body.title.trim();
    }
    const element = await ServiceElementModel.create(req.body);
    return res.status(200).send({ text: 'Элемент услуги создана успешно!', element });
  } catch (error) {
    return res.status(400).send({ text: 'Элемент услуги не создана', error });
  }
});

router.get('/', [auth], async (req: Request, res: Response) => {
  try {
    const elements = await ServiceElementModel.find().sort({ title: 1 }).exec();

    elements.sort((a, b) => {
      const first = a.title;
      const second = b.title;
      return first.localeCompare(second);
    });

    return res.status(200).send(elements);
  } catch (error) {
    return res.status(400).send(error);
  }
});

router.put('/:id', [auth, checkRole(['admin'])], async (req: Request, res: Response) => {
  let { title } = req.body;
  if (!title) return res.status(400).send('Вы не указали наименование!');
  try {
    if (title) {
      title = title.trim();
    }
    const element = await ServiceElementModel.findByIdAndUpdate(req.params.id, {
      title,
    }).exec();
    return element ? res.send({ text: 'Элемент услуги изменена успешно!', element }) : null;
  } catch (e) {
    return res.status(400).send({ text: 'Элемент услуги не изменена', e });
  }
});
export default router;
