import express, { Request, Response } from 'express';
import auth from '../../middleware/auth';
import checkRole from '../../middleware/checkRole';
import { KpiKeyIndicatorModel } from '../../models/catalogs/KpiToPosition/KpiKeyIndicator';

const router = express.Router();

router.post('/', [auth, checkRole(['admin'])], async (req: Request, res: Response) => {
  try {
    const element = await KpiKeyIndicatorModel.create(req.body);
    return res.status(200).send({ message: 'Элемент добавлен успешно', element });
  } catch (error) {
    return res.status(400).send({ message: 'Элемент не добавлен', error });
  }
});

router.get('/', [auth], async (req: Request, res: Response) => {
  try {
    const elements = await KpiKeyIndicatorModel.find().sort({ title: 1 }).exec();
    return res.status(200).send(elements);
  } catch (error) {
    return res.status(400).send(error);
  }
});

router.put('/:id', async (req: Request, res: Response) => {
  const { title } = req.body;
  try {
    const element = await KpiKeyIndicatorModel.findByIdAndUpdate(req.params.id, {
      title,
    }).exec();
    return element ? res.send(element) : null;
  } catch (e) {
    return res.status(400).send(e);
  }
});
export default router;
