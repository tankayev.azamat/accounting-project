import express, { Request, Response } from 'express';
import auth from '../../middleware/auth';
import checkRole from '../../middleware/checkRole';
import { PositionModel } from '../../models/catalogs/Positions/Position';
import { Client, ClientModel, ClientStatus } from '../../models/Client/Client';
import { EmployeeModel, EmployeeStatus } from '../../models/Employee/Employee';
import { UserModel } from '../../models/User/User';

const router = express.Router();

router.get('/', [auth, checkRole(['user'])], async (req: Request, res: Response) => {
  const userInfo = req.body.user;
  const user = await UserModel.findById(userInfo.id);
  if (!user) return res.status(400).send({ message: 'Пользователь не найден' });
  const emp = await EmployeeModel.findById(user?.employeeId);
  if (!emp)
    return res
      .status(400)
      .send({ message: 'Данные сотрудника находящегося в текущей сессии не найдены' });
  const position = await PositionModel.findById(emp.position);
  if (!position)
    return res.status(400).send({ message: 'Не найдена запрашиваемая должность в базе данных' });
  let clients: Client[] = [];
  try {
    if (position.title === 'Ведущий бухгалтер') {
      const subEmployees = await EmployeeModel.find({
        status: EmployeeStatus.works,
        headId: emp._id,
      }).exec();

      for (const item of subEmployees) {
        const subEmpClients = await ClientModel.find({
          status: ClientStatus.active,
          accompanyingEmployees: { $in: [item?._id] },
        }).exec();
        for (const i of subEmpClients) {
          clients.push(i);
        }
      }

      const headEmpClients = await ClientModel.find({
        status: ClientStatus.active,
        accompanyingEmployees: { $in: [emp?._id] },
      }).exec();

      for (const item of headEmpClients) {
        clients.push(item);
      }
      clients = [...new Map(clients.map((item) => [item['name'], item])).values()];
    } else {
      clients = await ClientModel.find({
        status: ClientStatus.active,
        accompanyingEmployees: { $in: [emp?._id] },
      }).exec();
    }
    return res.send(clients);
  } catch (e) {
    return res.status(400).send(e);
  }
});

export default router;
