import express, { Request, Response } from 'express';
import auth from '../../middleware/auth';
import { checkRole } from '../../middleware/checkRole';
import { KpiCatalogModel } from '../../models/catalogs/KpiToPosition/KpiToPosition';
import { EmployeeModel } from '../../models/Employee/Employee';
import { KPIFactModel } from '../../models/KPIFact/KPIFact';
import { UserModel } from '../../models/User/User';
const router = express.Router();

router.get('/', [auth, checkRole(['user'])], async (req: Request, res: Response) => {
  const userInfo = req.body.user;
  const employee = await UserModel.findById(userInfo.id).then(async (user) => {
    if (!user) return;
    const emp = await EmployeeModel.findById(user?.employeeId);
    return emp;
  });
  if (!employee) return res.status(400).send({ message: 'Пользователь не найден' });

  try {
    const kpi = await KpiCatalogModel.find({ positionId: employee?.position })
      .populate('positionId')
      .exec();
    if (!kpi) return res.status(400).send({ message: 'Kpi для запрашиваемой должности не найден' });
    return res.send(kpi);
  } catch (e) {
    return res.status(400).send(e);
  }
});

router.get('/employee', [auth, checkRole(['user'])], async (req: Request, res: Response) => {
  const userInfo = req.body.user;
  try {
    let emp;
    await UserModel.findById(userInfo.id).then(async (user) => {
      if (!user) return;
      emp = await EmployeeModel.findById(user?.employeeId);
    });
    return res.send([emp]);
  } catch (e) {
    return res.status(400).send(e);
  }
});

router.get('/:id', [auth, checkRole(['user'])], async (req: Request, res: Response) => {
  try {
    const kpiFact = await KPIFactModel.findById(req.params.id).exec();
    return res.status(200).send(kpiFact);
  } catch (error) {
    return res.status(400).send(error);
  }
});

export default router;
