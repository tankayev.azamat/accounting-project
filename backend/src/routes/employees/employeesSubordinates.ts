import express, { Request, Response } from 'express';
import auth from '../../middleware/auth';
import { checkRole } from '../../middleware/checkRole';
import { EmployeeModel } from '../../models/Employee/Employee';
import { UserModel } from '../../models/User/User';
const router = express.Router();

router.get('/', [auth, checkRole(['user'])], async (req: Request, res: Response) => {
  const userInfo = req.body.user;
  try {
    const employee = await UserModel.findById(userInfo.id).then(async (user) => {
      if (!user) return;
      const emp = await EmployeeModel.findById(user?.employeeId);
      return emp;
    });
    if (!employee) return res.status(400).send({ message: 'Пользователь не найден' });

    const subEmployees = await EmployeeModel.find({ headId: employee?._id }).populate('position');
    if (!subEmployees)
      return res.status(400).send({ message: 'Запрашиваемые данные по подчиненным не найдены' });
    return res.status(200).send(subEmployees);
  } catch (e) {
    return res.status(400).send(e);
  }
});

export default router;
